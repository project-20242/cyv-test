package net.tecgurus.clickyvende;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.InputStream;
import java.nio.file.Paths;
import java.util.Properties;

public class BaseTest {

    protected WebDriver driver;
    protected String baseUrl;

    /**
     * Este método se ejecuta antes de cada prueba. Configura el entorno de prueba.
     */
    @BeforeEach
    public void setUp() throws Exception {
        // Cargar las propiedades desde el archivo application.properties
        Properties properties = new Properties();
        try (InputStream input = getClass().getClassLoader().getResourceAsStream("application.properties")) {
            if (input == null) {
                System.out.println("Lo siento, no se puede encontrar application.properties");
                return;
            }
            properties.load(input);
        }

        // Obtener la URL según el entorno (producción o local)
        String environment = System.getProperty("env", "development");
        baseUrl = properties.getProperty("url." + environment, properties.getProperty("url"));

        // Configurar el driver de Chrome
        // Obtener la ruta absoluta del chromedriver en el directorio resources
        String chromeDriverPath = Paths.get(getClass().getClassLoader().getResource("chromedriver.exe").toURI()).toFile().getAbsolutePath();
        System.setProperty("webdriver.chrome.driver", chromeDriverPath);

        // Configurar ChromeOptions
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--allow-insecure-localhost"); // Permitir conexiones locales inseguras
        options.addArguments("--disable-web-security"); // Deshabilitar la seguridad web
        options.addArguments("--remote-allow-origins=*"); // Permitir orígenes remotos

        // Inicializar el driver de Chrome con opciones
        driver = new ChromeDriver(options);
    }

    /**
     * Este método se ejecuta después de cada prueba. Cierra el navegador.
     */
    @AfterEach
    public void tearDown() {
        if (driver != null) {
            // Cerrar el navegador
            driver.quit();
        }
    }
}