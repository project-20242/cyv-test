package net.tecgurus.clickyvende.products;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import net.tecgurus.clickyvende.BaseTest;

public class ProductsImageImportTest extends BaseTest {

    @Test
    public void testMassImport() {
        // PASO 1: Preparación del entorno
        loginAsAdmin();

        // PASO 2: Navegar al módulo de productos
        navigateToProductsModule();

        // PASO 3: Importar productos desde un archivo Excel
        importProducts();

        // PASO 4: Navegar al módulo de imágenes
        navigateToImagesModule();

        // PASO 5: Importar imágenes desde un archivo ZIP
        importImages();

        // PASO 6: Verificar la importación
        verifyImport();
    }

    private void loginAsAdmin() {
        driver.get(baseUrl + "login");
        driver.manage().window().maximize();

        // Espera explícita para asegurar que el elemento interceptador no sea clickeable
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("cm")));

        // Localización y acción sobre los elementos de entrada y el botón
        WebElement usernameInput = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("email")));
        WebElement passwordInput = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("password")));
        WebElement accept = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("c-p-bn")));
        WebElement loginButton = driver.findElement(By.cssSelector(".btn.btn-primary.btn-block.mt-2.login_button"));

        usernameInput.sendKeys("alex.mtz@gmail.com");
        passwordInput.sendKeys("12345678");
        accept.click();

        // Usar JavaScript para hacer clic en el botón si es necesario
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", loginButton);
    }


    private void navigateToProductsModule() {
        driver.manage().window().maximize();
        driver.findElement(By.cssSelector(".dash-item:nth-child(6) .dash-mtext")).click();
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("cm")));
        WebElement productLink = wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("Producto")));
        productLink.click();
        driver.findElement(By.cssSelector(".material-icons")).click();
    }

    private void navigateToImagesModule() {
        driver.manage().window().maximize();
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        WebElement uploadImage = wait
                .until(ExpectedConditions.elementToBeClickable(By.cssSelector(".material-symbols-outlined")));
        uploadImage.click();
    }

    private void importProducts() {
        Path filePath = Paths.get("src", "test", "resources", "perfumes_alex.xlsx");
        WebElement fileInput = driver.findElement(By.id("file"));
        fileInput.sendKeys(filePath.toAbsolutePath().toString());
        WebElement importButton = driver.findElement(By.id("import-button"));
        importButton.click();
        new WebDriverWait(driver, Duration.ofSeconds(30))
                .until(ExpectedConditions.visibilityOfElementLocated(By.id("notifier-container")));
    }

    private void importImages() {
        Path filePath = Paths.get("src", "test", "resources", "Imagenes.zip");
        WebElement fileInput = driver.findElement(By.id("file"));
        fileInput.sendKeys(filePath.toAbsolutePath().toString());
        WebElement importButton = driver.findElement(By.id("import-button"));
        importButton.click();
        new WebDriverWait(driver, Duration.ofSeconds(30))
                .until(ExpectedConditions.visibilityOfElementLocated(By.id("notifier-container")));
    }

    private void verifyImport() {
        driver.manage().window().maximize();
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20)); // Espera de hasta 20 segundos

        // Hacer clic y escribir en el campo de búsqueda
        WebElement searchInput = wait
                .until(ExpectedConditions.elementToBeClickable(By.cssSelector(".dataTable-input")));
        searchInput.click();
        searchInput.clear();
        searchInput.sendKeys("Hotdog");

        // Esperar a que los resultados de la búsqueda se actualicen y sean visibles
        // Esto depende de cómo se muestren los resultados en tu aplicación
        WebElement result = wait
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[contains(text(), 'Hotdog')]")));
        String palabra = "Hotdog";
        assertEquals("Hotdog", palabra, "El nombre del producto debe ser Hotdog");
        String palabra2 = "Belleza";
        assertEquals("Belleza", palabra2, "La categoria del producto debe ser Belleza");
        String palabra3 = "Perfumes";
        assertEquals("Perfumes", palabra3, "La subcategoria del producto debe ser Perfumes");
        String palabra4 = "D&G";
        assertEquals("D&G", palabra4, "La marca del producto debe ser D&G");
        String palabra5 = "Recomendado";
        assertEquals("Recomendado", palabra5, "La etiqueta del producto debe ser Recomendado");

        // Espera adicional para observación durante las pruebas (opcional)
        try {
            Thread.sleep(10000); // Espera fija de 10 segundos
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}