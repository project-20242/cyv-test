package net.tecgurus.clickyvende.home;

import net.tecgurus.clickyvende.BaseTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.InputStream;
import java.nio.file.Paths;
import java.util.Properties;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.interactions.Actions;

import java.time.Duration;
import java.util.Set;

import static org.asynchttpclient.util.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class Busquedaproductos extends BaseTest {

    @Test
    public void Busquedaproductos() throws InterruptedException {
        driver.manage().window().maximize();
        driver.get(baseUrl);

        try {
            WebElement acceptCookiesButton = driver.findElement(By.xpath("//button[contains(text(), 'Aceptar')]"));
            assertNotNull(acceptCookiesButton, "El botón de aceptar cookies no se encontró.");
            acceptCookiesButton.click();
            Thread.sleep(1000);
        } catch (Exception e) {
            System.out.println("El botón de aceptar cookies no se encontró o ya se aceptaron.");
        }

        WebElement loginButtonInitial = driver.findElement(By.xpath("//span[contains(text(), 'Inicio Sesión')]"));
        assertNotNull(loginButtonInitial, "El botón de Inicio Sesión no se encontró.");
        loginButtonInitial.click();

        WebElement emailField = driver.findElement(By.id("email"));
        WebElement passwordField = driver.findElement(By.id("password"));
        WebElement loginButton = driver.findElement(By.className("login_button"));

        assertNotNull(emailField, "El campo de correo electrónico no se encontró.");
        assertNotNull(passwordField, "El campo de contraseña no se encontró.");
        assertNotNull(loginButton, "El botón de inicio de sesión no se encontró.");

        emailField.sendKeys("chamoy@gmail.com");
        passwordField.sendKeys("12345678");
        loginButton.click();

        Thread.sleep(2000);

        WebElement storeSelectionLink = driver.findElement(By.xpath("//a[contains(@class, 'cust-btn') and contains(@class, 'bg-warning')]"));
        assertNotNull(storeSelectionLink, "El enlace de selección de tienda no se encontró.");
        storeSelectionLink.click();

        Thread.sleep(1000);
        Thread.sleep(2000);

        unomas();
    }

    private void unomas() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20));

        String mainTab = driver.getWindowHandle();
        System.out.println("Main Tab:" + mainTab);

        WebElement storeImage = driver.findElement(By.xpath("//*[@id=\"storeList\"]/a[4]/img"));
        assertNotNull(storeImage, "La imagen de la tienda no se encontró.");
        storeImage.click();

        Thread.sleep(2000);
        wait.until(ExpectedConditions.numberOfWindowsToBe(2));

        Set<String> handles = driver.getWindowHandles();
        for (String actual : handles) {
            System.out.println("Handle ID:" + actual);
            if (!actual.equalsIgnoreCase(mainTab)) {
                System.out.println("Changing Tab");
                driver.switchTo().window(actual);

                Thread.sleep(2000);

                WebElement buscarProduct = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("product")));
                assertNotNull(buscarProduct, "El campo de búsqueda de productos no se encontró.");
                buscarProduct.sendKeys("LECHE LALA");

                Thread.sleep(2000);

                WebElement buscarclick = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/header/div[3]/div/div[2]/div[1]/form/li")));
                assertNotNull(buscarclick, "El botón de búsqueda no se encontró.");
                buscarclick.click();
                Thread.sleep(5000);

                WebElement volverinicio = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/header/div[3]/div/div[1]/h1/a")));
                assertNotNull(volverinicio, "El enlace para volver al inicio no se encontró.");
                volverinicio.click();

                Thread.sleep(2000);

                WebElement todascategorias = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"slider_first_button_0_preview\"]")));
                assertNotNull(todascategorias, "El botón de todas las categorías no se encontró.");
                todascategorias.click();

                Thread.sleep(2000);

                WebElement vercate = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/section[2]/div/div[2]/div[1]/div/div[2]/div/a")));
                assertNotNull(vercate, "El enlace de ver categoría no se encontró.");
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", vercate);
                Thread.sleep(2000);

                WebElement tenisselec = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/section[2]/div/div[2]/div[1]/div/div[2]/div/div/div/div[2]/label")));
                assertNotNull(tenisselec, "El selector de tenis no se encontró.");
                tenisselec.click();
                Thread.sleep(2000);

                WebElement postreselec = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/section[2]/div/div[2]/div[1]/div/div[2]/div/div/div/div[3]/label")));
                assertNotNull(postreselec, "El selector de postres no se encontró.");
                postreselec.click();

                Thread.sleep(2000);

                WebElement paver = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/section[2]/div/div[1]/div[2]/div/div/div")));
                assertNotNull(paver, "El elemento para ver más productos no se encontró.");
                ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", paver);
                Thread.sleep(2000);

                WebElement clickacept = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"product_filter_btn\"]")));
                assertNotNull(clickacept, "El botón de aceptar filtros no se encontró.");
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", clickacept);

                Thread.sleep(3000);

                WebElement filtrouno = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/section[2]/div/div[2]/div[1]/div/div[6]/button[2]")));
                assertNotNull(filtrouno, "El primer filtro no se encontró.");
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", filtrouno);
                Thread.sleep(2000);

                WebElement vercate2 = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/section[2]/div/div[2]/div[1]/div/div[2]/div/a")));
                assertNotNull(vercate2, "El segundo enlace de ver categoría no se encontró.");
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", vercate2);
                Thread.sleep(2000);

                WebElement tenisselec2 = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/section[2]/div/div[2]/div[1]/div/div[2]/div/div/div/div[2]/label")));
                assertNotNull(tenisselec2, "El segundo selector de tenis no se encontró.");
                tenisselec2.click();
                Thread.sleep(2000);

                WebElement vermarca1 = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/section[2]/div/div[2]/div[1]/div/div[5]/div/a")));
                assertNotNull(vermarca1, "El enlace de ver marca no se encontró.");
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", vermarca1);
                Thread.sleep(2000);

                WebElement paraver2 = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/section[2]/div/div[1]/div[2]/div/div/div")));
                assertNotNull(paraver2, "El elemento para ver más productos (segunda vez) no se encontró.");
                ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", paraver2);

                Thread.sleep(1000);
                WebElement seleccionarmarca = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/section[2]/div/div[2]/div[1]/div/div[5]/div/div/div/div[4]/label")));
                assertNotNull(seleccionarmarca, "El selector de marca no se encontró.");
                seleccionarmarca.click();
                Thread.sleep(2000);

                WebElement clickaceptmarc = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"product_filter_btn\"]")));
                assertNotNull(clickaceptmarc, "El botón de aceptar filtros por marca no se encontró.");
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", clickaceptmarc);

                Thread.sleep(3000);

                WebElement produdesta = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/section[3]/div[2]/div[1]")));
                assertNotNull(produdesta, "El producto destacado no se encontró.");
                ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", produdesta);

                Thread.sleep(2000);

                WebElement aceptarleche = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/section[3]/div[2]/div[2]/div/div/div[7]/div/div[3]/div[2]/button")));
                assertNotNull(aceptarleche, "El botón de aceptar leche no se encontró.");
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", aceptarleche);

                Thread.sleep(2000);

                WebElement volver = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/footer/div[2]/div[1]/div[1]/div/div[1]/a")));
                assertNotNull(volver, "El enlace para volver al inicio no se encontró.");
                volver.click();

                Thread.sleep(2000);

                WebElement iniciarsesion = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/header/div[3]/div/div[2]/div[3]/ul/li[2]/a")));
                assertNotNull(iniciarsesion, "El enlace de iniciar sesión no se encontró.");
                iniciarsesion.click();
                Thread.sleep(2000);

                WebElement emailField = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("email")));
                assertNotNull(emailField, "El campo de correo electrónico no se encontró.");
                emailField.clear();
                Thread.sleep(2000);
                emailField.sendKeys("ineshabibi@gmail.com");
                Thread.sleep(2000);

                WebElement passwordField = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("password")));
                assertNotNull(passwordField, "El campo de contraseña no se encontró.");
                passwordField.clear();
                Thread.sleep(2000);
                passwordField.sendKeys("12345678");
                Thread.sleep(2000);

                WebElement boton = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("login_button")));
                assertNotNull(boton, "El botón de inicio de sesión no se encontró.");
                boton.click();
                Thread.sleep(2000);

                WebElement recomendaciones = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"product_category_button_preview\"]")));
                assertNotNull(recomendaciones, "El botón de recomendaciones no se encontró.");
                ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", recomendaciones);

                Thread.sleep(5000);
            }
        }
    }
}