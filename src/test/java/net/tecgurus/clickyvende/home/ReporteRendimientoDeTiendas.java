package net.tecgurus.clickyvende.home;

import net.tecgurus.clickyvende.BaseTest;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ReporteRendimientoDeTiendas extends BaseTest {

    @Test
    public void ejecutarPrueba() throws InterruptedException {
        iniciarSesionYVerTienda();
    }

    private void iniciarSesionYVerTienda() throws InterruptedException {
        driver.manage().window().maximize();
        driver.get(baseUrl);

        WebDriverWait espera = new WebDriverWait(driver, Duration.ofSeconds(20));

        try {
            WebElement acceptCookiesButton = driver.findElement(By.xpath("//button[contains(text(), 'Aceptar')]"));
            assertNotNull(acceptCookiesButton, "El botón de aceptar cookies no se encontró.");
            acceptCookiesButton.click();
            Thread.sleep(1000);
        } catch (Exception e) {
            System.out.println("El botón de aceptar cookies no se encontró o ya se aceptaron.");
        }

        WebElement loginButtonInitial = driver.findElement(By.xpath("//span[contains(text(), 'Inicio Sesión')]"));
        assertNotNull(loginButtonInitial, "El botón de Inicio Sesión no se encontró.");
        loginButtonInitial.click();

        WebElement emailField = driver.findElement(By.id("email"));
        WebElement passwordField = driver.findElement(By.id("password"));
        WebElement loginButton = driver.findElement(By.className("login_button"));

        assertNotNull(emailField, "El campo de correo electrónico no se encontró.");
        assertNotNull(passwordField, "El campo de contraseña no se encontró.");
        assertNotNull(loginButton, "El botón de inicio de sesión no se encontró.");

        emailField.sendKeys("superadmin@example.com");
        passwordField.sendKeys("12345678");
        loginButton.click();

        Thread.sleep(2000);

        WebElement menuReportes = espera.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"html-dir-tag\"]/body/nav/div[1]/div[2]/div[1]/div[2]/div/div/div/ul/li[6]")));
        assertNotNull(menuReportes, "El menú de reportes no se encontró.");
        menuReportes.click();
        Thread.sleep(2000);

        WebElement opcionVentasPorTienda = espera.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//html[@id='html-dir-tag']/body/nav/div/div[2]/div/div[2]/div/div/div/ul/li[6]/ul/li/a")));
        assertNotNull(opcionVentasPorTienda, "La opción de ventas por tienda no se encontró.");
        opcionVentasPorTienda.click();
        Thread.sleep(2000);

        WebElement opcionVentasPorAno = espera.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"pills-tab\"]/li[1]")));
        assertNotNull(opcionVentasPorAno, "La opción de ventas por año no se encontró.");
        opcionVentasPorAno.click();
        Thread.sleep(2000);

        WebElement botonDescargaAno = espera.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"html-dir-tag\"]/body/div[1]/div/div[1]/div/div/div[2]/div/a")));
        assertNotNull(botonDescargaAno, "El botón de descarga por año no se encontró.");
        botonDescargaAno.click();
        Thread.sleep(2000);

        WebElement opcionUltimoMes = espera.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"pills-tab\"]/li[2]")));
        assertNotNull(opcionUltimoMes, "La opción de ventas del último mes no se encontró.");
        opcionUltimoMes.click();
        Thread.sleep(2000);

        WebElement botonDescargaUltimoMes = espera.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"html-dir-tag\"]/body/div[1]/div/div[1]/div/div/div[2]/div/a")));
        assertNotNull(botonDescargaUltimoMes, "El botón de descarga del último mes no se encontró.");
        botonDescargaUltimoMes.click();
        Thread.sleep(2000);

        WebElement opcionEsteMes = espera.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"pills-tab\"]/li[3]")));
        assertNotNull(opcionEsteMes, "La opción de ventas de este mes no se encontró.");
        opcionEsteMes.click();
        Thread.sleep(2000);

        WebElement botonDescargaEsteMes = espera.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"html-dir-tag\"]/body/div[1]/div/div[1]/div/div/div[2]/div/a")));
        assertNotNull(botonDescargaEsteMes, "El botón de descarga de este mes no se encontró.");
        botonDescargaEsteMes.click();
        Thread.sleep(2000);

        WebElement campoFechaInicio = espera.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='start_date']")));
        assertNotNull(campoFechaInicio, "El campo de fecha de inicio no se encontró.");
        ((JavascriptExecutor) driver).executeScript("arguments[0].value='2024-05-09';", campoFechaInicio);
        assertTrue(campoFechaInicio.getAttribute("value").equals("2024-05-09"), "La fecha de inicio no se estableció correctamente.");
        Thread.sleep(2000);

        WebElement campoFechaFin = espera.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='end_date']")));
        assertNotNull(campoFechaFin, "El campo de fecha de fin no se encontró.");
        ((JavascriptExecutor) driver).executeScript("arguments[0].value='2024-05-10';", campoFechaFin);
        assertTrue(campoFechaFin.getAttribute("value").equals("2024-05-10"), "La fecha de fin no se estableció correctamente.");
        Thread.sleep(2000);

        WebElement botonGenerarReporte = espera.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"filter_type\"]")));
        assertNotNull(botonGenerarReporte, "El botón de generar reporte no se encontró.");
        botonGenerarReporte.click();
        Thread.sleep(2000);

        WebElement botonDescargaFechas = espera.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"html-dir-tag\"]/body/div[1]/div/div[1]/div/div/div[2]/div/a")));
        assertNotNull(botonDescargaFechas, "El botón de descarga entre fechas no se encontró.");
        botonDescargaFechas.click();
        Thread.sleep(2000);
    }
}
