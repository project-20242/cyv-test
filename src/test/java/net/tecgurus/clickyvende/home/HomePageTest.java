package net.tecgurus.clickyvende.home;


import net.tecgurus.clickyvende.BaseTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.InputStream;
import java.nio.file.Paths;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class HomePageTest extends BaseTest {


    /**
     * Este método contiene la prueba que verifica si el sitio web muestra el texto específico.
     */
    @Test
    public void testHomePage() {
        // Navegar a la URL base
        driver.get(baseUrl);

        // Encontrar el elemento del cuerpo de la página
        WebElement element = driver.findElement(By.tagName("body"));

        // Obtener el texto del cuerpo
        String bodyText = element.getText();

        // Verificar que el texto esperado esté presente en el cuerpo de la página
        assertTrue(bodyText.contains("Donde la Excelencia se Fusiona con la Innovación"), "¡Texto no encontrado!");
    }
}
