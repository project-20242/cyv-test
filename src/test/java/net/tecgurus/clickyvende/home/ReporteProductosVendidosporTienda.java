package net.tecgurus.clickyvende.home;

import net.tecgurus.clickyvende.BaseTest;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class ReporteProductosVendidosporTienda extends BaseTest {

    @Test
    public void testVerReporteProductos() throws InterruptedException {
        // Ingresar a la página
        LoginYVerTienda();

    }




    private void LoginYVerTienda() throws InterruptedException {
        driver.manage().window().maximize();
        driver.get(baseUrl + "login");

        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20)); // Aumentamos el tiempo de espera

        // Definimos variables
        WebElement usernameInput = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("email")));
        WebElement passwordInput = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("password")));
        WebElement loginBtn = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".btn.btn-primary.btn-block.mt-2.login_button")));
        WebElement accept = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("c-p-bn")));

        Thread.sleep(2000);

        // Ingresamos las credenciales y hacemos clic en el botón de login
        usernameInput.sendKeys("superadmin@example.com");
        passwordInput.sendKeys("12345678");
        accept.click();
        loginBtn.click();

        Thread.sleep(2000);



        WebElement reports = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"html-dir-tag\"]/body/nav/div[1]/div[2]/div[1]/div[2]/div/div/div/ul/li[6]")));
        reports.click();
        Thread.sleep(2000);


        WebElement ventasportienda= wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"html-dir-tag\"]/body/nav/div[1]/div[2]/div[1]/div[2]/div/div/div/ul/li[6]/ul/li[2]")));
        ventasportienda.click();
        Thread.sleep(2000);


        WebElement element1 = driver.findElement(By.tagName("body"));
        String bodyText1 = element1.getText();
        assertTrue(bodyText1.contains("Reporte de los Productos más Vendidos por Tienda"), "¡Texto no encontrado 1!");


        //VENTAS POR AÑO

        WebElement poraño= wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"pills-tab\"]/li[1]")));
        poraño.click();
        Thread.sleep(2000);


        WebElement clickdescarga1= wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"html-dir-tag\"]/body/div[1]/div/div[1]/div/div/div[2]/div/a")));
        clickdescarga1.click();
        Thread.sleep(2000);

        //VENTAS DEL ULTIMO MES
        WebElement lastmonth= wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"pills-tab\"]/li[2]")));
        lastmonth.click();
        Thread.sleep(2000);

        WebElement clickdescarga2= wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"html-dir-tag\"]/body/div[1]/div/div[1]/div/div/div[2]/div/a")));
        clickdescarga2.click();
        Thread.sleep(2000);

        //VENTAS DE ESTE MES

        WebElement thismonth= wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"pills-tab\"]/li[3]")));
        thismonth.click();
        Thread.sleep(2000);

        WebElement clickdescarga3= wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"html-dir-tag\"]/body/div[1]/div/div[1]/div/div/div[2]/div/a")));
        clickdescarga3.click();
        Thread.sleep(2000);

        //VENTAS ENTRE FECHAS


        WebElement deldia= wait.until(ExpectedConditions.presenceOfElementLocated(By.name("start_date")));
        deldia.sendKeys("01062024");
        Thread.sleep(2000);


        WebElement aldia= wait.until(ExpectedConditions.presenceOfElementLocated(By.name("end_date")));
        aldia.sendKeys("08062024");
        Thread.sleep(2000);


        WebElement generar= wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"filter_type\"]")));
        generar.click();
        Thread.sleep(2000);

        WebElement clickdescarga4= wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"html-dir-tag\"]/body/div[1]/div/div[1]/div/div/div[2]/div/a")));
        clickdescarga4.click();
        Thread.sleep(2000);

        WebElement element2 = driver.findElement(By.tagName("body"));
        String bodyText2 = element2.getText();
        assertTrue(bodyText2.contains("Reporte de los Productos más Vendidos por Tienda"), "¡Texto no encontrado 2!");









    }

}



