package net.tecgurus.clickyvende.home;

import net.tecgurus.clickyvende.BaseTest;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.assertTrue;


public class tetsProbarReportedecuponesdedescuento extends BaseTest {

    @Test
    public void testProductCreation() throws InterruptedException {
        // Navegar a la URL base
        driver.get(baseUrl);

        try {
            WebElement acceptCookiesButton = driver.findElement(By.xpath("//button[contains(text(), 'Aceptar')]"));
            assertTrue(acceptCookiesButton.isDisplayed(), "El botón de aceptar cookies debe estar visible.");
            acceptCookiesButton.click();
            // Esperar un momento para asegurarse de que la acción se complete
            Thread.sleep(1000); // Ajusta el tiempo según sea necesario
        } catch (Exception e) {
            // No hacer nada si el botón no se encuentra
            System.out.println("El botón de aceptar cookies no se encontró o ya se aceptaron.");
        }

        // Encontrar y hacer clic en el botón "Inicio Sesión"
        WebElement loginButtonInitial = driver.findElement(By.xpath("//span[contains(text(), 'Inicio Sesión')]"));
        assertTrue(loginButtonInitial.isDisplayed(), "El botón de inicio de sesión debe estar visible.");
        loginButtonInitial.click();

        // Iniciar sesión
        WebElement emailField = driver.findElement(By.id("email"));
        WebElement passwordField = driver.findElement(By.id("password"));
        WebElement loginButton = driver.findElement(By.className("login_button"));

        assertTrue(emailField.isDisplayed(), "El campo de correo electrónico debe estar visible.");
        assertTrue(passwordField.isDisplayed(), "El campo de contraseña debe estar visible.");
        assertTrue(loginButton.isDisplayed(), "El botón de inicio de sesión debe estar visible.");

        emailField.sendKeys("admin@example.com");
        passwordField.sendKeys("12345678");
        loginButton.click();

        // Esperar a que la página cargue después del inicio de sesión
        Thread.sleep(2000);

        // Navegar a la sección de Reportes
        WebElement reportsMenu = driver.findElement(By.linkText("Reportes"));
        assertTrue(reportsMenu.isDisplayed(), "El menú de reportes debe estar visible.");
        reportsMenu.click();

        WebElement ordersReportsDropdown = driver.findElement(By.linkText("Reportes de pedidos"));
        assertTrue(ordersReportsDropdown.isDisplayed(), "El menú desplegable de reportes de pedidos debe estar visible.");
        ordersReportsDropdown.click();

        Thread.sleep(1000);

        WebElement discountCouponsReport = driver.findElement(By.linkText("Discount Coupons Report"));
        assertTrue(discountCouponsReport.isDisplayed(), "El reporte de cupones de descuento debe estar visible.");
        discountCouponsReport.click();

        // Esperar a que la página de reportes cargue completamente
        Thread.sleep(1000);

        // Generar reporte anual
        generateAnnualReport();

        // Generar reporte del último mes
        generateLastMonthReport();

        // Generar reporte del mes actual
        generateThisMonthReport();

        generateSevenDayReport();

        // Generar reporte de rango de fechas
        generateReportForDateRange("20/07/2024", "21/07/2024");

    }

    private void generateAnnualReport() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));

        // Seleccionar el botón de "Año"
        WebElement yearButton = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(text(), 'Año')]")));
        assertTrue(yearButton.isDisplayed(), "El botón de Año debe estar visible.");
        yearButton.click();

        // Esperar un momento para asegurarse de que la acción se complete
        Thread.sleep(1000); // Ajusta el tiempo según sea necesario

        // Hacer clic en el botón de exportar
        WebElement exportButton = driver.findElement(By.cssSelector(".exportChartButton"));
        assertTrue(exportButton.isDisplayed(), "El botón de exportar debe estar visible.");
        exportButton.click();

        // Esperar un momento para asegurarse de que el reporte se genere y descargue
        Thread.sleep(2000);

        // Aquí deberías agregar la verificación de que el archivo se descargó correctamente
        // Dependiendo de tu entorno, esto podría involucrar verificar la existencia de un archivo en una carpeta específica
    }

    private void generateLastMonthReport() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));

        // Seleccionar el botón "Último Mes"
        WebElement lastMonthButton = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("button.nav-link.chart-data[value='last-month']")));
        assertTrue(lastMonthButton.isDisplayed(), "El botón de Último Mes debe estar visible.");
        lastMonthButton.click();

        // Esperar un momento para asegurarse de que la acción se complete
        Thread.sleep(1000);

        // Seleccionar el botón de exportar
        WebElement exportButton = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".exportChartButton")));
        assertTrue(exportButton.isDisplayed(), "El botón de exportar debe estar visible.");
        exportButton.click();

        // Esperar un momento para asegurarse de que el reporte se genere y descargue
        Thread.sleep(2000);

        // Aquí deberías agregar la verificación de que el archivo se descargó correctamente
        // Dependiendo de tu entorno, esto podría involucrar verificar la existencia de un archivo en una carpeta específica
    }

    private void generateThisMonthReport() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));

        // Seleccionar el botón "Este Mes"
        WebElement thisMonthButton = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("button.nav-link.chart-data[value='this-month']")));
        assertTrue(thisMonthButton.isDisplayed(), "El botón de Este Mes debe estar visible.");
        thisMonthButton.click();

        // Esperar un momento para asegurarse de que la acción se complete
        Thread.sleep(1000);

        // Seleccionar el botón de exportar
        WebElement exportButton = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".exportChartButton")));
        assertTrue(exportButton.isDisplayed(), "El botón de exportar debe estar visible.");
        exportButton.click();

        // Esperar un momento para asegurarse de que el reporte se genere y descargue
        Thread.sleep(2000);

    }

    public void generateSevenDayReport() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));

        // Seleccionar el botón "Últimos 7 días"
        WebElement sevenDayButton = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("button.nav-link.chart-data[value='seven-day']")));
        assertTrue(sevenDayButton.isDisplayed(), "El botón de Últimos 7 días debe estar visible.");
        sevenDayButton.click();

        // Esperar un momento para asegurarse de que la acción se complete
        Thread.sleep(1000);

        // Seleccionar el botón de exportar
        WebElement exportButton = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".exportChartButton")));
        assertTrue(exportButton.isDisplayed(), "El botón de exportar debe estar visible.");
        exportButton.click();

        // Esperar un momento para asegurarse de que el reporte se genere y descargue
        Thread.sleep(2000);

        // Aquí deberías agregar la verificación de que el archivo se descargó correctamente
        // Dependiendo de tu entorno, esto podría involucrar verificar la existencia de un archivo en una carpeta específica
    }

    private void generateReportForDateRange(String startDate, String endDate) throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));

        // Ingresar la fecha de inicio
        WebElement startDateField = wait.until(ExpectedConditions.elementToBeClickable(By.name("start_date")));
        assertTrue(startDateField.isDisplayed(), "El campo de fecha de inicio debe estar visible.");
        startDateField.clear();
        startDateField.sendKeys(formatDate(startDate));

        // Ingresar la fecha de fin
        WebElement endDateField = wait.until(ExpectedConditions.elementToBeClickable(By.name("end_date")));
        assertTrue(endDateField.isDisplayed(), "El campo de fecha de fin debe estar visible.");
        endDateField.clear();
        endDateField.sendKeys(formatDate(endDate));

        // Esperar un momento para asegurarse de que la acción se complete
        Thread.sleep(3000);

        // Seleccionar el botón de generar
        WebElement generateButton = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("button.generate_button")));
        assertTrue(generateButton.isDisplayed(), "El botón de generar debe estar visible.");
        generateButton.click();

        // Esperar un momento para asegurarse de que el reporte se genere y descargue
        Thread.sleep(2000);

        // Seleccionar el botón de exportar
        WebElement exportButton = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".exportChartButton")));
        assertTrue(exportButton.isDisplayed(), "El botón de exportar debe estar visible.");
        exportButton.click();

        // Esperar un momento para asegurarse de que el reporte se genere y descargue
        Thread.sleep(2000);

        // Aquí deberías agregar la verificación de que el archivo se descargó correctamente
        // Dependiendo de tu entorno, esto podría involucrar verificar la existencia de un archivo en una carpeta específica
    }

    private String formatDate(String date) {
        DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate parsedDate = LocalDate.parse(date, inputFormatter);
        return outputFormatter.format(parsedDate);
    }
}
