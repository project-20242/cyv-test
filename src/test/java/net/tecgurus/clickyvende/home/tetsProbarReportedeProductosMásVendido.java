package net.tecgurus.clickyvende.home;

import net.tecgurus.clickyvende.BaseTest;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class tetsProbarReportedeProductosMásVendido extends BaseTest {

    @Test
    public void testGenerateProductReports() throws InterruptedException {
        // Navegar a la URL base
        driver.manage().window().maximize();
        driver.get(baseUrl);
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20)); // Aumentamos el tiempo de espera


        try {
            WebElement acceptCookiesButton = driver.findElement(By.xpath("//button[contains(text(), 'Aceptar')]"));
            acceptCookiesButton.click();
            // Esperar un momento para asegurarse de que la acción se complete
            Thread.sleep(1000); // Ajusta el tiempo según sea necesario
        } catch (Exception e) {
            // No hacer nada si el botón no se encuentra
            System.out.println("El botón de aceptar cookies no se encontró o ya se aceptaron.");
        }

        // Encontrar y hacer clic en el botón "Inicio Sesión"
        WebElement loginButtonInitial = driver.findElement(By.xpath("//span[contains(text(), 'Inicio Sesión')]"));
        loginButtonInitial.click();

        // Iniciar sesión
        WebElement emailField = driver.findElement(By.id("email"));
        WebElement passwordField = driver.findElement(By.id("password"));
        WebElement loginButton = driver.findElement(By.className("login_button"));

        emailField.sendKeys("chamoy@gmail.com");
        passwordField.sendKeys("12345678");
        loginButton.click();

        // Esperar a que la página cargue después del inicio de sesión
        Thread.sleep(2000);

        WebElement reportesver = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"html-dir-tag\"]/body/nav/div[1]/div[2]/div[1]/div[2]/div/div/div/ul/li[10]/a")));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", reportesver);

        Thread.sleep(2000);
        // Navegar a la sección de Reportes
        WebElement reportsMenu = driver.findElement(By.xpath("//*[@id=\"html-dir-tag\"]/body/nav/div[1]/div[2]/div[1]/div[2]/div/div/div/ul/li[10]/a"));
        reportsMenu.click();

        WebElement bestSellingProductsReports = driver.findElement(By.xpath("//*[@id=\"html-dir-tag\"]/body/nav/div[1]/div[2]/div[1]/div[2]/div/div/div/ul/li[10]/ul/li[5]"));
        bestSellingProductsReports.click();

        // Esperar a que la página de reportes cargue completamente
        Thread.sleep(2000);

        // Generar reporte anual
        generateAnnualReport();

        // Generar reporte del último mes
        generateLastMonthReport();

        // Generar reporte del mes actual
        generateThisMonthReport();

        // Generar reporte del 7 dias
        generate7Report();

        // Generar reporte de rango de fechas
        generateReportForDateRange("15/07/2024", "16/07/2024");
    }

    private void generateAnnualReport() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));

        // Seleccionar el botón de "Año"
        WebElement yearButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[contains(text(), 'year')]")));
        yearButton.click();

        // Esperar un momento para asegurarse de que la acción se complete
        Thread.sleep(1000); // Ajusta el tiempo según sea necesario

        // Hacer clic en el botón de exportar
        WebElement exportButton = driver.findElement(By.className("exportChartButton"));
        exportButton.click();


        // Esperar un momento para asegurarse de que el reporte se genere y descargue
        Thread.sleep(2000);

        // Aquí deberías agregar la verificación de que el archivo se descargó correctamente
    }

    private void generateLastMonthReport() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));

        // Seleccionar el botón "Último Mes"
        WebElement lastMonthButton = driver.findElement(By.xpath("//button[@class='nav-link chart-data ' and @value='last-month']"));
        lastMonthButton.click();

        // Esperar un momento para asegurarse de que la acción se complete
        Thread.sleep(1000);

        // Seleccionar el botón de exportar
        WebElement exportButton = driver.findElement(By.className("exportChartButton"));
        exportButton.click();

        // Esperar un momento para asegurarse de que el reporte se genere y descargue
        Thread.sleep(2000);

        // Aquí deberías agregar la verificación de que el archivo se descargó correctamente
    }

    private void generateThisMonthReport() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));

        // Seleccionar el botón "Este Mes"
        WebElement thisMonthButton = driver.findElement(By.xpath("//button[@class='nav-link chart-data ' and @value='this-month']"));
        thisMonthButton.click();

        // Esperar un momento para asegurarse de que la acción se complete
        Thread.sleep(1000);

        // Seleccionar el botón de exportar
        WebElement exportButton = driver.findElement(By.className("exportChartButton"));
        exportButton.click();

        // Esperar un momento para asegurarse de que el reporte se genere y descargue
        Thread.sleep(2000);
    }
    private void generate7Report() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));

        // Seleccionar el botón de "Año"
        WebElement last7DaysButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@class='nav-link chart-data ' and @value='seven-day']")));
        last7DaysButton.click();

        // Esperar un momento para asegurarse de que la acción se complete
        Thread.sleep(1000); // Ajusta el tiempo según sea necesario

        // Hacer clic en el botón de exportar
        WebElement exportButton = driver.findElement(By.className("exportChartButton"));
        exportButton.click();


        // Esperar un momento para asegurarse de que el reporte se genere y descargue
        Thread.sleep(2000);

        // Aquí deberías agregar la verificación de que el archivo se descargó correctamente
    }

    private void generateReportForDateRange(String startDate, String endDate) throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));

        // Ingresar la fecha de inicio
        WebElement startDateField = wait.until(ExpectedConditions.elementToBeClickable(By.name("start_date")));
        startDateField.clear();
        startDateField.sendKeys(formatDate(startDate));

        // Ingresar la fecha de fin
        WebElement endDateField = wait.until(ExpectedConditions.elementToBeClickable(By.name("end_date")));
        endDateField.clear();
        endDateField.sendKeys(formatDate(endDate));

        // Seleccionar el botón de generar
        WebElement generateButton = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".btn.btn-primary.label-margin.chart-data.generate_button")));
        generateButton.click();

        // Esperar un momento para asegurarse de que la acción se complete
        Thread.sleep(2000);

        // Seleccionar el botón de exportar
        WebElement exportButton = driver.findElement(By.className("exportChartButton"));
        exportButton.click();
        // Esperar un momento para asegurarse de que el reporte se genere y descargue
        Thread.sleep(2000);

        // Aquí deberías agregar la verificación de que el archivo se descargó correctamente
    }

    private String formatDate(String date) {
        DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate localDate = LocalDate.parse(date, inputFormatter);
        return localDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
    }
}
