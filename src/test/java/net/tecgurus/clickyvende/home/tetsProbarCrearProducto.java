package net.tecgurus.clickyvende.home;

import net.tecgurus.clickyvende.BaseTest;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.nio.file.Path;
import java.nio.file.Paths;

import java.time.Duration;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class tetsProbarCrearProducto extends BaseTest {

    private JavascriptExecutor js;

    @Test
    public void testCapturaProducto() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50)); // Aumentado el tiempo de espera

        driver.manage().window().maximize();
        // Navegar a la URL base
        driver.get(baseUrl);

        // Aceptar cookies si el botón está presente
        try {
            WebElement acceptCookiesButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[contains(text(), 'Aceptar')]")));
            acceptCookiesButton.click();
        } catch (TimeoutException e) {
            System.out.println("El botón de aceptar cookies no se encontró o ya se aceptaron.");
        }

        // Iniciar sesión
        WebElement loginButtonInitial = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(), 'Inicio Sesión')]")));
        loginButtonInitial.click();

        WebElement emailField = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("email")));
        WebElement passwordField = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("password")));
        WebElement loginButton = wait.until(ExpectedConditions.elementToBeClickable(By.className("login_button")));

        emailField.sendKeys("chamoy@gmail.com");
        passwordField.sendKeys("12345678");
        loginButton.click();

        // Navegar a la sección de Productos
        WebElement productosMenu = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(), 'Productos')]")));
        productosMenu.click();

        WebElement productoSubMenu = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(text(), 'Producto')]")));
        productoSubMenu.click();

        // Hacer clic en el botón para crear un nuevo producto
        WebElement nuevoProductoButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@data-title='Create New Product']")));
        nuevoProductoButton.click();

        // Ingresar información del producto
        WebElement nombreField = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("input[name='name']")));
        nombreField.sendKeys("Nombre del Producto");
        Thread.sleep(3001); // Espera de 3 segundos

        // Seleccionar categoría
        WebElement categoryDropdown = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div.choices[data-type='select-one']")));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", categoryDropdown);
        categoryDropdown.click();
        Thread.sleep(3000); // Espera de 3 segundos

        WebElement dropdownList = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.choices__list--dropdown")));
        List<WebElement> categoryOptions = dropdownList.findElements(By.cssSelector("div.choices__item--selectable"));

        boolean foundCategory = false;
        for (WebElement option : categoryOptions) {
            if (option.getText().equals("ROPA")) {
                option.click();
                foundCategory = true;
                break;
            }
        }

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 500);");
        Thread.sleep(2500);

        assertTrue(foundCategory, "No se encontró la categoría 'ROPA'");
        Thread.sleep(3000); // Espera de 3 segundos

        // Seleccionar subcategoría
        WebElement subcategoryDropdown = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".subcategory_id_div .choices__list--single")));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", subcategoryDropdown);
        subcategoryDropdown.click();
        Thread.sleep(3000); // Espera de 3 segundos

        WebElement subcategoryOption = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#choices--subcategory_id-item-choice-2")));
        subcategoryOption.click();
        Thread.sleep(3000); // Espera de 3 segundos

        // Seleccionar situación fiscal
        WebElement situacionFiscalDropdown = wait.until(ExpectedConditions.elementToBeClickable(By.name("tax_status")));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", situacionFiscalDropdown);
        situacionFiscalDropdown.click();
        Thread.sleep(3000); // Espera de 3 segundos

        WebElement situacionFiscalOptionNone = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("option[value='none']")));
        situacionFiscalOptionNone.click();
        Thread.sleep(3000); // Espera de 3 segundos

        // Abrir el menú desplegable de "Marca"
        WebElement brandDropdown = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#brand-dropdown ~ .choices__list--single")));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", brandDropdown);
        brandDropdown.click();
        Thread.sleep(3000); // Espera de 3 segundos

        WebElement nikeOption = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#choices--brand-dropdown-item-choice-4")));
        nikeOption.click();
        Thread.sleep(3000); // Espera de 3 segundos

        // Abrir el menú desplegable de "Etiqueta"
        WebElement etiquetaDropdown = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#label-dropdown ~ .choices__list--single")));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", etiquetaDropdown);
        etiquetaDropdown.click();
        Thread.sleep(3000); // Espera de 3 segundos


        WebElement walmartOption = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#choices--label-dropdown-item-choice-2")));
        walmartOption.click();
        Thread.sleep(3000); // Espera de 3 segundos

        // Abrir el menú desplegable de "Envío"
        WebElement envioDropdown = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#shipping_id ~ .choices__list--single")));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", envioDropdown);
        envioDropdown.click();
        Thread.sleep(3000); // Espera de 3 segundos

        WebElement temuOption = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#choices--shipping_id-item-choice-2")));
        temuOption.click();
        Thread.sleep(3000); // Espera de 3 segundos

        // Ingresar peso
        WebElement pesoField = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("input[name='product_weight']")));
        pesoField.sendKeys("1.5");
        Thread.sleep(2500);

        // Ingresar precio
        WebElement precioField = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("input[name='price']")));
        precioField.sendKeys("100");
        Thread.sleep(2500);

        // Ingresar precio de venta
        WebElement precioVentaField = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("input[name='sale_price']")));
        precioVentaField.sendKeys("90");
        Thread.sleep(2500);

        // Desplazar hasta el inicio de la página
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        // Localizar el recuadro para arrastrar y soltar la imagen
        WebElement dropzone = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.dropzone")));
        Thread.sleep(2500);

        // Ruta del archivo de imagen en el proyecto
        Path imagePath = Paths.get("src", "test", "resources", "Nike.jpeg");

        // Ejecutar el script para simular el arrastre y la liberación de la imagen
        ((JavascriptExecutor) driver).executeScript(
                "var dataTransfer = new DataTransfer();" +
                        "var file = new File([new Blob([''], {type: 'image/jpeg'})], arguments[0], { type: 'image/jpeg' });" +
                        "dataTransfer.items.add(file);" +
                        "var event = new DragEvent('drop', { dataTransfer: dataTransfer });" +
                        "arguments[1].dispatchEvent(event);",
                imagePath.toAbsolutePath().toString(), dropzone
        );
        Thread.sleep(2500);
        

//        // Localizar el campo de carga de imagen para "Imagen de portada de carga"
//        WebElement coverImageUpload = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("cover_image")));
//        coverImageUpload.sendKeys(imagePath.toAbsolutePath().toString());


        // Esperar y localizar el primer área editable (descripción del producto)
        WebElement descriptionField = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//div[@contenteditable='true'])[1]")));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", descriptionField);
        descriptionField.clear();
        descriptionField.sendKeys("Este es un producto de alta calidad, ideal para cualquier ocasión.");
        Thread.sleep(2500);

        // Esperar y localizar el segundo área editable (especificaciones del producto)
        WebElement specificationsField = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//div[@contenteditable='true'])[2]")));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", specificationsField);
        specificationsField.clear();
        specificationsField.sendKeys("Especificaciones: \n- Material: Algodón \n- Color: Negro \n- Tamaño: M, L, XL");
        Thread.sleep(2500);

        // Esperar y localizar el tercer área editable (detalles del producto)
        WebElement detailsField = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//div[@contenteditable='true'])[3]")));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", detailsField);
        detailsField.clear();
        detailsField.sendKeys("Detalles del producto: \n- Tipo: Deportivo \n- Marca: Nike \n- Material: Sintético \n- Garantía: 1 año");
        Thread.sleep(2500);

        // Desplazar hasta la parte superior de la página
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(5000);

        // Localizar el botón "Guardar" y hacer clic
        WebElement saveButton = wait.until(ExpectedConditions.elementToBeClickable(By.id("submit-all")));
        saveButton.click();
        Thread.sleep(2500);

//        // Confirmar que el producto fue creado con éxito
//        WebElement successMessage = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(text(), 'Producto creado con éxito')]")));
//        assertTrue(successMessage.isDisplayed(), "El mensaje de éxito no se mostró");
//
//        // Validar que el producto aparece en la lista de productos
//        WebElement productList = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(@class, 'product-list')]")));
//        assertTrue(productList.getText().contains("Nombre del Producto"), "El producto no se encuentra en la lista de productos");
    }
}