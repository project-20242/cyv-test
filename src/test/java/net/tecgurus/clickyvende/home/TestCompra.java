package net.tecgurus.clickyvende.home;

import net.tecgurus.clickyvende.BaseTest;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestCompra extends BaseTest {

    @Test
    public void testHacerCompra() throws InterruptedException {
        // Ingresar a la página
        LoginYVerTienda();

        // Comprar productos
        comprar();


    }
    //SE DEBE DE TENER UNA CUENTA YA CREADA QUE MODIFICARAS EN LOS CAMPOS DE ABAJO

    //SE DEBE TENER ACTIVADO STRIPE Y SI NO LO TIENES, ESTOS SON LOS CODIGOS

    //Clave publicable
    //pk_test_51I8wDiH92DQUN2aQR83DqlfWngUGe7tbW0lJfNcBStXBNul0s1EiLjLsCV1WK4ROWuQKdjq0hEJLaLeSubJSy9zR00SCwtNhoo

    //Clave secreta
    //sk_test_51I8wDiH92DQUN2aQu3Bbu7INic24tOYutZJ5pXZdCyv12iDILr2agncoMacK3Og3GiMMaebNMBo6dnPJsnfhlYtW00tMNOGI6T


    //PARA METODO DE ENVIO CREAR UN METODO DE ENVIO
    //COUNTRY DEBE DE SER ARGENTINA
    //STATE DEBE DE SER BUENOS AIRES


    private void LoginYVerTienda() throws InterruptedException {
        driver.manage().window().maximize();
        driver.get(baseUrl + "login");

        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20)); // Aumentamos el tiempo de espera

        // Definimos variables
        WebElement usernameInput = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("email")));
        WebElement passwordInput = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("password")));
        WebElement loginBtn = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".btn.btn-primary.btn-block.mt-2.login_button")));
        WebElement accept = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("c-p-bn")));

        Thread.sleep(2000);

        // Ingresamos las credenciales y hacemos clic en el botón de login
        usernameInput.sendKeys("phone@gmail.com");
        passwordInput.sendKeys("12345678");
        accept.click();
        loginBtn.click();

        WebElement boton = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".cust-btn > .ti-chevron-down")));
        boton.click();
        Thread.sleep(5000);
    }

    private void comprar() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20)); // Aumentamos el tiempo de espera

        String mainTab = driver.getWindowHandle();
        System.out.println("Main Tab:" + mainTab);

        driver.findElement(By.xpath("//*[@id=\"storeList\"]/a[2]/img")).click();
        Thread.sleep(3000);
        // Esperar hasta que la nueva ventana esté disponible y cambiar a ella
        wait.until(ExpectedConditions.numberOfWindowsToBe(2));

        Set<String> handles = driver.getWindowHandles();
        for (String actual : handles) {
            System.out.println("Handle ID:" + actual);
            if (!actual.equalsIgnoreCase(mainTab)) {
                System.out.println("Changing Tab");
                driver.switchTo().window(actual);

                // Realizar todas las acciones en la nueva ventana aquí


                Thread.sleep(2000);

                WebElement iniciarsesion = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/header/div[3]/div/div[2]/div[3]/ul/li[2]/a")));
                iniciarsesion.click();
                Thread.sleep(2000);


                //INICIAR SESION E LA TIENDA CON UNA CUENTA YA REGISTRADA


                //SE DEBE DE TENER UNA CUENTA YA REGISTRADA, CAMBIAR LOS CAMPOS POR LA QUE HAN CREADO
                WebElement emailField = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("email")));
                emailField.clear();
                Thread.sleep(2000);
                emailField.sendKeys("totodiaz0711@gmail.com");
                Thread.sleep(2000);

                WebElement passwordField = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("password")));
                passwordField.clear();
                Thread.sleep(2000);
                passwordField.sendKeys("12345678");
                Thread.sleep(2000);

                WebElement boton = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("login_button")));
                boton.click();
                Thread.sleep(2000);



                //Paso 1.1: El usuario navega a la página principal de la tienda.
                WebElement reselection = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".btn.btn-white")));
                reselection.click();
                Thread.sleep(2000);


                //Paso 1.2: El usuario puede buscar el producto usando la barra de búsqueda o navegar a través de las categorías.

                WebElement view = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/section[2]/div/div[2]/div[2]/div/div[1]/div[1]/div")));
                view.click();
                Thread.sleep(2000);



                //Paso 2.1: El usuario hace clic en el producto deseado para ver los detalles.

                //Paso 2.2: El usuario revisa la descripción, especificaciones, precio, y disponibilidad del producto.

                //Paso 2.3: El usuario selecciona la cantidad deseada (si es aplicable).
                WebElement addcantidad = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/section[1]/div/div/div[2]/div/div/div[4]/div[1]/form/div/div/button[2]")));
                addcantidad.click();
                Thread.sleep(2000);


                WebElement btnag = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/section[1]/div/div/div[2]/div/div/button")));
                btnag.click();
                Thread.sleep(7000);


                //Paso 4.1: El usuario hace clic en el icono del carrito para revisar los productos agregados.
                //Paso 4.2: El usuario verifica la cantidad, precio y puede eliminar o modificar la cantidad de productos.

                WebElement btncarrito = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/header/div[3]/div/div[2]/div[3]/ul/li[4]")));
                btncarrito.click();
                Thread.sleep(2000);



                //Paso 5.1: El usuario hace clic en "Proceder a la Compra" o "Checkout".
                WebElement btncheckout = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"cart-body\"]/div[2]/a[1]/center/button")));
                btncheckout.click();
                Thread.sleep(2000);

                WebElement btncheckout2 = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/section[1]/div/div[2]/div[2]/div/a")));
                btncheckout2.click();
                Thread.sleep(2000);



//                WebElement firstname = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("billing_info[firstname]")));
//                firstname.sendKeys("Ernesto");
//                Thread.sleep(2000);
//
//                WebElement lastname = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("billing_info[lastname]")));
//                lastname.sendKeys("Diaz");
//                Thread.sleep(2000);
//
//                WebElement email = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("billing_info[email]")));
//                email.sendKeys("toto0711@gmail.com");
//                Thread.sleep(2000);
//
//                WebElement telephone = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("billing_info[billing_user_telephone]")));
//                telephone.sendKeys("123456789");
//                Thread.sleep(2000);
//
//              //Paso 6.1: El usuario ingresa la dirección de envío.

                WebElement billing = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("billing_info[billing_address]")));
                billing.sendKeys("Sauces");
                Thread.sleep(2000);


                // DIRRECION DE FACTURACION
                WebElement dropdowncountry = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div.nice-select.form-control.country_change")));
                dropdowncountry.click();

                // Espera hasta que las opciones sean visibles y haz clic en la opción deseada usando JavaScript
                WebElement option = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("li.option[data-value='11']")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", option);


                WebElement cp = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("billing_info[billing_postecode]")));
                cp.sendKeys("55766");
                Thread.sleep(2000);



                WebElement dropdownstate = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div.nice-select.form-control.state_name.state_chage")));
                dropdownstate.click();
                Thread.sleep(2000);


                // Espera hasta que las opciones sean visibles y haz clic en la opción deseada usando JavaScript
                WebElement optionstate = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("li.option[data-value='3634']")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", optionstate);
                Thread.sleep(2000);



                WebElement dropdowncity = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div.nice-select.form-control.city_change")));
                dropdowncity.click();

                // Espera hasta que las opciones sean visibles y haz clic en la opción deseada usando JavaScript
                WebElement optioncity = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("li.option[data-value='1']")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", optioncity);



                // Entrega
                WebElement deliveryAddress = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("billing_info[delivery_address]")));
                deliveryAddress.sendKeys("Loma");
                Thread.sleep(2000);

                // Selección del país
                WebElement dropdownCountryDelivery = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//section[@class='Delivery_Address']//div[@class='nice-select form-control country_change']")));
                dropdownCountryDelivery.click();

                WebElement optionCountryDelivery = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//section[@class='Delivery_Address']//li[@data-value='11']")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", optionCountryDelivery);
                Thread.sleep(2000);

                WebElement cpadress = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("billing_info[delivery_postcode]")));
                cpadress.sendKeys("55766");
                Thread.sleep(2000);

                // Selección del estado
                WebElement dropdownStateDelivery = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//section[@class='Delivery_Address']//div[@class='nice-select form-control state_name state_chage delivery_list']")));
                dropdownStateDelivery.click();
                Thread.sleep(2000);

                WebElement optionStateDelivery = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//section[@class='Delivery_Address']//li[@data-value='3634']")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", optionStateDelivery);
                Thread.sleep(2000);

                // Selección de la ciudad
                WebElement dropdownCityDelivery = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//section[@class='Delivery_Address']//div[@class='nice-select form-control city_change delivery_list']")));
                dropdownCityDelivery.click();

                WebElement optionCityDelivery = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//section[@class='Delivery_Address']//li[@data-value='1']")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", optionCityDelivery);
                Thread.sleep(2000);


                // Esperar a que el botón "Continuar" esté presente y hacer clic en él usando JavascriptExecutor
                WebElement continuarCheckout = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".d-flex.acc-back-btn-wrp.align-items-center.justify-content-end .btn.continue-btn.confirm_btn.billing_done")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", continuarCheckout);
                Thread.sleep(2000);



                //Paso 7.1: El usuario selecciona el método de pago (tarjeta de crédito, PayPal, etc.).

                WebElement paymenttype = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"formdata\"]/div[1]/div[2]/div/div/div[2]/div/label")));
                paymenttype.click();
                Thread.sleep(2000);

                WebElement produdesta = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"formdata\"]/div[1]/div[2]/div/div/div[6]/div/label")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", produdesta);
                Thread.sleep(2000);

                WebElement terminosyc = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"formdata\"]/div[1]/div[2]/div/div/div[8]/div/div/label")));
                terminosyc.click();
                Thread.sleep(2000);

                // Esperar a que el botón "Continuar" esté presente y hacer clic en él usando JavascriptExecutor
                WebElement continueEnd = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"formdata\"]/div[1]/div[2]/div/div/div[8]/div/button")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", continueEnd);
                Thread.sleep(2000);

                // Esperar a que el formulario "payfast_form" esté presente y hacer clic en él usando JavascriptExecutor
                WebElement pagar = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"payfast_form\"]")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", pagar);
                Thread.sleep(2000);




                //YA EN STRIPE PARA PAGAR

                //Paso 7.2: El usuario ingresa los detalles del pago (número de tarjeta, fecha de vencimiento, CVV).


                WebElement element1 = driver.findElement(By.tagName("body"));
                String bodyText1 = element1.getText();
                assertTrue(bodyText1.contains("Pago con tarjeta"), "¡Texto no encontrado 1!");

                WebElement emailstripe = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("email")));
                emailstripe.sendKeys("totodiaz0711@gmail.com");
                Thread.sleep(2000);

                WebElement cardNumber = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("cardNumber")));
                cardNumber.sendKeys("4000000320000021");
                Thread.sleep(2000);

                WebElement cardExpiry = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("cardExpiry")));
                cardExpiry.sendKeys("0427");
                Thread.sleep(2000);


                WebElement cardCvc = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("cardCvc")));
                cardCvc.sendKeys("123");
                Thread.sleep(2000);

                WebElement billingName = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("billingName")));
                billingName.sendKeys("Ernesto Diaz");
                Thread.sleep(3000);


                //Paso 8.2: El usuario hace clic en "Confirmar Pedido" o "Realizar Pedido".

                WebElement submitbutton = wait.until(ExpectedConditions.presenceOfElementLocated(By.className("SubmitButton-IconContainer")));
                submitbutton.click();
                Thread.sleep(2000);

                WebElement volveralpanel = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/section/div/div/div/div/div[2]/a")));
                volveralpanel.click();
                Thread.sleep(2000);

                //Paso 10.1: El usuario puede acceder a la sección "Mis Pedidos" en su cuenta para hacer seguimiento al estado del pedido.

                WebElement miperfil = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(" /html/body/header/div[3]/div/div[2]/div[3]/ul/li[2]/a")));
                miperfil.click();
                Thread.sleep(2000);


                WebElement micuenta = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/header/div[3]/div/div[2]/div[3]/ul/li[2]/div/ul/li[1]/a")));
                micuenta.click();
                Thread.sleep(2000);


                WebElement historialpedidos = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"account-nav\"]/li[5]/a")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", historialpedidos);
                Thread.sleep(2000);


                WebElement verpedido = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"tab-5\"]/div[2]/table/tbody/tr[1]/td[7]/button")));
                verpedido.click();
                Thread.sleep(10000);


                WebElement element2 = driver.findElement(By.tagName("body"));
                String bodyText2 = element2.getText();
                assertTrue(bodyText2.contains("CANTIDAD"), "¡Texto no encontrado 2!");
            }

        }

    }


}
