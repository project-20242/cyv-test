package net.tecgurus.clickyvende.home;

import net.tecgurus.clickyvende.BaseTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.Random;

public class Planes extends BaseTest {

    private String nombreAleatorio;

    @Test
    public void pruebaPlanes() throws InterruptedException {
        driver.get(baseUrl);
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20));

        // Aceptar cookies si aparece el botón
        try {
            WebElement aceptarCookiesButton = driver.findElement(By.xpath("//button[contains(text(), 'Aceptar')]"));
            aceptarCookiesButton.click();
            Thread.sleep(1000);
        } catch (Exception e) {
            System.out.println("El botón de aceptar cookies no se encontró o ya se aceptaron.");
        }

        // Iniciar sesión
        WebElement inicioSesion = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(), 'Inicio Sesión')]")));
        Assertions.assertTrue(inicioSesion.isDisplayed(), "El botón de inicio de sesión no está visible.");
        Assertions.assertEquals("Inicio Sesión", inicioSesion.getText(), "El texto del botón de inicio de sesión no es el esperado.");
        inicioSesion.click();

        WebElement campoEmail = driver.findElement(By.id("email"));
        WebElement campoContrasena = driver.findElement(By.id("password"));
        WebElement botonLogin = driver.findElement(By.className("login_button"));

        campoEmail.sendKeys("superadmin@example.com");
        campoContrasena.sendKeys("12345678");
        botonLogin.click();
        Thread.sleep(2000);

        // Navegar a la sección de planes
        WebElement plan = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"html-dir-tag\"]/body/nav/div[1]/div[2]/div[1]/div[2]/div/div/div/ul/li[5]")));
        plan.click();
        Thread.sleep(2000);

        // Crear un nuevo plan
        WebElement crearPlan = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("a.btn-sm.btn-primary")));
        crearPlan.click();

        // Rellenar formulario de nuevo plan
        nombreAleatorio = generarNombreAleatorio(); // Almacena el nombre aleatorio generado
        rellenarFormularioPlan();

        // Volver a la lista de planes y verificar
        verificarPlanEnLista();
    }

    private String generarNombreAleatorio() {
        String caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        StringBuilder nombre = new StringBuilder();
        Random random = new Random();
        int longitud = 8; // Longitud del nombre aleatorio

        for (int i = 0; i < longitud; i++) {
            int indice = random.nextInt(caracteres.length());
            nombre.append(caracteres.charAt(indice));
        }

        return "Plan-" + nombre.toString();
    }

    private void rellenarFormularioPlan() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20));
        JavascriptExecutor js = (JavascriptExecutor) driver;

        try {
            // Usar el nombreAleatorio almacenado
            js.executeScript("document.getElementsByName('name')[0].value='" + nombreAleatorio + "';");
            js.executeScript("document.getElementsByName('price')[0].value='300';");

            // Manejar el campo de duración
            WebElement selectDuracion = driver.findElement(By.id("duration"));
            Select duracion = new Select(selectDuracion);
            duracion.selectByVisibleText("Per Month");

            js.executeScript("document.getElementsByName('max_stores')[0].value='5';");
            js.executeScript("document.getElementsByName('max_products')[0].value='50';");
            js.executeScript("document.getElementsByName('max_users')[0].value='12';");
            js.executeScript("document.getElementsByName('storage_limit')[0].value='500';");
            js.executeScript("document.getElementsByName('trial_days')[0].value='1';");

            // Desplazar y hacer clic en los checkboxes
            scrollYClick(By.id("enable_domain"));
            scrollYClick(By.id("enable_subdomain"));
            scrollYClick(By.id("enable_chatgpt"));
            scrollYClick(By.id("enable_tax"));
            scrollYClick(By.id("trial"));

            // Seleccionar temas
            js.executeScript("document.getElementsByName('themes[]')[0].click();");
            js.executeScript("document.getElementsByName('themes[]')[1].click();");

            // Desplazar hasta el campo de descripción y rellenar
            WebElement descripcion = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//textarea[@name='description']")));
            js.executeScript("arguments[0].scrollIntoView(true);", descripcion);
            Thread.sleep(500); // Esperar a que el scroll se complete
            js.executeScript("arguments[0].value='Nuevo plan, nueva experiencia, nuevos éxitos';", descripcion);

            // Intentar encontrar el botón "Crear" con diferentes selectores
            WebElement botonCrear = null;
            try {
                botonCrear = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@type='submit' and @value='Crear']")));
            } catch (TimeoutException e) {
                try {
                    botonCrear = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@type='submit' and @value='Create']")));
                } catch (TimeoutException ex) {
                    System.out.println("No se pudo encontrar el botón 'Crear' en ningún idioma.");
                    return;
                }
            }
            // Desplazar y hacer clic usando JavaScript
            js.executeScript("arguments[0].scrollIntoView(true);", botonCrear);

            js.executeScript("arguments[0].click();", botonCrear);

            Thread.sleep(1000);

        } catch (NoSuchElementException e) {
            System.out.println("Elemento no encontrado: " + e.getMessage());
        } catch (TimeoutException e) {
            System.out.println("Timeout esperando al elemento: " + e.getMessage());
        }
    }

    private void scrollYClick(By locator) throws InterruptedException {
        WebElement elemento = driver.findElement(locator);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView(true);", elemento);
        Thread.sleep(500);
        if (!elemento.isSelected()) {
            elemento.click();
        }
        Thread.sleep(2000);
    }

    private void verificarPlanEnLista() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20));

        try {
            WebElement botonVolver = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("a.btn.btn-secondary")));
            botonVolver.click();
        } catch (Exception e) {
            WebElement botonVolver = driver.findElement(By.cssSelector("btn btn-secondary btn-light me-2 ai-btn"));
            ((JavascriptExecutor) driver).executeScript("arguments[0].click();", botonVolver);
        }
        Thread.sleep(2000);

        // Buscar la tarjeta del plan basado en el nombre aleatorio
        WebElement tarjetaPlan = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(@class, 'plan_card') and .//span[contains(text(), '" + nombreAleatorio + "')]]")));
        Assertions.assertTrue(tarjetaPlan.isDisplayed(), "El nuevo plan no aparece en la lista de tarjetas.");

        WebElement precioPlan = tarjetaPlan.findElement(By.xpath(".//h3[contains(text(), '300')]"));
        WebElement duracionPlan = tarjetaPlan.findElement(By.xpath(".//p[contains(text(), '1')]"));
        WebElement tiendasMaximasPlan = tarjetaPlan.findElement(By.xpath(".//span[contains(text(), '5')]"));
        WebElement productosMaximosPlan = tarjetaPlan.findElement(By.xpath(".//span[contains(text(), '50')]"));
        WebElement usuariosMaximosPlan = tarjetaPlan.findElement(By.xpath(".//span[contains(text(), '12')]"));

        Assertions.assertTrue(precioPlan.isDisplayed(), "El precio del plan no es correcto.");
        Assertions.assertTrue(duracionPlan.isDisplayed(), "La duración del plan no es correcta.");
        Assertions.assertTrue(tiendasMaximasPlan.isDisplayed(), "El número de tiendas máximas no es correcto.");
        Assertions.assertTrue(productosMaximosPlan.isDisplayed(), "El número de productos máximos no es correcto.");
        Assertions.assertTrue(usuariosMaximosPlan.isDisplayed(), "El número de usuarios máximos no es correcto.");
    }
}
