package net.tecgurus.clickyvende.home;

import static org.junit.jupiter.api.Assertions.assertTrue;
import net.tecgurus.clickyvende.BaseTest;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.lang.reflect.Method;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Pattern;
import java.text.Normalizer;



public class testTemas extends BaseTest {
   private String baseUrl ="https://test.clickyvende.net/";
    public static void main(String[] args) throws InterruptedException {
        testTemas test = new testTemas();
        test.TemasPersonalizacionPrueba();
    }
    @Test
    public void TemasPersonalizacionPrueba() throws InterruptedException {

        System.setProperty("webdriver.chrome.driver", "C:\\laragon\\www\\cyv-test\\src\\test\\resources\\chromedriver.exe");

        // Configurar ChromeOptions
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        options.addArguments("--disable-gpu");
        options.addArguments("--no-sandbox");
        options.addArguments("--disable-dev-shm-usage");
        options.setExperimentalOption("useAutomationExtension", false);
        options.setExperimentalOption("excludeSwitches", new String[]{"enable-automation"});

        // Inicializar el controlador
        driver = new ChromeDriver(options);

        // Verificar si el driver se ha inicializado
        if (driver != null) {
            System.out.println("El driver se ha inicializado correctamente.");
        } else {
            System.out.println("El driver no se ha inicializado correctamente.");
        }

        // Preguntar cuántos temas probar antes de abrir el navegador
        int numberOfThemes = chooseQty();

        try {
            // Maximizar ventana del navegador
            driver.manage().window().maximize();

            // Navegar a la URL base
            driver.get(baseUrl);
            System.out.println("Navegando a la URL: " + baseUrl);

            // Aceptar cookies si el botón está presente
            acceptCookies();

            // Iniciar sesión
            login();

            // Ir a sección de temas
            chooseTheme();

            // Probar temas
            testThemes(numberOfThemes);

        } catch (Exception e) {
            System.out.println("Error durante la ejecución del navegador.");
            e.printStackTrace();
        } finally {
            // Cerrar el navegador al finalizar las pruebas o en caso de error
            if (driver != null) {
                System.out.println("Cerrando el navegador. Prueba finalizada");
                driver.quit();
            }
        }
//       { //temas
//        grocery();
//        barbecue();
//        babycare();
//        books();
//        diamond();
//        bakestore();
//        cosmeticdentistry();
//        coffee();
//        blossom();
//        eyewear();
//        fashion();
//        forhim();
//        foureyes();
//        foodstuffs();
//        game();
//        gameness();
//        garden();
//        generaltendency();
//        giftbox();
//        gifts();
//        giftsforher();
//        gifty();
//        glasses();
//        biking();
//        grocery2();
//        babycare2();
//        steps();
//        toothfairy();
//        timer();
//        trinkets();
//        twowheeler();
//        sheltering();
//        shoes();
//        spects();
//        style();
//        supermarket();
//        motorcycle();
//        mountains();
//        perfumes();
//        property();
//        sensation();
//        logisticsboxes();
//        landholdings();
//        mensonly();
//        minimarket();
//        homedecor();
//        housewares();
//        kidscare();
//        kitchen();
//        wristwatch();}
    }


    private void acceptCookies() {
        try {
            System.out.println("Aceptando cookies");
            WebElement acceptCookiesButton = driver.findElement(By.xpath("//button[contains(text(), 'Aceptar')]"));
            acceptCookiesButton.click();
            // Esperar a que el botón desaparezca
            new WebDriverWait(driver, Duration.ofSeconds(5))
                    .until(ExpectedConditions.invisibilityOf(acceptCookiesButton));
        } catch (Exception e) {
            System.out.println("El botón de aceptar cookies no se encontró o ya se aceptaron.");
        }
    }

    private void login() {
        System.out.println("inciando sesion");
        WebElement loginButtonInitial = driver.findElement(By.xpath("//span[contains(text(), 'Inicio Sesión')]"));
        loginButtonInitial.click();

        WebElement emailField = driver.findElement(By.id("email"));
        WebElement passwordField = driver.findElement(By.id("password"));
        WebElement loginButton = driver.findElement(By.className("login_button"));

        emailField.sendKeys("marinesolverara@gmail.com");
        passwordField.sendKeys("12345678");
        loginButton.click();

    }

    private void chooseTheme()   throws InterruptedException{
        System.out.println("Eligiendo tema");
        WebElement vistaPreviaButton = driver.findElement(By.xpath("//html[@id='html-dir-tag']/body/nav/div/div[2]/div/div[2]/div/div/div/ul/li[2]/a/span[2]"));
        vistaPreviaButton.click();

        Thread.sleep(1000);
    }

    private int chooseQty(){

        Scanner scanner = new Scanner(System.in);
        System.out.print("¿Cuántos temas deseas probar? ");
        int numberOfThemes = scanner.nextInt();
        scanner.close();  // Cerrar el escáner una vez que se obtenga la entrada del usuario
        return numberOfThemes;
    }

    private void testThemes(int numberOfThemes) {
        // Lista de nombres de temas
        String[] themes = {
                "grocery", "babycare","giftbox", "barbecue" , "books", "diamond", "bakestore",
                "cosmeticdentistry", "coffee", "blossom", "eyewear", "fashion", "forhim",
                "foureyes", "foodstuffs", "game", "gameness", "garden", "generaltendency",
                "gifts", "giftsforher", "gifty", "glasses", "biking", "grocery2", "babycare2",
                "steps", "toothfairy", "timer", "trinkets", "twowheeler", "sheltering", "shoes",
                "spects", "style", "supermarket", "motorcycle", "mountains", "perfumes",
                "property", "sensation", "logisticsboxes", "landholdings", "mensonly",
                "minimarket", "homedecor", "housewares", "kidscare", "kitchen", "wristwatch"
        };

        // Validar que el número de temas no exceda el tamaño del array
        List<String> successfulThemes = new ArrayList<>();


        for (int i = 0; i < numberOfThemes; i++) {
            try {
                // Obtener el método correspondiente al nombre del tema
                Method method = this.getClass().getMethod(themes[i]);

                // Invocar el método en la instancia actual
                System.out.println("Probando el tema: " + themes[i]);
                method.invoke(this);
                System.out.println("Prueba del tema '" + themes[i] + "' exitosa. Test Passed.");

                successfulThemes.add(themes[i]);

            } catch (NoSuchMethodException e) {
                System.out.println("Método no encontrado para el tema: " + themes[i]);
                e.printStackTrace();
            } catch (Exception e) {
                System.out.println("Error al probar el tema: " + themes[i]);
                e.printStackTrace();
            }
        }

        if (!successfulThemes.isEmpty()) {
            System.out.println("Prueba exitosa, los temas probados fueron: " + String.join(", ", successfulThemes));
        } else {
            System.out.println("No se probaron temas exitosamente.");
        }
    }

    public void grocery()  throws InterruptedException{



        //EMPIEZA ELEGIR TEMA  Y NAVEGA X EL Y REGRESA A LA PAGINAD EE ELGIR TEMA

        WebElement activarTemaButton = driver.findElement(By.cssSelector(".col-xl-3:nth-child(1) .d-inline > .btn"));
        activarTemaButton.click();

        Thread.sleep(9000);

        WebElement storeSelectionLink = driver.findElement(By.xpath("//a[contains(@class, 'cust-btn') and contains(@class, 'bg-warning')]"));
        storeSelectionLink.click();

        Thread.sleep(1000);

        WebElement targetElement = driver.findElement(By.xpath("//div[@id='storeList']/a[4]/img"));
        targetElement.click();

        Thread.sleep(1000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(originalWindow)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }

        WebElement spanElement = driver.findElement(By.xpath("//span[contains(.,'EN')]"));
        spanElement.click();

        Thread.sleep(1000);

        WebElement languageChangeLink = driver.findElement(By.xpath("//a[@href='https://test.clickyvende.net/change-language-store/es-mex']"));
        languageChangeLink.click();

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iconLabel = driver.findElement(By.cssSelector(".profile-header svg"));
        iconLabel.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        WebElement registroLink = findElementByTextInsensitive("a", "Registro");
        registroLink.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink = findElementByTextInsensitive("a", "Blog");
        blogLink.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink = findElementByTextInsensitive("a", "Colección");
        coleccionLink.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon = driver.findElement(By.cssSelector(".cart-header svg"));
        cartIcon.click();
        Thread.sleep(1000);

        WebElement closeCartIcon = driver.findElement(By.cssSelector(".closecart > svg"));
        closeCartIcon.click();
        Thread.sleep(1000);

        WebElement themeLogo = driver.findElement(By.cssSelector("h1 img"));
        themeLogo.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle = driver.getWindowHandle();
        assertTrue(currentWindowHandle.equals(originalWindow), "Failed to switch back to the original window.");
        Thread.sleep(2000);
    }

    public void barbecue()  throws InterruptedException {
        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 500);");
        Thread.sleep(2500);

        // Realizar la acción adicional
        WebElement activarTemaButton6 = driver.findElement(By.cssSelector(".col-xl-3:nth-child(6) .d-inline > .btn"));
        activarTemaButton6.click();

        Thread.sleep(9000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        String secondWindowHandle = driver.getWindowHandle();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(secondWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }


        driver.navigate().refresh();
        Thread.sleep(2000);

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iconLabel6 = driver.findElement(By.cssSelector(".profile-header svg"));
        iconLabel6.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        WebElement registroLink6 = findElementByTextInsensitive("a", "Registro");
        registroLink6.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement menuLink6 = driver.findElement(By.cssSelector(".menu-lnk:nth-child(3) > a"));
        menuLink6.click();
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);


        WebElement paginasLink6 = findElementByTextInsensitive("a", "Páginas");
        paginasLink6.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink6 = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink6.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink6 = findElementByTextInsensitive("a", "Blog");
        blogLink6.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink6 = findElementByTextInsensitive("a", "Colección");
        coleccionLink6.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon6 = driver.findElement(By.cssSelector(".cart-header svg"));
        cartIcon6.click();
        Thread.sleep(1000);

        WebElement closeCartIcon6 = driver.findElement(By.cssSelector(".closecart > svg"));
        closeCartIcon6.click();
        Thread.sleep(1000);

        WebElement themeLogo6 = driver.findElement(By.cssSelector("h1 img"));
        themeLogo6.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle6 = driver.getWindowHandle();
        assertTrue(currentWindowHandle6.equals(originalWindow), "Failed to switch back to the original window.");

    }

    public void babycare()  throws InterruptedException {

        // Realizar la acción adicional
        WebElement activarTemaButton2 = driver.findElement(By.cssSelector(".col-xl-3:nth-child(2) .d-inline > .btn"));
        activarTemaButton2.click();

        Thread.sleep(9000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        String secondWindowHandle = driver.getWindowHandle();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(secondWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }


        driver.navigate().refresh();
        Thread.sleep(2000);

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iconLabel2 = driver.findElement(By.cssSelector(".profile-header svg"));
        iconLabel2.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        WebElement registroLink2 = findElementByTextInsensitive("a", "Registro");
        registroLink2.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink2 = findElementByTextInsensitive("a", "Páginas");
        paginasLink2.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink2 = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink2.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink2 = findElementByTextInsensitive("a", "Blog");
        blogLink2.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink2 = findElementByTextInsensitive("a", "Colección");
        coleccionLink2.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon2 = driver.findElement(By.cssSelector(".cart-header svg"));
        cartIcon2.click();
        Thread.sleep(1000);

        WebElement closeCartIcon2 = driver.findElement(By.cssSelector(".closecart > svg"));
        closeCartIcon2.click();
        Thread.sleep(1000);

        WebElement themeLogo2 = driver.findElement(By.cssSelector("h1 img"));
        themeLogo2.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle2 = driver.getWindowHandle();
        assertTrue(currentWindowHandle2.equals(originalWindow), "Failed to switch back to the original window.");
    }

    public void books()  throws InterruptedException {

        // Realizar la acción adicional
        WebElement activarTemaButton3 = driver.findElement(By.cssSelector(".col-xl-3:nth-child(3) .d-inline > .btn"));
        activarTemaButton3.click();

        Thread.sleep(9000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        String secondWindowHandle = driver.getWindowHandle();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(secondWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }


        driver.navigate().refresh();
        Thread.sleep(2000);

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iconLabel3 = driver.findElement(By.cssSelector(".profile-header svg"));
        iconLabel3.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        WebElement registroLink3 = findElementByTextInsensitive("a", "Registro");
        registroLink3.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink3 = findElementByTextInsensitive("a", "Páginas");
        paginasLink3.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink3 = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink3.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink3 = findElementByTextInsensitive("a", "Blog");
        blogLink3.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink3 = findElementByTextInsensitive("a", "Colección");
        coleccionLink3.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon3 = driver.findElement(By.cssSelector(".cart-header svg"));
        cartIcon3.click();
        Thread.sleep(1000);

        WebElement closeCartIcon3 = driver.findElement(By.cssSelector(".closecart > svg"));
        closeCartIcon3.click();
        Thread.sleep(1000);

        WebElement themeLogo3 = driver.findElement(By.cssSelector("h1 img"));
        themeLogo3.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle3 = driver.getWindowHandle();
        assertTrue(currentWindowHandle3.equals(originalWindow), "Failed to switch back to the original window.");
    }

    public void diamond()  throws InterruptedException {

        // Realizar la acción adicional
        WebElement activarTemaButton4 = driver.findElement(By.cssSelector(".col-xl-3:nth-child(4) .d-inline > .btn"));
        activarTemaButton4.click();

        Thread.sleep(9000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        String secondWindowHandle = driver.getWindowHandle();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(secondWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }


        driver.navigate().refresh();
        Thread.sleep(2000);

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iconLabel4 = driver.findElement(By.cssSelector(".profile-header svg"));
        iconLabel4.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        WebElement registroLink4 = findElementByTextInsensitive("a", "Registro");
        registroLink4.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink4 = findElementByTextInsensitive("a", "Páginas");
        paginasLink4.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink4 = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink4.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink4 = findElementByTextInsensitive("a", "Blog");
        blogLink4.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink4 = findElementByTextInsensitive("a", "Colección");
        coleccionLink4.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon4 = driver.findElement(By.cssSelector(".cart-header svg"));
        cartIcon4.click();
        Thread.sleep(1000);

        WebElement closeCartIcon4 = driver.findElement(By.cssSelector(".closecart path"));
        closeCartIcon4.click();
        Thread.sleep(1000);

        WebElement themeLogo4 = driver.findElement(By.cssSelector(".desk-only img"));
        themeLogo4.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle4 = driver.getWindowHandle();
        assertTrue(currentWindowHandle4.equals(originalWindow), "Failed to switch back to the original window.");
    }

    public void bakestore()  throws InterruptedException {

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 500);");
        Thread.sleep(2500);

        // Realizar la acción adicional
        WebElement activarTemaButton5 = driver.findElement(By.cssSelector(".col-xl-3:nth-child(5) .d-inline > .btn"));
        activarTemaButton5.click();

        Thread.sleep(9000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        String secondWindowHandle = driver.getWindowHandle();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(secondWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }


        driver.navigate().refresh();
        Thread.sleep(2000);

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iniciosesionLink = findElementByTextInsensitive("a", "Iniciar Sesión");
        iniciosesionLink.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        WebElement registroLink5 = findElementByTextInsensitive("a", "Registro");
        registroLink5.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement menuLink = driver.findElement(By.cssSelector(".menu-lnk:nth-child(3) > a"));
        menuLink.click();
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink5 = findElementByTextInsensitive("a", "Páginas");
        paginasLink5.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink5 = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink5.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink5 = findElementByTextInsensitive("a", "Blog");
        blogLink5.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink5 = findElementByTextInsensitive("a", "Colección");
        coleccionLink5.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon5 = driver.findElement(By.cssSelector(".cart-header svg"));
        cartIcon5.click();
        Thread.sleep(1000);

        WebElement closeCartIcon5 = driver.findElement(By.cssSelector(".closecart path"));
        closeCartIcon5.click();
        Thread.sleep(1000);

        WebElement themeLogo5 = driver.findElement(By.cssSelector("h1 img"));
        themeLogo5.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle5 = driver.getWindowHandle();
        assertTrue(currentWindowHandle5.equals(originalWindow), "Failed to switch back to the original window.");
    }

    public void cosmeticdentistry()  throws InterruptedException {

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 500);");
        Thread.sleep(2500);

        // Realizar la acción adicional
        WebElement activarTemaButton8    = driver.findElement(By.cssSelector(".col-xl-3:nth-child(8) .d-inline > .btn"));
        activarTemaButton8.click();

        Thread.sleep(9000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        String secondWindowHandle = driver.getWindowHandle();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(secondWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }


        driver.navigate().refresh();
        Thread.sleep(2000);

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iconLabel8 = driver.findElement(By.cssSelector(".profile-header svg"));
        iconLabel8.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 550);");
        Thread.sleep(2500);

        WebElement registroLink8 = findElementByTextInsensitive("a", "Registro");
        registroLink8.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink8 = findElementByTextInsensitive("a", "Páginas");
        paginasLink8.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink8 = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink8.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink8 = findElementByTextInsensitive("a", "Blog");
        blogLink8.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink8 = findElementByTextInsensitive("a", "Colección");
        coleccionLink8.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon8 = driver.findElement(By.cssSelector(".cart-header svg"));
        cartIcon8.click();
        Thread.sleep(1000);

        WebElement closeCartIcon8 = driver.findElement(By.cssSelector(".closecart > svg"));
        closeCartIcon8.click();
        Thread.sleep(1000);

        WebElement themeLogo8 = driver.findElement(By.cssSelector("h1 img"));
        themeLogo8.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle8 = driver.getWindowHandle();
        assertTrue(currentWindowHandle8.equals(originalWindow), "Failed to switch back to the original window.");
    }

    public void coffee()  throws InterruptedException {

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 500);");
        Thread.sleep(2500);

        // Realizar la acción adicional
        WebElement activarTemaButton9 = driver.findElement(By.cssSelector(".col-xl-3:nth-child(9) .d-inline > .btn"));
        activarTemaButton9.click();

        Thread.sleep(9000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        String secondWindowHandle = driver.getWindowHandle();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(secondWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }


        driver.navigate().refresh();
        Thread.sleep(2000);

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iconLabel9 = driver.findElement(By.cssSelector(".profile-header svg"));
        iconLabel9.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 550);");
        Thread.sleep(2500);

        WebElement registroLink9 = findElementByTextInsensitive("a", "Registro");
        registroLink9.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink9 = findElementByTextInsensitive("a", "Páginas");
        paginasLink9.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink9 = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink9.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink9 = findElementByTextInsensitive("a", "Blog");
        blogLink9.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink9 = findElementByTextInsensitive("a", "Colección");
        coleccionLink9.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon9 = driver.findElement(By.cssSelector(".cart-header svg"));
        cartIcon9.click();
        Thread.sleep(1000);

        WebElement closeCartIcon9 = driver.findElement(By.cssSelector(".closecart > svg"));
        closeCartIcon9.click();
        Thread.sleep(1000);

        WebElement themeLogo9 = driver.findElement(By.cssSelector("h1 img"));
        themeLogo9.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle9 = driver.getWindowHandle();
        assertTrue(currentWindowHandle9.equals(originalWindow), "Failed to switch back to the original window.");
    }

    public void blossom()  throws InterruptedException {

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 500);");
        Thread.sleep(2500);

        // Realizar la acción adicional
        WebElement activarTemaButton10 = driver.findElement(By.cssSelector(".col-xl-3:nth-child(10) .d-inline > .btn"));
        activarTemaButton10.click();

        Thread.sleep(9000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        String secondWindowHandle = driver.getWindowHandle();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(secondWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }


        driver.navigate().refresh();
        Thread.sleep(2000);

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iconLabel10 = driver.findElement(By.cssSelector(".profile-header svg"));
        iconLabel10.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        WebElement registroLink10 = findElementByTextInsensitive("a", "Registro");
        registroLink10.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink10 = findElementByTextInsensitive("a", "Páginas");
        paginasLink10.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink10 = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink10.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink10 = findElementByTextInsensitive("a", "Blog");
        blogLink10.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink10 = findElementByTextInsensitive("a", "Colección");
        coleccionLink10.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon10 = driver.findElement(By.cssSelector(".cart-header svg"));
        cartIcon10.click();
        Thread.sleep(1000);

        WebElement closeCartIcon10 = driver.findElement(By.cssSelector(".closecart > svg"));
        closeCartIcon10.click();
        Thread.sleep(1000);

        WebElement themeLogo10 = driver.findElement(By.cssSelector("h1 img"));
        themeLogo10.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle10 = driver.getWindowHandle();
        assertTrue(currentWindowHandle10.equals(originalWindow), "Failed to switch back to the original window.");
    }

    public void eyewear()  throws InterruptedException {

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 500);");
        Thread.sleep(2500);

        // Realizar la acción adiciona
        WebElement activarTemaButton11 = driver.findElement(By.cssSelector(".col-xl-3:nth-child(11) .d-inline > .btn"));
        activarTemaButton11.click();

        Thread.sleep(9000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        String secondWindowHandle = driver.getWindowHandle();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(secondWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }


        driver.navigate().refresh();
        Thread.sleep(2000);

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iconLabel11 = driver.findElement(By.cssSelector(".profile-header svg"));
        iconLabel11.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        WebElement registroLink11 = findElementByTextInsensitive("a", "Registro");
        registroLink11.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink11 = findElementByTextInsensitive("a", "Páginas");
        paginasLink11.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink11 = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink11.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink11 = findElementByTextInsensitive("a", "Blog");
        blogLink11.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink11 = findElementByTextInsensitive("a", "Colección");
        coleccionLink11.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon11 = driver.findElement(By.cssSelector(".cart-header svg"));
        cartIcon11.click();
        Thread.sleep(1000);

        WebElement closeCartIcon11 = driver.findElement(By.cssSelector(".closecart path"));
        closeCartIcon11.click();
        Thread.sleep(1000);

        WebElement themeLogo11 = driver.findElement(By.cssSelector("h1 img"));
        themeLogo11.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle11 = driver.getWindowHandle();
        assertTrue(currentWindowHandle11.equals(originalWindow), "Failed to switch back to the original window.");
    }

    public void fashion()  throws InterruptedException {

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 1050);");
        Thread.sleep(2500);

        // Realizar la acción adicional
        WebElement activarTemaButton13 = driver.findElement(By.cssSelector(".col-xl-3:nth-child(13) .d-inline > .btn"));
        activarTemaButton13.click();

        Thread.sleep(9000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        String secondWindowHandle = driver.getWindowHandle();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(secondWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }


        driver.navigate().refresh();
        Thread.sleep(2000);

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iconLabel13 = driver.findElement(By.cssSelector(".profile-header svg"));
        iconLabel13.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        WebElement registroLink13 = findElementByTextInsensitive("a", "Registro");
        registroLink13.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink13 = findElementByTextInsensitive("a", "Páginas");
        paginasLink13.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink13 = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink13.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink13 = findElementByTextInsensitive("a", "Blog");
        blogLink13.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink13 = findElementByTextInsensitive("a", "Colección");
        coleccionLink13.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon13 = driver.findElement(By.cssSelector(".cart-header svg"));
        cartIcon13.click();
        Thread.sleep(1000);

        WebElement closeCartIcon13 = driver.findElement(By.cssSelector(".closecart > svg"));
        closeCartIcon13.click();
        Thread.sleep(1000);

        WebElement themeLogo13 = driver.findElement(By.cssSelector("h1 img"));
        themeLogo13.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle13 = driver.getWindowHandle();
        assertTrue(currentWindowHandle13.equals(originalWindow), "Failed to switch back to the original window.");
    }

    public void forhim()  throws InterruptedException {

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 1050);");
        Thread.sleep(2500);

        // Realizar la acción adicional
        WebElement activarTemaButton14 = driver.findElement(By.cssSelector(".col-xl-3:nth-child(14) .d-inline > .btn"));
        activarTemaButton14.click();

        Thread.sleep(9000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        String secondWindowHandle = driver.getWindowHandle();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(secondWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }


        driver.navigate().refresh();
        Thread.sleep(2000);

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iconLabel14 = driver.findElement(By.cssSelector(".profile-header svg"));
        iconLabel14.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        WebElement registroLink14 = findElementByTextInsensitive("a", "Registro");
        registroLink14.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink14 = findElementByTextInsensitive("a", "Páginas");
        paginasLink14.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink14 = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink14.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink14 = findElementByTextInsensitive("a", "Blog");
        blogLink14.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink14 = findElementByTextInsensitive("a", "Colección");
        coleccionLink14.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon14 = driver.findElement(By.cssSelector(".cart-header svg"));
        cartIcon14.click();
        Thread.sleep(1000);

        WebElement closeCartIcon14 = driver.findElement(By.cssSelector(".closecart > svg"));
        closeCartIcon14.click();
        Thread.sleep(1000);

        WebElement themeLogo14 = driver.findElement(By.cssSelector(".logo-col:nth-child(3) img"));
        themeLogo14.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle14 = driver.getWindowHandle();
        assertTrue(currentWindowHandle14.equals(originalWindow), "Failed to switch back to the original window.");
    }

    public void foureyes()  throws InterruptedException {

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 1050);");
        Thread.sleep(2500);

        // Realizar la acción adicional
        WebElement activarTemaButton15 = driver.findElement(By.cssSelector(".col-xl-3:nth-child(15) .d-inline > .btn"));
        activarTemaButton15.click();

        Thread.sleep(9000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        String secondWindowHandle = driver.getWindowHandle();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(secondWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }


        driver.navigate().refresh();
        Thread.sleep(2000);

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iconLabel15 = driver.findElement(By.cssSelector(".profile-header svg"));
        iconLabel15.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        WebElement registroLink15 = findElementByTextInsensitive("a", "Registro");
        registroLink15.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink15 = findElementByTextInsensitive("a", "Páginas");
        paginasLink15.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink15 = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink15.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink15 = findElementByTextInsensitive("a", "Blog");
        blogLink15.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink15 = findElementByTextInsensitive("a", "Colección");
        coleccionLink15.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon15 = driver.findElement(By.cssSelector(".cart-header svg"));
        cartIcon15.click();
        Thread.sleep(1000);

        WebElement closeCartIcon15 = driver.findElement(By.cssSelector(".closecart > svg"));
        closeCartIcon15.click();
        Thread.sleep(1000);

        WebElement themeLogo15 = driver.findElement(By.cssSelector(".logo-col:nth-child(3) img"));
        themeLogo15.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle15 = driver.getWindowHandle();
        assertTrue(currentWindowHandle15.equals(originalWindow), "Failed to switch back to the original window.");
    }

    public void foodstuffs()  throws InterruptedException {

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 1050);");
        Thread.sleep(2500);

        // Realizar la acción adicional
        WebElement activarTemaButton16 = driver.findElement(By.cssSelector(".col-xl-3:nth-child(16) .d-inline > .btn"));
        activarTemaButton16.click();

        Thread.sleep(9000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        String secondWindowHandle = driver.getWindowHandle();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(secondWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }


        driver.navigate().refresh();
        Thread.sleep(2000);

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iconLabel16 = driver.findElement(By.cssSelector(".profile-header svg"));
        iconLabel16.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        WebElement registroLink16 = findElementByTextInsensitive("a", "Registro");
        registroLink16.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink16 = findElementByTextInsensitive("a", "Páginas");
        paginasLink16.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink16 = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink16.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink16 = findElementByTextInsensitive("a", "Blog");
        blogLink16.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink16 = findElementByTextInsensitive("a", "Colección");
        coleccionLink16.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon16 = driver.findElement(By.cssSelector(".cart-header svg"));
        cartIcon16.click();
        Thread.sleep(1000);

        WebElement closeCartIcon16 = driver.findElement(By.cssSelector(".closecart > svg"));
        closeCartIcon16.click();
        Thread.sleep(1000);

        WebElement themeLogo16 = driver.findElement(By.cssSelector("h1 img"));
        themeLogo16.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle16 = driver.getWindowHandle();
        assertTrue(currentWindowHandle16.equals(originalWindow), "Failed to switch back to the original window.");
    }

    public void game()  throws InterruptedException {

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 1050);");
        Thread.sleep(2500);

        // Realizar la acción adicional
        WebElement activarTemaButton18 = driver.findElement(By.cssSelector(".col-xl-3:nth-child(18) .d-inline > .btn"));
        activarTemaButton18.click();

        Thread.sleep(9000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        String secondWindowHandle = driver.getWindowHandle();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(secondWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }


        driver.navigate().refresh();
        Thread.sleep(2000);

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iconLabel18 = driver.findElement(By.cssSelector(".profile-header svg"));
        iconLabel18.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 550);");
        Thread.sleep(2500);

        WebElement registroLink18 = findElementByTextInsensitive("a", "Registro");
        registroLink18.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink18 = findElementByTextInsensitive("a", "Páginas");
        paginasLink18.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink18 = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink18.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink18 = findElementByTextInsensitive("a", "Blog");
        blogLink18.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink18 = findElementByTextInsensitive("a", "Colección");
        coleccionLink18.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon18 = driver.findElement(By.cssSelector(".cart-header svg"));
        cartIcon18.click();
        Thread.sleep(1000);

        WebElement closeCartIcon18 = driver.findElement(By.cssSelector(".closecart > svg"));
        closeCartIcon18.click();
        Thread.sleep(1000);

        WebElement themeLogo18 = driver.findElement(By.cssSelector("h1 img"));
        themeLogo18.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle18 = driver.getWindowHandle();
        assertTrue(currentWindowHandle18.equals(originalWindow), "Failed to switch back to the original window.");
    }

    public void gameness()  throws InterruptedException {

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 1050);");
        Thread.sleep(2500);

        // Realizar la acción adicional
        WebElement activarTemaButton19 = driver.findElement(By.cssSelector(".col-xl-3:nth-child(19) .d-inline > .btn"));
        activarTemaButton19.click();

        Thread.sleep(9000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        String secondWindowHandle = driver.getWindowHandle();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(secondWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }


        driver.navigate().refresh();
        Thread.sleep(2000);

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iconLabel19 = driver.findElement(By.cssSelector(".profile-header svg"));
        iconLabel19.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        WebElement registroLink19 = findElementByTextInsensitive("a", "Registro");
        registroLink19.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink19 = findElementByTextInsensitive("a", "Páginas");
        paginasLink19.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink19 = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink19.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink19 = findElementByTextInsensitive("a", "Blog");
        blogLink19.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink19 = findElementByTextInsensitive("a", "Colección");
        coleccionLink19.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon19 = driver.findElement(By.cssSelector(".cart-header svg"));
        cartIcon19.click();
        Thread.sleep(1000);

        WebElement closeCartIcon19 = driver.findElement(By.cssSelector(".closecart path"));
        closeCartIcon19.click();
        Thread.sleep(1000);

        WebElement themeLogo19 = driver.findElement(By.cssSelector("h1 img"));
        themeLogo19.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle19 = driver.getWindowHandle();
        assertTrue(currentWindowHandle19.equals(originalWindow), "Failed to switch back to the original window.");
    }

    public void garden()  throws InterruptedException {

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 1050);");
        Thread.sleep(2500);

        // Realizar la acción adicional
        WebElement activarTemaButton20 = driver.findElement(By.cssSelector(".col-xl-3:nth-child(20) .d-inline > .btn"));
        activarTemaButton20.click();

        Thread.sleep(9000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        String secondWindowHandle = driver.getWindowHandle();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(secondWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }


        driver.navigate().refresh();
        Thread.sleep(2000);

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iconLabel20 = driver.findElement(By.cssSelector(".profile-header svg"));
        iconLabel20.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        WebElement registroLink20 = findElementByTextInsensitive("a", "Registro");
        registroLink20.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink20 = findElementByTextInsensitive("a", "Páginas");
        paginasLink20.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink20 = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink20.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink20 = findElementByTextInsensitive("a", "Blog");
        blogLink20.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink20 = findElementByTextInsensitive("a", "Colección");
        coleccionLink20.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon20 = driver.findElement(By.cssSelector(".cart-count > svg"));
        cartIcon20.click();
        Thread.sleep(1000);

        WebElement closeCartIcon20 = driver.findElement(By.cssSelector(".closecart > svg"));
        closeCartIcon20.click();
        Thread.sleep(1000);

        WebElement themeLogo20 = driver.findElement(By.cssSelector("h1 img"));
        themeLogo20.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle20 = driver.getWindowHandle();
        assertTrue(currentWindowHandle20.equals(originalWindow), "Failed to switch back to the original window.");
    }

    public void generaltendency()  throws InterruptedException {

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 1650);");
        Thread.sleep(2500);

        // Realizar la acción adicional
        WebElement activarTemaButton21 = driver.findElement(By.cssSelector(".col-xl-3:nth-child(21) .d-inline > .btn"));
        activarTemaButton21.click();

        Thread.sleep(9000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        String secondWindowHandle = driver.getWindowHandle();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(secondWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }


        driver.navigate().refresh();
        Thread.sleep(2000);

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iconLabel21 = driver.findElement(By.cssSelector(".profile-header svg"));
        iconLabel21.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        WebElement registroLink21 = findElementByTextInsensitive("a", "Registro");
        registroLink21.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink21 = findElementByTextInsensitive("a", "Páginas");
        paginasLink21.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink21 = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink21.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink21 = findElementByTextInsensitive("a", "Blog");
        blogLink21.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink21 = findElementByTextInsensitive("a", "Colección");
        coleccionLink21.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon21 = driver.findElement(By.cssSelector(".cart-header svg"));
        cartIcon21.click();
        Thread.sleep(1000);

        WebElement closeCartIcon21 = driver.findElement(By.cssSelector(".closecart > svg"));
        closeCartIcon21.click();
        Thread.sleep(1000);

        WebElement themeLogo21 = driver.findElement(By.cssSelector(".h3 > a"));
        themeLogo21.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle21 = driver.getWindowHandle();
        assertTrue(currentWindowHandle21.equals(originalWindow), "Failed to switch back to the original window.");
    }

    public void giftbox()  throws InterruptedException {

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 1650);");
        Thread.sleep(2500);

        // Realizar la acción adicional
        WebElement activarTemaButton22 = driver.findElement(By.cssSelector(".col-xl-3:nth-child(22) .d-inline > .btn"));
        activarTemaButton22.click();

        Thread.sleep(9000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        String secondWindowHandle = driver.getWindowHandle();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(secondWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }


        driver.navigate().refresh();
        Thread.sleep(2000);

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iconLabel22 = driver.findElement(By.cssSelector(".profile-header svg"));
        iconLabel22.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        WebElement registroLink22 = findElementByTextInsensitive("a", "Registro");
        registroLink22.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink22 = findElementByTextInsensitive("a", "Páginas");
        paginasLink22.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink22 = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink22.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink22 = findElementByTextInsensitive("a", "Blog");
        blogLink22.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink22 = findElementByTextInsensitive("a", "Colección");
        coleccionLink22.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon22 = driver.findElement(By.cssSelector(".cart-header svg"));
        cartIcon22.click();
        Thread.sleep(1000);

        WebElement closeCartIcon22 = driver.findElement(By.cssSelector(".closecart > svg"));
        closeCartIcon22.click();
        Thread.sleep(1000);

        WebElement themeLogo22 = driver.findElement(By.cssSelector("h1 img"));
        themeLogo22.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle22 = driver.getWindowHandle();
        assertTrue(currentWindowHandle22.equals(originalWindow), "Failed to switch back to the original window.");
    }

    public void gifts()  throws InterruptedException {

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 1650);");
        Thread.sleep(2500);

        // Realizar la acción adicional
        WebElement activarTemaButton23 = driver.findElement(By.cssSelector(".col-xl-3:nth-child(23) .d-inline > .btn"));
        activarTemaButton23.click();

        Thread.sleep(9000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        String secondWindowHandle = driver.getWindowHandle();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(secondWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }


        driver.navigate().refresh();
        Thread.sleep(2000);

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iconLabel23 = driver.findElement(By.cssSelector(".profile-header svg"));
        iconLabel23.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 550);");
        Thread.sleep(2500);

        WebElement registroLink23 = findElementByTextInsensitive("a", "Registro");
        registroLink23.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink23 = findElementByTextInsensitive("a", "Páginas");
        paginasLink23.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink23 = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink23.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink23 = findElementByTextInsensitive("a", "Blog");
        blogLink23.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink23 = findElementByTextInsensitive("a", "Colección");
        coleccionLink23.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon23 = driver.findElement(By.cssSelector(".cart-header svg"));
        cartIcon23.click();
        Thread.sleep(1000);

        WebElement closeCartIcon23 = driver.findElement(By.cssSelector(".closecart > svg"));
        closeCartIcon23.click();
        Thread.sleep(1000);

        WebElement themeLogo23 = driver.findElement(By.cssSelector("h1 img"));
        themeLogo23.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle23 = driver.getWindowHandle();
        assertTrue(currentWindowHandle23.equals(originalWindow), "Failed to switch back to the original window.");
    }

    public void giftsforher()  throws InterruptedException {

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 1650);");
        Thread.sleep(2500);

        // Realizar la acción adicional
        WebElement activarTemaButton24 = driver.findElement(By.cssSelector(".col-xl-3:nth-child(24) .d-inline > .btn"));
        activarTemaButton24.click();

        Thread.sleep(9000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        String secondWindowHandle = driver.getWindowHandle();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(secondWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }


        driver.navigate().refresh();
        Thread.sleep(2000);

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iconLabel24 = driver.findElement(By.cssSelector(".profile-header svg"));
        iconLabel24.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 480);");
        Thread.sleep(2500);

        WebElement registroLink24 = findElementByTextInsensitive("a", "Registro");
        registroLink24.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink24 = findElementByTextInsensitive("a", "Páginas");
        paginasLink24.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink24 = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink24.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink24 = findElementByTextInsensitive("a", "Blog");
        blogLink24.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink24 = findElementByTextInsensitive("a", "Colección");
        coleccionLink24.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon24 = driver.findElement(By.cssSelector(".cart-header svg"));
        cartIcon24.click();
        Thread.sleep(1000);

        WebElement closeCartIcon24 = driver.findElement(By.cssSelector(".closecart > svg"));
        closeCartIcon24.click();
        Thread.sleep(1000);

        WebElement themeLogo24 = driver.findElement(By.cssSelector("h1 img"));
        themeLogo24.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle24 = driver.getWindowHandle();
        assertTrue(currentWindowHandle24.equals(originalWindow), "Failed to switch back to the original window.");
    }

    public void gifty()  throws InterruptedException {

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 1650);");
        Thread.sleep(2500);

        // Realizar la acción adicional
        WebElement activarTemaButton25 = driver.findElement(By.cssSelector(".col-xl-3:nth-child(25) .d-inline > .btn"));
        activarTemaButton25.click();

        Thread.sleep(9000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        String secondWindowHandle = driver.getWindowHandle();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(secondWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }


        driver.navigate().refresh();
        Thread.sleep(2000);

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iconLabel25 = driver.findElement(By.cssSelector(".profile-header svg"));
        iconLabel25.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 500);");
        Thread.sleep(2500);

        WebElement registroLink25 = findElementByTextInsensitive("a", "Registro");
        registroLink25.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink25 = findElementByTextInsensitive("a", "Páginas");
        paginasLink25.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink25 = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink25.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink25 = findElementByTextInsensitive("a", "Blog");
        blogLink25.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink25 = findElementByTextInsensitive("a", "Colección");
        coleccionLink25.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon25 = driver.findElement(By.cssSelector(".cart-header svg"));
        cartIcon25.click();
        Thread.sleep(1000);

        WebElement closeCartIcon25 = driver.findElement(By.cssSelector(".closecart path"));
        closeCartIcon25.click();
        Thread.sleep(1000);

        WebElement themeLogo25 = driver.findElement(By.cssSelector("h1 img"));
        themeLogo25.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle25 = driver.getWindowHandle();
        assertTrue(currentWindowHandle25.equals(originalWindow), "Failed to switch back to the original window.");
    }

    public void glasses()  throws InterruptedException{

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 1650);");
        Thread.sleep(2500);

        // Realizar la acción adicional
        WebElement activarTemaButton26 = driver.findElement(By.cssSelector(".col-xl-3:nth-child(26) .d-inline > .btn"));
        activarTemaButton26.click();

        Thread.sleep(9000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        String secondWindowHandle = driver.getWindowHandle();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(secondWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }


        driver.navigate().refresh();
        Thread.sleep(2000);

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iconLabel26 = driver.findElement(By.cssSelector(".profile-header svg"));
        iconLabel26.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 500);");
        Thread.sleep(2500);

        WebElement registroLink26 = findElementByTextInsensitive("a", "Registro");
        registroLink26.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink26 = findElementByTextInsensitive("a", "Páginas");
        paginasLink26.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink26 = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink26.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink26 = findElementByTextInsensitive("a", "Blog");
        blogLink26.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink26 = findElementByTextInsensitive("a", "Colección");
        coleccionLink26.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon26 = driver.findElement(By.cssSelector(".cart-header svg"));
        cartIcon26.click();
        Thread.sleep(1000);

        WebElement closeCartIcon26 = driver.findElement(By.cssSelector(".closecart > svg"));
        closeCartIcon26.click();
        Thread.sleep(1000);

        WebElement themeLogo26 = driver.findElement(By.cssSelector("h1 img"));
        themeLogo26.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle26 = driver.getWindowHandle();
        assertTrue(currentWindowHandle26.equals(originalWindow), "Failed to switch back to the original window.");
    }

    public void biking()  throws InterruptedException{

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 1650);");
        Thread.sleep(2500);

        // Realizar la acción adicional
        WebElement activarTemaButton27 = driver.findElement(By.cssSelector(".col-xl-3:nth-child(27) .d-inline > .btn"));
        activarTemaButton27.click();

        Thread.sleep(9000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        String secondWindowHandle = driver.getWindowHandle();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(secondWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }


        driver.navigate().refresh();
        Thread.sleep(2000);

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iconLabel27 = driver.findElement(By.cssSelector(".profile-header svg"));
        iconLabel27.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        WebElement registroLink27 = findElementByTextInsensitive("a", "Registro");
        registroLink27.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink27 = findElementByTextInsensitive("a", "Páginas");
        paginasLink27.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink27 = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink27.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink27 = findElementByTextInsensitive("a", "Blog");
        blogLink27.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink27 = findElementByTextInsensitive("a", "Colección");
        coleccionLink27.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon27 = driver.findElement(By.cssSelector(".cart-header svg"));
        cartIcon27.click();
        Thread.sleep(1000);

        WebElement closeCartIcon27 = driver.findElement(By.cssSelector(".closecart path"));
        closeCartIcon27.click();
        Thread.sleep(1000);

        WebElement themeLogo27 = driver.findElement(By.cssSelector(".logo-col:nth-child(3) img"));
        themeLogo27.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle27 = driver.getWindowHandle();
        assertTrue(currentWindowHandle27.equals(originalWindow), "Failed to switch back to the original window.");
    }

    public void grocery2()  throws InterruptedException{


        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 500);");
        Thread.sleep(2500);

        // Realizar la acción adicional
        WebElement activarTemaButton6 = driver.findElement(By.cssSelector(".col-xl-3:nth-child(6) .d-inline > .btn"));
        activarTemaButton6.click();

        Thread.sleep(9000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 1650);");
        Thread.sleep(2500);

        WebElement activarTemaButton28 = driver.findElement(By.cssSelector(".col-xl-3:nth-child(28) .d-inline > .btn"));
        activarTemaButton28.click();

        Thread.sleep(9000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        String secondWindowHandle = driver.getWindowHandle();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(secondWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }


        driver.navigate().refresh();
        Thread.sleep(2000);

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iconLabel28 = driver.findElement(By.cssSelector(".profile-header svg"));
        iconLabel28.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        WebElement registroLink28 = findElementByTextInsensitive("a", "Registro");
        registroLink28.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink28 = findElementByTextInsensitive("a", "Páginas");
        paginasLink28.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink28 = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink28.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink28 = findElementByTextInsensitive("a", "Blog");
        blogLink28.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink28 = findElementByTextInsensitive("a", "Colección");
        coleccionLink28.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon28 = driver.findElement(By.cssSelector(".cart-header svg"));
        cartIcon28.click();
        Thread.sleep(1000);

        WebElement closeCartIcon28 = driver.findElement(By.cssSelector(".closecart > svg"));
        closeCartIcon28.click();
        Thread.sleep(1000);

        WebElement themeLogo28 = driver.findElement(By.cssSelector("h1 img"));
        themeLogo28.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle28 = driver.getWindowHandle();
        assertTrue(currentWindowHandle28.equals(originalWindow), "Failed to switch back to the original window.");
    }

    public void babycare2()  throws InterruptedException{

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 2200);");
        Thread.sleep(2500);

        // Realizar la acción adicional
        WebElement activarTemaButton29 = driver.findElement(By.cssSelector(".col-xl-3:nth-child(29) .d-inline > .btn"));
        activarTemaButton29.click();

        Thread.sleep(9000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        String secondWindowHandle = driver.getWindowHandle();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(secondWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }


        driver.navigate().refresh();
        Thread.sleep(2000);

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iconLabel29 = driver.findElement(By.cssSelector(".profile-header svg"));
        iconLabel29.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        WebElement registroLink29 = findElementByTextInsensitive("a", "Registro");
        registroLink29.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink29 = findElementByTextInsensitive("a", "Páginas");
        paginasLink29.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink29 = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink29.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink29 = findElementByTextInsensitive("a", "Blog");
        blogLink29.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink29 = findElementByTextInsensitive("a", "Colección");
        coleccionLink29.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon29 = driver.findElement(By.cssSelector(".cart-header svg"));
        cartIcon29.click();
        Thread.sleep(1000);

        WebElement closeCartIcon29 = driver.findElement(By.cssSelector(".closecart > svg"));
        closeCartIcon29.click();
        Thread.sleep(1000);

        WebElement themeLogo29 = driver.findElement(By.cssSelector("h1 img"));
        themeLogo29.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle29 = driver.getWindowHandle();
        assertTrue(currentWindowHandle29.equals(originalWindow), "Failed to switch back to the original window.");
    }

    public void steps()  throws InterruptedException {

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 2200);");
        Thread.sleep(2500);

        // Realizar la acción adicional
        WebElement activarTemaButton30 = driver.findElement(By.cssSelector(".col-xl-3:nth-child(30) .d-inline > .btn"));
        activarTemaButton30.click();

        Thread.sleep(9000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        String secondWindowHandle = driver.getWindowHandle();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(secondWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }


        driver.navigate().refresh();
        Thread.sleep(2000);

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iconLabel30 = driver.findElement(By.cssSelector(".menu-dropdown svg"));
        iconLabel30.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        WebElement registroLink30 = findElementByTextInsensitive("a", "Registro");
        registroLink30.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink30 = findElementByTextInsensitive("a", "Páginas");
        paginasLink30.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink30 = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink30.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink30 = findElementByTextInsensitive("a", "Blog");
        blogLink30.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink30 = findElementByTextInsensitive("a", "Colección");
        coleccionLink30.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon30 = driver.findElement(By.cssSelector(".cart-header svg"));
        cartIcon30.click();
        Thread.sleep(1000);

        WebElement closeCartIcon30 = driver.findElement(By.cssSelector(".closecart > svg"));
        closeCartIcon30.click();
        Thread.sleep(1000);

        WebElement themeLogo30 = driver.findElement(By.cssSelector("h1 img"));
        themeLogo30.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle30 = driver.getWindowHandle();
        assertTrue(currentWindowHandle30.equals(originalWindow), "Failed to switch back to the original window.");
    }

    public void toothfairy()  throws InterruptedException {

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 2200);");
        Thread.sleep(2500);

        // Realizar la acción adicional
        WebElement activarTemaButton31 = driver.findElement(By.cssSelector(".col-xl-3:nth-child(31) .d-inline > .btn"));
        activarTemaButton31.click();

        Thread.sleep(9000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        String secondWindowHandle = driver.getWindowHandle();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(secondWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }


        driver.navigate().refresh();
        Thread.sleep(2000);

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iconLabel31 = driver.findElement(By.cssSelector(".profile-header svg"));
        iconLabel31.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        WebElement registroLink31 = findElementByTextInsensitive("a", "Registro");
        registroLink31.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink31 = findElementByTextInsensitive("a", "Páginas");
        paginasLink31.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink31 = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink31.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink31 = findElementByTextInsensitive("a", "Blog");
        blogLink31.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink31 = findElementByTextInsensitive("a", "Colección");
        coleccionLink31.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon31 = driver.findElement(By.cssSelector(".cart-header svg"));
        cartIcon31.click();
        Thread.sleep(1000);

        WebElement closeCartIcon31 = driver.findElement(By.cssSelector(".closecart > svg"));
        closeCartIcon31.click();
        Thread.sleep(1000);

        WebElement themeLogo31 = driver.findElement(By.cssSelector("h1 img"));
        themeLogo31.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle31 = driver.getWindowHandle();
        assertTrue(currentWindowHandle31.equals(originalWindow), "Failed to switch back to the original window.");
    }

    public void timer()  throws InterruptedException {

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 2200);");
        Thread.sleep(2500);

        // Realizar la acción adicional
        WebElement activarTemaButton32 = driver.findElement(By.cssSelector(".col-xl-3:nth-child(32) .d-inline > .btn"));
        activarTemaButton32.click();

        Thread.sleep(9000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        String secondWindowHandle = driver.getWindowHandle();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(secondWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }


        driver.navigate().refresh();
        Thread.sleep(2000);

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iconLabel32 = driver.findElement(By.cssSelector(".profile-header svg"));
        iconLabel32.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 500);");
        Thread.sleep(2500);

        WebElement registroLink32 = findElementByTextInsensitive("a", "Registro");
        registroLink32.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink32 = findElementByTextInsensitive("a", "Páginas");
        paginasLink32.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink32 = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink32.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink32 = findElementByTextInsensitive("a", "Blog");
        blogLink32.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink32 = findElementByTextInsensitive("a", "Colección");
        coleccionLink32.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon32 = driver.findElement(By.cssSelector(".cart-header svg"));
        cartIcon32.click();
        Thread.sleep(1000);

        WebElement closeCartIcon32 = driver.findElement(By.cssSelector(".closecart > svg"));
        closeCartIcon32.click();
        Thread.sleep(1000);

        WebElement themeLogo32 = driver.findElement(By.cssSelector("h1 img"));
        themeLogo32.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle32 = driver.getWindowHandle();
        assertTrue(currentWindowHandle32.equals(originalWindow), "Failed to switch back to the original window.");
    }

    public void trinkets()  throws InterruptedException {


        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 2400);");
        Thread.sleep(2500);


        // Realizar la acción adicional
        WebElement activarTemaButton34 = driver.findElement(By.cssSelector(".col-xl-3:nth-child(34) .d-inline > .btn"));
        activarTemaButton34.click();

        Thread.sleep(9000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        String secondWindowHandle = driver.getWindowHandle();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(secondWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }


        driver.navigate().refresh();
        Thread.sleep(2000);

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iconLabel34 = driver.findElement(By.cssSelector(".profile-header svg"));
        iconLabel34.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        WebElement registroLink34 = findElementByTextInsensitive("a", "Registro");
        registroLink34.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink34 = findElementByTextInsensitive("a", "Páginas");
        paginasLink34.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink34 = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink34.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink34 = findElementByTextInsensitive("a", "Blog");
        blogLink34.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink34 = findElementByTextInsensitive("a", "Colección");
        coleccionLink34.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon34 = driver.findElement(By.cssSelector(".cart-header svg"));
        cartIcon34.click();
        Thread.sleep(1000);

        WebElement closeCartIcon34 = driver.findElement(By.cssSelector(".closecart > svg"));
        closeCartIcon34.click();
        Thread.sleep(1000);

        WebElement themeLogo34 = driver.findElement(By.cssSelector("h1 img"));
        themeLogo34.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle34 = driver.getWindowHandle();
        assertTrue(currentWindowHandle34.equals(originalWindow), "Failed to switch back to the original window.");
    }

    public void twowheeler()  throws InterruptedException {

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 2400);");
        Thread.sleep(2500);

        // Realizar la acción adicional
        WebElement activarTemaButton35 = driver.findElement(By.cssSelector(".col-xl-3:nth-child(35) .d-inline > .btn"));
        activarTemaButton35.click();

        Thread.sleep(9000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        String secondWindowHandle = driver.getWindowHandle();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(secondWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }


        driver.navigate().refresh();
        Thread.sleep(2000);

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iconLabel35 = driver.findElement(By.cssSelector(".profile-header svg"));
        iconLabel35.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 550);");
        Thread.sleep(2500);

        WebElement registroLink35 = findElementByTextInsensitive("a", "Registro");
        registroLink35.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink35 = findElementByTextInsensitive("a", "Páginas");
        paginasLink35.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink35 = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink35.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink35 = findElementByTextInsensitive("a", "Blog");
        blogLink35.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink35 = findElementByTextInsensitive("a", "Colección");
        coleccionLink35.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon35 = driver.findElement(By.cssSelector(".cart-header svg"));
        cartIcon35.click();
        Thread.sleep(1000);

        WebElement closeCartIcon35 = driver.findElement(By.cssSelector(".closecart > svg"));
        closeCartIcon35.click();
        Thread.sleep(1000);

        WebElement themeLogo35 = driver.findElement(By.cssSelector(".h3 img"));
        themeLogo35.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle35 = driver.getWindowHandle();
        assertTrue(currentWindowHandle35.equals(originalWindow), "Failed to switch back to the original window.");

        System.out.println("Tema twowheeler verficado sin problema");
    }

    public void sheltering()  throws InterruptedException {

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 2200);");
        Thread.sleep(2500);

        // Realizar la acción adicional
        WebElement activarTemaButton36 = driver.findElement(By.cssSelector(".col-xl-3:nth-child(36) .d-inline > .btn"));
        activarTemaButton36.click();

        Thread.sleep(9000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        String secondWindowHandle = driver.getWindowHandle();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(secondWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }


        driver.navigate().refresh();
        Thread.sleep(2000);

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iconLabel36 = driver.findElement(By.cssSelector(".profile-header svg"));
        iconLabel36.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 550);");
        Thread.sleep(2500);

        WebElement registroLink36 = findElementByTextInsensitive("a", "Registro");
        registroLink36.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink36 = findElementByTextInsensitive("a", "Páginas");
        paginasLink36.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink36 = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink36.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink36 = findElementByTextInsensitive("a", "Blog");
        blogLink36.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink36 = findElementByTextInsensitive("a", "Colección");
        coleccionLink36.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon36 = driver.findElement(By.cssSelector(".cart-header svg"));
        cartIcon36.click();
        Thread.sleep(1000);

        WebElement closeCartIcon36 = driver.findElement(By.cssSelector(".closecart path"));
        closeCartIcon36.click();
        Thread.sleep(1000);

        WebElement themeLogo36 = driver.findElement(By.cssSelector(".logo-col:nth-child(3) img"));
        themeLogo36.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle36 = driver.getWindowHandle();
        assertTrue(currentWindowHandle36.equals(originalWindow), "Failed to switch back to the original window.");
    }

    public void shoes()  throws InterruptedException {

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 2800);");
        Thread.sleep(2500);

        // Realizar la acción adicional
        WebElement activarTemaButton37 = driver.findElement(By.cssSelector(".col-xl-3:nth-child(37) .d-inline > .btn"));
        activarTemaButton37.click();

        Thread.sleep(9000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        String secondWindowHandle = driver.getWindowHandle();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(secondWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }


        driver.navigate().refresh();
        Thread.sleep(2000);

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iconLabel37 = driver.findElement(By.cssSelector(".profile-header svg"));
        iconLabel37.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        WebElement registroLink37 = findElementByTextInsensitive("a", "Registro");
        registroLink37.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink37 = findElementByTextInsensitive("a", "Páginas");
        paginasLink37.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink37 = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink37.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink37 = findElementByTextInsensitive("a", "Blog");
        blogLink37.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink37 = findElementByTextInsensitive("a", "Colección");
        coleccionLink37.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon37 = driver.findElement(By.cssSelector(".cart-header svg"));
        cartIcon37.click();
        Thread.sleep(1000);

        WebElement closeCartIcon37 = driver.findElement(By.cssSelector(".closecart > svg"));
        closeCartIcon37.click();
        Thread.sleep(1000);

        WebElement themeLogo37 = driver.findElement(By.cssSelector("h1 img"));
        themeLogo37.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle37 = driver.getWindowHandle();
        assertTrue(currentWindowHandle37.equals(originalWindow), "Failed to switch back to the original window.");
        Thread.sleep(2500);
    }

    public void spects()  throws InterruptedException {

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 2800);");
        Thread.sleep(2500);

        // Realizar la acción adicional
        WebElement activarTemaButton38 = driver.findElement(By.cssSelector(".col-xl-3:nth-child(38) .d-inline > .btn"));
        activarTemaButton38.click();

        Thread.sleep(9000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        String secondWindowHandle = driver.getWindowHandle();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(secondWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }


        driver.navigate().refresh();
        Thread.sleep(2000);

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iconLabel38 = driver.findElement(By.cssSelector(".profile-header svg"));
        iconLabel38.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        WebElement registroLink38 = findElementByTextInsensitive("a", "Registro");
        registroLink38.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink38 = findElementByTextInsensitive("a", "Páginas");
        paginasLink38.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink38 = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink38.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink38 = findElementByTextInsensitive("a", "Blog");
        blogLink38.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink38 = findElementByTextInsensitive("a", "Colección");
        coleccionLink38.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon38 = driver.findElement(By.cssSelector(".cart-header svg"));
        cartIcon38.click();
        Thread.sleep(1000);

        WebElement closeCartIcon38 = driver.findElement(By.cssSelector(".closecart > svg"));
        closeCartIcon38.click();
        Thread.sleep(1000);

        WebElement themeLogo38 = driver.findElement(By.cssSelector("h1 img"));
        themeLogo38.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle38 = driver.getWindowHandle();
        assertTrue(currentWindowHandle38.equals(originalWindow), "Failed to switch back to the original window.");
        Thread.sleep(2500);
    }

    public void style()  throws InterruptedException {

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 2800);");
        Thread.sleep(2500);

        // Realizar la acción adicional
        WebElement activarTemaButton39 = driver.findElement(By.cssSelector(".col-xl-3:nth-child(39) .d-inline > .btn"));
        activarTemaButton39.click();

        Thread.sleep(9000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        String secondWindowHandle = driver.getWindowHandle();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(secondWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }


        driver.navigate().refresh();
        Thread.sleep(2000);

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iconLabel39 = driver.findElement(By.cssSelector(".profile-header svg"));
        iconLabel39.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 500);");
        Thread.sleep(2500);

        WebElement registroLink39 = findElementByTextInsensitive("a", "Registro");
        registroLink39.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink39 = findElementByTextInsensitive("a", "Páginas");
        paginasLink39.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink39 = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink39.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink39 = findElementByTextInsensitive("a", "Blog");
        blogLink39.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink39 = findElementByTextInsensitive("a", "Colección");
        coleccionLink39.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon39 = driver.findElement(By.cssSelector(".cart-header svg"));
        cartIcon39.click();
        Thread.sleep(1000);

        WebElement closeCartIcon39 = driver.findElement(By.cssSelector(".closecart > svg"));
        closeCartIcon39.click();
        Thread.sleep(1000);

        WebElement themeLogo39 = driver.findElement(By.cssSelector("h1 img"));
        themeLogo39.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle39 = driver.getWindowHandle();
        assertTrue(currentWindowHandle39.equals(originalWindow), "Failed to switch back to the original window.");
    }

    public void supermarket()  throws InterruptedException {

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 2800);");
        Thread.sleep(2500);

        // Realizar la acción adicional
        WebElement activarTemaButton40 = driver.findElement(By.cssSelector(".col-xl-3:nth-child(40) .d-inline > .btn"));
        activarTemaButton40.click();

        Thread.sleep(9000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        String secondWindowHandle = driver.getWindowHandle();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(secondWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }


        driver.navigate().refresh();
        Thread.sleep(2000);

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iconLabel40 = driver.findElement(By.cssSelector(".profile-header svg"));
        iconLabel40.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 500);");
        Thread.sleep(2500);

        WebElement registroLink40 = findElementByTextInsensitive("a", "Registro");
        registroLink40.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink40 = findElementByTextInsensitive("a", "Páginas");
        paginasLink40.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink40 = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink40.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink40 = findElementByTextInsensitive("a", "Blog");
        blogLink40.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink40 = findElementByTextInsensitive("a", "Colección");
        coleccionLink40.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon40 = driver.findElement(By.cssSelector(".cart-header svg"));
        cartIcon40.click();
        Thread.sleep(1000);

        WebElement closeCartIcon40 = driver.findElement(By.cssSelector(".closecart > svg"));
        closeCartIcon40.click();
        Thread.sleep(1000);

        WebElement themeLogo40 = driver.findElement(By.cssSelector("h1 img"));
        themeLogo40.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle40 = driver.getWindowHandle();
        assertTrue(currentWindowHandle40.equals(originalWindow), "Failed to switch back to the original window.");
        Thread.sleep(2500);
    }

    public void motorcycle()  throws InterruptedException {

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 2800);");
        Thread.sleep(2500);

        // Realizar la acción adicional
        WebElement activarTemaButton41 = driver.findElement(By.cssSelector(".col-xl-3:nth-child(41) .d-inline > .btn"));
        activarTemaButton41.click();

        Thread.sleep(9000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        String secondWindowHandle = driver.getWindowHandle();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(secondWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }


        driver.navigate().refresh();
        Thread.sleep(2000);

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iconLabel41 = driver.findElement(By.cssSelector(".profile-header svg"));
        iconLabel41.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        WebElement registroLink41 = findElementByTextInsensitive("a", "Registro");
        registroLink41.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink41 = findElementByTextInsensitive("a", "Páginas");
        paginasLink41.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink41 = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink41.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink41 = findElementByTextInsensitive("a", "Blog");
        blogLink41.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink41 = findElementByTextInsensitive("a", "Colección");
        coleccionLink41.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon41 = driver.findElement(By.cssSelector(".cart-header svg"));
        cartIcon41.click();
        Thread.sleep(1000);

        WebElement closeCartIcon41 = driver.findElement(By.cssSelector(".closecart > svg"));
        closeCartIcon41.click();
        Thread.sleep(1000);

        WebElement themeLogo41 = driver.findElement(By.cssSelector(".h3 img"));
        themeLogo41.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle41 = driver.getWindowHandle();
        assertTrue(currentWindowHandle41.equals(originalWindow), "Failed to switch back to the original window.");
        Thread.sleep(2500);
    }

    public void mountains()  throws InterruptedException {

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 2800);");
        Thread.sleep(2500);

        // Realizar la acción adicional
        WebElement activarTemaButton42 = driver.findElement(By.cssSelector(".col-xl-3:nth-child(42) .d-inline > .btn"));
        activarTemaButton42.click();

        Thread.sleep(9000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        String secondWindowHandle = driver.getWindowHandle();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(secondWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }


        driver.navigate().refresh();
        Thread.sleep(2000);

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iconLabel42 = driver.findElement(By.cssSelector(".header-user svg"));
        iconLabel42.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        WebElement registroLink42 = findElementByTextInsensitive("a", "Registro");
        registroLink42.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink42 = findElementByTextInsensitive("a", "Páginas");
        paginasLink42.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink42 = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink42.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink42 = findElementByTextInsensitive("a", "Blog");
        blogLink42.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink42 = findElementByTextInsensitive("a", "Colección");
        coleccionLink42.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon42 = driver.findElement(By.cssSelector(".cart-header svg"));
        cartIcon42.click();
        Thread.sleep(1000);

        WebElement closeCartIcon42 = driver.findElement(By.cssSelector(".closecart > svg"));
        closeCartIcon42.click();
        Thread.sleep(1000);

        WebElement themeLogo42 = driver.findElement(By.cssSelector(".logo-col img"));
        themeLogo42.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle42 = driver.getWindowHandle();
        assertTrue(currentWindowHandle42.equals(originalWindow), "Failed to switch back to the original window.");
        Thread.sleep(2500);
    }

    public void perfumes()  throws InterruptedException {

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 2800);");
        Thread.sleep(2500);

        // Realizar la acción adicional
        WebElement activarTemaButton43 = driver.findElement(By.cssSelector(".col-xl-3:nth-child(43) .d-inline > .btn"));
        activarTemaButton43.click();

        Thread.sleep(9000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        String secondWindowHandle = driver.getWindowHandle();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(secondWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }


        driver.navigate().refresh();
        Thread.sleep(2000);

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iconLabel43 = driver.findElement(By.cssSelector(".profile-header svg"));
        iconLabel43.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 500);");
        Thread.sleep(2500);

        WebElement registroLink43 = findElementByTextInsensitive("a", "Registro");
        registroLink43.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink43 = findElementByTextInsensitive("a", "Páginas");
        paginasLink43.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink43 = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink43.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink43 = findElementByTextInsensitive("a", "Blog");
        blogLink43.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink43 = findElementByTextInsensitive("a", "Colección");
        coleccionLink43.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon43 = driver.findElement(By.cssSelector(".cart-header svg"));
        cartIcon43.click();
        Thread.sleep(1000);

        WebElement closeCartIcon43 = driver.findElement(By.cssSelector(".closecart > svg"));
        closeCartIcon43.click();
        Thread.sleep(1000);

        WebElement themeLogo43 = driver.findElement(By.cssSelector("h1 img"));
        themeLogo43.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle43 = driver.getWindowHandle();
        assertTrue(currentWindowHandle43.equals(originalWindow), "Failed to switch back to the original window.");
        Thread.sleep(2500);
    }

    public void property()  throws InterruptedException {

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 2800);");
        Thread.sleep(2500);

        // Realizar la acción adicional
        WebElement activarTemaButton44 = driver.findElement(By.cssSelector(".col-xl-3:nth-child(44) .d-inline > .btn"));
        activarTemaButton44.click();

        Thread.sleep(9000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        String secondWindowHandle = driver.getWindowHandle();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(secondWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }


        driver.navigate().refresh();
        Thread.sleep(2000);

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iconLabel44 = driver.findElement(By.cssSelector(".profile-header svg"));
        iconLabel44.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 500);");
        Thread.sleep(2500);

        WebElement registroLink44 = findElementByTextInsensitive("a", "Registro");
        registroLink44.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink44 = findElementByTextInsensitive("a", "Páginas");
        paginasLink44.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink44 = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink44.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink44 = findElementByTextInsensitive("a", "Blog");
        blogLink44.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink44 = findElementByTextInsensitive("a", "Colección");
        coleccionLink44.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon44 = driver.findElement(By.cssSelector(".cart-header svg"));
        cartIcon44.click();
        Thread.sleep(1000);

        WebElement closeCartIcon44 = driver.findElement(By.cssSelector(".closecart > svg"));
        closeCartIcon44.click();
        Thread.sleep(1000);

        WebElement themeLogo44 = driver.findElement(By.cssSelector("h1 img"));
        themeLogo44.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle44 = driver.getWindowHandle();
        assertTrue(currentWindowHandle44.equals(originalWindow), "Failed to switch back to the original window.");
        Thread.sleep(2500);
    }

    public void sensation()  throws InterruptedException {

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 3370);");
        Thread.sleep(2500);

        // Realizar la acción adicional
        WebElement activarTemaButton45 = driver.findElement(By.cssSelector(".col-xl-3:nth-child(45) .d-inline > .btn"));
        activarTemaButton45.click();

        Thread.sleep(9000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        String secondWindowHandle = driver.getWindowHandle();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(secondWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }


        driver.navigate().refresh();
        Thread.sleep(2000);

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iconLabel45 = driver.findElement(By.cssSelector(".profile-header svg"));
        iconLabel45.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        WebElement registroLink45 = findElementByTextInsensitive("a", "Registro");
        registroLink45.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink45 = findElementByTextInsensitive("a", "Páginas");
        paginasLink45.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink45 = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink45.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink45 = findElementByTextInsensitive("a", "Blog");
        blogLink45.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink45 = findElementByTextInsensitive("a", "Colección");
        coleccionLink45.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon45 = driver.findElement(By.cssSelector(".cart-header svg"));
        cartIcon45.click();
        Thread.sleep(1000);

        WebElement closeCartIcon45 = driver.findElement(By.cssSelector(".closecart > svg"));
        closeCartIcon45.click();
        Thread.sleep(1000);

        WebElement themeLogo45 = driver.findElement(By.cssSelector("h1 img"));
        themeLogo45.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle45 = driver.getWindowHandle();
        assertTrue(currentWindowHandle45.equals(originalWindow), "Failed to switch back to the original window.");
        Thread.sleep(2500);
    }

    public void logisticsboxes()  throws InterruptedException {

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 3370);");
        Thread.sleep(2500);

        // Realizar la acción adicional
        WebElement activarTemaButton46 = driver.findElement(By.cssSelector(".col-xl-3:nth-child(46) .d-inline > .btn"));
        activarTemaButton46.click();

        Thread.sleep(9000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        String secondWindowHandle = driver.getWindowHandle();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(secondWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }


        driver.navigate().refresh();
        Thread.sleep(2000);

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iconLabel46 = driver.findElement(By.cssSelector(".profile-header svg"));
        iconLabel46.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        WebElement registroLink46 = findElementByTextInsensitive("a", "Registro");
        registroLink46.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink46 = findElementByTextInsensitive("a", "Páginas");
        paginasLink46.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink46 = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink46.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink46 = findElementByTextInsensitive("a", "Blog");
        blogLink46.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink46 = findElementByTextInsensitive("a", "Colección");
        coleccionLink46.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon46 = driver.findElement(By.cssSelector(".cart-header svg"));
        cartIcon46.click();
        Thread.sleep(1000);

        WebElement closeCartIcon46 = driver.findElement(By.cssSelector(".closecart > svg"));
        closeCartIcon46.click();
        Thread.sleep(1000);

        WebElement themeLogo46 = driver.findElement(By.cssSelector("h1 img"));
        themeLogo46.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle46 = driver.getWindowHandle();
        assertTrue(currentWindowHandle46.equals(originalWindow), "Failed to switch back to the original window.");
        Thread.sleep(2500);
    }

    public void landholdings()  throws InterruptedException {

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 3370);");
        Thread.sleep(2500);

        // Realizar la acción adicional
        WebElement activarTemaButton47 = driver.findElement(By.cssSelector(".col-xl-3:nth-child(47) .d-inline > .btn"));
        activarTemaButton47.click();

        Thread.sleep(9000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        String secondWindowHandle = driver.getWindowHandle();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(secondWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }


        driver.navigate().refresh();
        Thread.sleep(2000);

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iconLabel47 = driver.findElement(By.cssSelector(".profile-header svg"));
        iconLabel47.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        WebElement registroLink47 = findElementByTextInsensitive("a", "Registro");
        registroLink47.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink47 = findElementByTextInsensitive("a", "Páginas");
        paginasLink47.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink47 = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink47.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink47 = findElementByTextInsensitive("a", "Blog");
        blogLink47.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink47 = findElementByTextInsensitive("a", "Colección");
        coleccionLink47.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon47 = driver.findElement(By.cssSelector(".cart-header svg"));
        cartIcon47.click();
        Thread.sleep(1000);

        WebElement closeCartIcon47 = driver.findElement(By.cssSelector(".closecart > svg"));
        closeCartIcon47.click();
        Thread.sleep(1000);

        WebElement themeLogo47 = driver.findElement(By.cssSelector("h1 img"));
        themeLogo47.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle47 = driver.getWindowHandle();
        assertTrue(currentWindowHandle47.equals(originalWindow), "Failed to switch back to the original window.");
        Thread.sleep(2500);
    }

    public void mensonly()  throws InterruptedException {

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 3370);");
        Thread.sleep(2500);

        // Realizar la acción adicional
        WebElement activarTemaButton48 = driver.findElement(By.cssSelector(".col-xl-3:nth-child(48) .d-inline > .btn"));
        activarTemaButton48.click();

        Thread.sleep(9000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        String secondWindowHandle = driver.getWindowHandle();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(secondWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }


        driver.navigate().refresh();
        Thread.sleep(2000);

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iconLabel48 = driver.findElement(By.cssSelector(".profile-header svg"));
        iconLabel48.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 5" +
                "" +
                "" +
                "50);");
        Thread.sleep(2500);

        WebElement registroLink48 = findElementByTextInsensitive("a", "Registro");
        registroLink48.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink48 = findElementByTextInsensitive("a", "Páginas");
        paginasLink48.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink48 = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink48.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink48 = findElementByTextInsensitive("a", "Blog");
        blogLink48.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink48 = findElementByTextInsensitive("a", "Colección");
        coleccionLink48.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon48 = driver.findElement(By.cssSelector(".cart-header svg"));
        cartIcon48.click();
        Thread.sleep(1000);

        WebElement closeCartIcon48 = driver.findElement(By.cssSelector(".closecart > svg"));
        closeCartIcon48.click();
        Thread.sleep(1000);

        WebElement themeLogo48 = driver.findElement(By.cssSelector("h1 img"));
        themeLogo48.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle48 = driver.getWindowHandle();
        assertTrue(currentWindowHandle48.equals(originalWindow), "Failed to switch back to the original window.");
        Thread.sleep(2500);
    }

    public void minimarket()  throws InterruptedException {

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 3370);");
        Thread.sleep(2500);

        // Realizar la acción adicional
        WebElement activarTemaButton50 = driver.findElement(By.cssSelector(".col-xl-3:nth-child(50) .d-inline > .btn"));
        activarTemaButton50.click();

        Thread.sleep(9000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        String secondWindowHandle = driver.getWindowHandle();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(secondWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }


        driver.navigate().refresh();
        Thread.sleep(2000);

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iconLabel50 = driver.findElement(By.cssSelector(".profile-header svg"));
        iconLabel50.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        WebElement registroLink50 = findElementByTextInsensitive("a", "Registro");
        registroLink50.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink50 = findElementByTextInsensitive("a", "Páginas");
        paginasLink50.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink50 = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink50.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink50 = findElementByTextInsensitive("a", "Blog");
        blogLink50.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink50 = findElementByTextInsensitive("a", "Colección");
        coleccionLink50.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon50 = driver.findElement(By.cssSelector(".cart-count > .count"));
        cartIcon50.click();
        Thread.sleep(1000);

        WebElement closeCartIcon50 = driver.findElement(By.cssSelector(".closecart > svg"));
        closeCartIcon50.click();
        Thread.sleep(1000);

        WebElement themeLogo50 = driver.findElement(By.cssSelector("h1 img"));
        themeLogo50.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle50 = driver.getWindowHandle();
        assertTrue(currentWindowHandle50.equals(originalWindow), "Failed to switch back to the original window.");
        Thread.sleep(2500);
    }

    public void homedecor()  throws InterruptedException {

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 3370);");
        Thread.sleep(2500);

        // Realizar la acción adicional
        WebElement activarTemaButton51 = driver.findElement(By.cssSelector(".col-xl-3:nth-child(51) .d-inline > .btn"));
        activarTemaButton51.click();

        Thread.sleep(9000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        String secondWindowHandle = driver.getWindowHandle();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(secondWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }


        driver.navigate().refresh();
        Thread.sleep(2000);

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iconLabel51 = driver.findElement(By.cssSelector(".profile-header svg"));
        iconLabel51.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        WebElement registroLink51 = findElementByTextInsensitive("a", "Registro");
        registroLink51.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink51 = findElementByTextInsensitive("a", "Páginas");
        paginasLink51.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink51 = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink51.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink51 = findElementByTextInsensitive("a", "Blog");
        blogLink51.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink51 = findElementByTextInsensitive("a", "Colección");
        coleccionLink51.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon51 = driver.findElement(By.cssSelector(".cart-header svg"));
        cartIcon51.click();
        Thread.sleep(1000);

        WebElement closeCartIcon51 = driver.findElement(By.cssSelector(".closecart > svg"));
        closeCartIcon51.click();
        Thread.sleep(1000);

        WebElement themeLogo51 = driver.findElement(By.cssSelector("h1 img"));
        themeLogo51.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle51 = driver.getWindowHandle();
        assertTrue(currentWindowHandle51.equals(originalWindow), "Failed to switch back to the original window.");
        Thread.sleep(2500);
    }

    public void housewares()  throws InterruptedException {

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 3370);");
        Thread.sleep(2500);

        // Realizar la acción adicional
        WebElement activarTemaButton52 = driver.findElement(By.cssSelector(".col-xl-3:nth-child(52) .d-inline > .btn"));
        activarTemaButton52.click();

        Thread.sleep(9000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        String secondWindowHandle = driver.getWindowHandle();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(secondWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }


        driver.navigate().refresh();
        Thread.sleep(2000);

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iconLabel52 = driver.findElement(By.cssSelector(".profile-header svg"));
        iconLabel52.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 550);");
        Thread.sleep(2500);

        WebElement registroLink52 = findElementByTextInsensitive("a", "Registro");
        registroLink52.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink52 = findElementByTextInsensitive("a", "Páginas");
        paginasLink52.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink52 = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink52.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink52 = findElementByTextInsensitive("a", "Blog");
        blogLink52.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink52 = findElementByTextInsensitive("a", "Colección");
        coleccionLink52.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon52 = driver.findElement(By.cssSelector(".cart-header svg"));
        cartIcon52.click();
        Thread.sleep(1000);

        WebElement closeCartIcon52 = driver.findElement(By.cssSelector(".closecart > svg"));
        closeCartIcon52.click();
        Thread.sleep(1000);

        WebElement themeLogo52 = driver.findElement(By.cssSelector("h1 img"));
        themeLogo52.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle52 = driver.getWindowHandle();
        assertTrue(currentWindowHandle52.equals(originalWindow), "Failed to switch back to the original window.");
        Thread.sleep(2500);
    }

    public void kidscare()  throws InterruptedException {

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 3700);");
        Thread.sleep(2500);

        // Realizar la acción adicional
        WebElement activarTemaButton53 = driver.findElement(By.cssSelector(".col-xl-3:nth-child(53) .d-inline > .btn"));
        activarTemaButton53.click();

        Thread.sleep(9000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        String secondWindowHandle = driver.getWindowHandle();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(secondWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }


        driver.navigate().refresh();
        Thread.sleep(2000);

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iconLabel53 = driver.findElement(By.cssSelector(".menu-dropdown > a"));
        iconLabel53.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        WebElement registroLink53 = findElementByTextInsensitive("a", "Registro");
        registroLink53.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink53 = findElementByTextInsensitive("a", "Páginas");
        paginasLink53.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink53 = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink53.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink53 = findElementByTextInsensitive("a", "Blog");
        blogLink53.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink53 = findElementByTextInsensitive("a", "Colección");
        coleccionLink53.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon53 = driver.findElement(By.cssSelector(".cart-header svg"));
        cartIcon53.click();
        Thread.sleep(1000);

        WebElement closeCartIcon53 = driver.findElement(By.cssSelector(".closecart > svg"));
        closeCartIcon53.click();
        Thread.sleep(1000);

        WebElement themeLogo53 = driver.findElement(By.cssSelector("h1 img"));
        themeLogo53.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle53 = driver.getWindowHandle();
        assertTrue(currentWindowHandle53.equals(originalWindow), "Failed to switch back to the original window.");
        Thread.sleep(2500);
    }

    public void kitchen()  throws InterruptedException {

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 3700);");
        Thread.sleep(2500);

        // Realizar la acción adicional
        WebElement activarTemaButton54 = driver.findElement(By.cssSelector(".col-xl-3:nth-child(54) .d-inline > .btn"));
        activarTemaButton54.click();

        Thread.sleep(9000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        String secondWindowHandle = driver.getWindowHandle();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(secondWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }


        driver.navigate().refresh();
        Thread.sleep(2000);

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iconLabel54 = driver.findElement(By.cssSelector(".profile-header svg"));
        iconLabel54.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 500);");
        Thread.sleep(2500);

        WebElement registroLink54 = findElementByTextInsensitive("a", "Registro");
        registroLink54.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink54 = findElementByTextInsensitive("a", "Páginas");
        paginasLink54.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink54 = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink54.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink54 = findElementByTextInsensitive("a", "Blog");
        blogLink54.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink54 = findElementByTextInsensitive("a", "Colección");
        coleccionLink54.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon54 = driver.findElement(By.cssSelector(".cart-header svg"));
        cartIcon54.click();
        Thread.sleep(1000);

        WebElement closeCartIcon54 = driver.findElement(By.cssSelector(".closecart > svg"));
        closeCartIcon54.click();
        Thread.sleep(1000);

        WebElement themeLogo54 = driver.findElement(By.cssSelector("h1 img"));
        themeLogo54.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle54 = driver.getWindowHandle();
        assertTrue(currentWindowHandle54.equals(originalWindow), "Failed to switch back to the original window.");
        Thread.sleep(2500);
    }

    public void wristwatch()  throws InterruptedException {

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 3700);");
        Thread.sleep(2500);

        // Realizar la acción adicional
        WebElement activarTemaButton55 = driver.findElement(By.cssSelector(".col-xl-3:nth-child(55) .d-inline > .btn"));
        activarTemaButton55.click();

        Thread.sleep(9000);

        String originalWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        String secondWindowHandle = driver.getWindowHandle();
        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(secondWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }


        driver.navigate().refresh();
        Thread.sleep(2000);

        // Scroll
        for (int i = 0; i < 6; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
            Thread.sleep(2500);
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement iconLabel55 = driver.findElement(By.cssSelector(".profile-header svg"));
        iconLabel55.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        WebElement registroLink55 = findElementByTextInsensitive("a", "Registro");
        registroLink55.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink55 = findElementByTextInsensitive("a", "Páginas");
        paginasLink55.click();
        Thread.sleep(1000);

        WebElement preguntasFrecuentesLink55 = findElementByTextInsensitive("a", "Preguntas frecuentes");
        preguntasFrecuentesLink55.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement blogLink55 = findElementByTextInsensitive("a", "Blog");
        blogLink55.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        paginasLink = findElementByTextInsensitive("a", "Páginas");
        paginasLink.click();
        Thread.sleep(1000);

        WebElement coleccionLink55 = findElementByTextInsensitive("a", "Colección");
        coleccionLink55.click();
        Thread.sleep(1000);

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 650);");
        Thread.sleep(2500);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
        Thread.sleep(2500);

        WebElement cartIcon55 = driver.findElement(By.cssSelector(".cart-header svg"));
        cartIcon55.click();
        Thread.sleep(1000);

        WebElement closeCartIcon55 = driver.findElement(By.cssSelector(".closecart > svg"));
        closeCartIcon55.click();
        Thread.sleep(1000);

        WebElement themeLogo55 = driver.findElement(By.cssSelector(".logo-col:nth-child(3) img"));
        themeLogo55.click();
        Thread.sleep(1000);

        driver.switchTo().window(originalWindow);

        // Verificar que estamos de vuelta en la ventana original
        String currentWindowHandle55 = driver.getWindowHandle();
        assertTrue(currentWindowHandle55.equals(originalWindow), "Failed to switch back to the original window.");
        Thread.sleep(2500);
    }


    public String normalizeText(String text) {
        String normalized = Normalizer.normalize(text, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(normalized).replaceAll("").toLowerCase();
    }

    public WebElement findElementByTextInsensitive(String tagName, String text) {
        String normalizedText = normalizeText(text);
        List<WebElement> elements = driver.findElements(By.tagName(tagName));
        for (WebElement element : elements) {
            if (normalizeText(element.getText()).equals(normalizedText)) {
                return element;
            }
        }
        throw new NoSuchElementException("No element with text (case insensitive and accents insensitive): " + text);
    }

}
