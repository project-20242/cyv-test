package net.tecgurus.clickyvende.home;

import net.tecgurus.clickyvende.BaseTest;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ReportePago extends BaseTest {

    @Test
    public void realizarReporteDePagos() throws InterruptedException {
        driver.get(baseUrl);
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20));

        aceptarCookies();

        verificarBotonInicioSesion();

        iniciarSesion("alejandroresendiz04aguilar@gmail.com", "12345678");

        navegarARapportedePagos(wait);

        exportarDatosPorAno(wait);
        exportarDatosPorMesPasado(wait);
        exportarDatosPorEsteMes(wait);
        exportarDatosPorUltimosSieteDias(wait);
        exportarDatosPorFechasEspecificas(wait, "09052024", "10052024");
    }

    private void aceptarCookies() throws InterruptedException {
        try {
            WebElement botonAceptarCookies = driver.findElement(By.xpath("//button[contains(text(), 'Aceptar')]"));
            botonAceptarCookies.click();
            Thread.sleep(1000);
        } catch (Exception e) {
            System.out.println("El botón de aceptar cookies no se encontró o ya se aceptaron.");
        }
    }

    private void verificarBotonInicioSesion() {
        WebElement botonInicioSesion = new WebDriverWait(driver, Duration.ofSeconds(30))
                .until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(), 'Inicio Sesión')]")));

        assertTrue(botonInicioSesion.isDisplayed(), "El botón de inicio de sesión no está visible.");
        assertEquals("Inicio Sesión", botonInicioSesion.getText(), "El texto del botón de inicio sesión no es el esperado.");
        botonInicioSesion.click();
    }

    private void iniciarSesion(String email, String contrasena) {
        WebElement campoEmail = driver.findElement(By.id("email"));
        WebElement campoContrasena = driver.findElement(By.id("password"));
        WebElement botonIniciarSesion = driver.findElement(By.className("login_button"));

        campoEmail.sendKeys(email);
        campoContrasena.sendKeys(contrasena);
        botonIniciarSesion.click();
    }

    private void navegarARapportedePagos(WebDriverWait wait) throws InterruptedException {
        WebElement reportes = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"html-dir-tag\"]/body/nav/div[1]/div[2]/div[1]/div[2]/div/div/div/ul/li[10]")));
        reportes.click();
        Thread.sleep(2000);

        WebElement pagos = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"html-dir-tag\"]/body/nav/div[1]/div[2]/div[1]/div[2]/div/div/div/ul/li[10]/ul/li[4]/a")));
        pagos.click();
        Thread.sleep(2000);
    }

    private void exportarDatosPorAno(WebDriverWait wait) throws InterruptedException {
        WebElement botonPorAno = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"pills-tab\"]/li[1]")));
        botonPorAno.click();
        Thread.sleep(2000);

        WebElement botonExportacion = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"html-dir-tag\"]/body/div[1]/div/div[1]/div/div/div[2]/div/a")));
        botonExportacion.click();
        Thread.sleep(2000);
    }

    private void exportarDatosPorMesPasado(WebDriverWait wait) throws InterruptedException {
        WebElement botonPorMesPasado = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"pills-tab\"]/li[2]")));
        botonPorMesPasado.click();
        Thread.sleep(2000);

        WebElement botonExportacion = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"html-dir-tag\"]/body/div[1]/div/div[1]/div/div/div[2]/div/a")));
        botonExportacion.click();
        Thread.sleep(2000);
    }

    private void exportarDatosPorEsteMes(WebDriverWait wait) throws InterruptedException {
        WebElement botonPorEsteMes = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"pills-tab\"]/li[3]")));
        botonPorEsteMes.click();
        Thread.sleep(2000);

        WebElement botonExportacion = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"html-dir-tag\"]/body/div[1]/div/div[1]/div/div/div[2]/div/a")));
        botonExportacion.click();
        Thread.sleep(2000);
    }

    private void exportarDatosPorUltimosSieteDias(WebDriverWait wait) throws InterruptedException {
        WebElement botonPorUltimosSieteDias = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"pills-tab\"]/li[4]")));
        botonPorUltimosSieteDias.click();
        Thread.sleep(2000);

        WebElement botonExportacion = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"html-dir-tag\"]/body/div[1]/div/div[1]/div/div/div[2]/div/a")));
        botonExportacion.click();
        Thread.sleep(2000);
    }

    private void exportarDatosPorFechasEspecificas(WebDriverWait wait, String fechaInicio, String fechaFin) throws InterruptedException {
        WebElement campoFechaInicio = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("start_date")));
        campoFechaInicio.sendKeys(fechaInicio);
        Thread.sleep(2000);

        WebElement campoFechaFin = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("end_date")));
        campoFechaFin.sendKeys(fechaFin);
        Thread.sleep(2000);

        WebElement botonGenerar = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"filter_type\"]")));
        botonGenerar.click();
        Thread.sleep(2000);

        WebElement botonExportacion = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"html-dir-tag\"]/body/div[1]/div/div[1]/div/div/div[2]/div/a")));
        botonExportacion.click();
        Thread.sleep(2000);
       }
    }