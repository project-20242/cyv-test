package net.tecgurus.clickyvende.home;

import net.tecgurus.clickyvende.BaseTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.Random;

public class RegistroUsuario extends BaseTest {

    @Test
    public void RegistrodeUsuario() {
        String randomUsername = "Usuario" + new Random().nextInt(1000);
        String randomPassword = "Pass" + (new Random().nextInt(9000) + 1000);
        String randomEmail = "usuario" + new Random().nextInt(1000) + "@example.com";
        String randomStoreName = "Tienda" + new Random().nextInt(1000);

        driver.get(baseUrl);
        WebDriverWait espera = new WebDriverWait(driver, Duration.ofSeconds(20));

        try {
            // Aceptar cookies si aparece el botón
            try {
                WebElement acceptCookiesButton = driver.findElement(By.xpath("//button[contains(text(), 'Aceptar')]"));
                acceptCookiesButton.click();
                Thread.sleep(1000); // Espera para asegurarse de que la acción se complete
            } catch (Exception e) {
                System.out.println("El botón de aceptar cookies no se encontró o ya se aceptaron.");
            }

            // Navegar a la página de registro
            WebElement botonRegistro = espera.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@href='https://test.clickyvende.net/register']")));
            botonRegistro.click();

            // Llenar el formulario de registro
            WebElement inputName = espera.until(ExpectedConditions.visibilityOfElementLocated(By.id("name")));
            inputName.sendKeys(randomUsername);

            WebElement inputStoreName = espera.until(ExpectedConditions.visibilityOfElementLocated(By.name("store_name")));
            inputStoreName.sendKeys(randomStoreName);

            WebElement inputEmail = espera.until(ExpectedConditions.visibilityOfElementLocated(By.id("email")));
            inputEmail.sendKeys(randomEmail);

            WebElement inputPassword = espera.until(ExpectedConditions.visibilityOfElementLocated(By.id("password")));
            inputPassword.sendKeys(randomPassword);

            WebElement inputConfirmPassword = espera.until(ExpectedConditions.visibilityOfElementLocated(By.id("password_confirmation")));
            inputConfirmPassword.sendKeys(randomPassword);

            // Esperar a que el botón de registro esté visible
            WebElement botonRegistrar = espera.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@type='submit' and contains(text(), ' Register ')]")));
            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", botonRegistrar);
            Thread.sleep(500); // Espera para asegurarse de que el scroll se complete
            ((JavascriptExecutor) driver).executeScript("arguments[0].click();", botonRegistrar);

            // Verificar que el registro fue exitoso
            WebElement successMessage = espera.until(ExpectedConditions.visibilityOfElementLocated(By.id("success_message")));
            Assertions.assertTrue(successMessage.isDisplayed(), "¡El registro no fue exitoso!");
            Assertions.assertEquals(successMessage.getText(), "Registro exitoso", "¡El mensaje de éxito no es correcto!");

        } catch (Exception e) {
            System.out.println("Error durante la ejecución del test: " + e.getMessage());
            e.printStackTrace();
        } finally {
            tearDown(); // Asegúrate de cerrar el WebDriver en caso de errores
        }
    }

    @AfterEach
    public void tearDown() {
        try {
            System.out.println("Iniciando el cierre del WebDriver...");
            if (driver != null) {
                driver.quit();
                Thread.sleep(1000); // Ajusta el tiempo si es necesario
            }
            System.out.println("WebDriver cerrado correctamente.");
        } catch (Exception e) {
            System.out.println("Error al cerrar el WebDriver: " + e.getMessage());
        }
    }
}
