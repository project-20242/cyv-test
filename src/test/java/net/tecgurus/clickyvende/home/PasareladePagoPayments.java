package net.tecgurus.clickyvende.home;

import net.tecgurus.clickyvende.BaseTest;
import org.asynchttpclient.Dsl;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class PasareladePagoPayments extends BaseTest {

    @Test
    public void testProbarMetodos() throws InterruptedException {
        // Ingresar a la página
        LoginYVerTienda();


        comprarbanktransfer();
        // Comprar productos en bank transfer


        comprarstripe();
        //Comprar en stripe

        comprarpaypal();
        //Comprar en Paypal

        comprarauthorize();
        //Comprar con Authorize





    }
    //SE DEBE DE TENER UNA CUENTA YA CREADA QUE MODIFICARAS EN LOS CAMPOS DE ABAJO

    //SE DEBE TENER ACTIVADO STRIPE Y SI NO LO TIENES, ESTOS SON LOS CODIGOS

    //Clave publicable
    //pk_test_51I8wDiH92DQUN2aQR83DqlfWngUGe7tbW0lJfNcBStXBNul0s1EiLjLsCV1WK4ROWuQKdjq0hEJLaLeSubJSy9zR00SCwtNhoo

    //Clave secreta
    //sk_test_51I8wDiH92DQUN2aQu3Bbu7INic24tOYutZJ5pXZdCyv12iDILr2agncoMacK3Og3GiMMaebNMBo6dnPJsnfhlYtW00tMNOGI6T


    //PARA METODO DE ENVIO CREAR UN METODO DE ENVIO
    //COUNTRY DEBE DE SER ARGENTINA
    //STATE DEBE DE SER BUENOS AIRES


    private void LoginYVerTienda() throws InterruptedException {
        driver.manage().window().maximize();
        driver.get(baseUrl + "login");

        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20)); // Aumentamos el tiempo de espera

        // Definimos variables
        WebElement usernameInput = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("email")));
        WebElement passwordInput = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("password")));
        WebElement loginBtn = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".btn.btn-primary.btn-block.mt-2.login_button")));
        WebElement accept = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("c-p-bn")));

        Thread.sleep(2000);

        // Ingresamos las credenciales y hacemos clic en el botón de login
        usernameInput.sendKeys("phone@gmail.com");
        passwordInput.sendKeys("12345678");
        accept.click();
        loginBtn.click();

        Thread.sleep(2000);



        WebElement config = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"html-dir-tag\"]/body/nav/div[1]/div[2]/div[1]/div[2]/div/div/div/ul/li[16]/a")));
        config.click();
        Thread.sleep(2000);

        WebElement configpay = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"useradd-sidenav\"]/a[5]")));
        configpay.click();
        Thread.sleep(2000);

        WebElement banktranfer = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"Bank_transfer\"]/button")));
        banktranfer.click();
        Thread.sleep(2000);

//        WebElement mensajesbt = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("bank_transfer")));
//        mensajesbt.clear();
//        mensajesbt.sendKeys(" BankTransfer");
//        Thread.sleep(2000);

        WebElement banktranfercl = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"Bank_transfer\"]/button")));
        banktranfercl.click();
        Thread.sleep(2000);

        WebElement stripe = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"Stripe\"]/button")));
        stripe.click();
        Thread.sleep(2000);

//        WebElement publicablestripe = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("stripe_publishable_key")));
//        publicablestripe.sendKeys("pk_test_51I8wDiH92DQUN2aQR83DqlfWngUGe7tbW0lJfNcBStXBNul0s1EiLjLsCV1WK4ROWuQKdjq0hEJLaLeSubJSy9zR00SCwtNhoo");
//        Thread.sleep(2000);
//
//        WebElement privatestripe = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("stripe_secret_key")));
//        privatestripe.clear();
//        privatestripe.sendKeys("sk_test_51I8wDiH92DQUN2aQu3Bbu7INic24tOYutZJ5pXZdCyv12iDILr2agncoMacK3Og3GiMMaebNMBo6dnPJsnfhlYtW00tMNOGI6T");
//        Thread.sleep(2000);

//        WebElement mensajest = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("stripe_unfo")));
//        mensajest.clear();
//        mensajest.sendKeys(" STRIPE");
//        Thread.sleep(2000);

        WebElement stripecl = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"Stripe\"]/button")));
        stripecl.click();
        Thread.sleep(2000);

        WebElement mercadocl = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"mercado\"]/button")));
        mercadocl.click();
        Thread.sleep(2000);

//        WebElement mensajesme= wait.until(ExpectedConditions.presenceOfElementLocated(By.name("mercado_unfo")));
//        mensajesme.clear();
//        mensajesme.sendKeys(" Mercado Pago");
//        Thread.sleep(2000);

        WebElement mercadoclcls = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"mercado\"]/button")));
        mercadoclcls.click();
        Thread.sleep(2000);


        WebElement paver = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"Stripe\"]/button")));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", paver);
        Thread.sleep(2000);

        WebElement paypalbt = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"Paypal\"]/button")));
        paypalbt.click();
        Thread.sleep(2000);

//        WebElement mensajespay= wait.until(ExpectedConditions.presenceOfElementLocated(By.name("paypal_unfo")));
//        mensajespay.clear();
//        mensajespay.sendKeys("Paypal");
//        Thread.sleep(2000);

        WebElement paypalbtcl = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"Paypal\"]/button")));
        paypalbtcl.click();
        Thread.sleep(2000);


        WebElement whatsappbt = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"whatsapp\"]/button")));
        whatsappbt.click();
        Thread.sleep(2000);
//
//        WebElement mensajeswhatsapp= wait.until(ExpectedConditions.presenceOfElementLocated(By.name("whatsapp_unfo")));
//        mensajeswhatsapp.clear();
//        mensajeswhatsapp.sendKeys("Whatsapp");
//        Thread.sleep(2000);

        WebElement whatsappbtclo = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"whatsapp\"]/button")));
        whatsappbtclo.click();
        Thread.sleep(2000);


        WebElement paver2 = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"Paypal\"]/button")));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", paver2);
        Thread.sleep(2000);

        WebElement authorizebt = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"authorizenet\"]/button")));
        authorizebt.click();
        Thread.sleep(2000);

//        WebElement mensajesautho= wait.until(ExpectedConditions.presenceOfElementLocated(By.name("authorizenet_unfo")));
//        mensajesautho.clear();
//        mensajesautho.sendKeys("Authorize Net");
//        Thread.sleep(2000);

        WebElement authorizebtcl = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"authorizenet\"]/button")));
        authorizebtcl.click();
        Thread.sleep(2000);


        WebElement savechanges = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"Payment_Setting\"]/div/div[2]/form/div[2]/div/input")));
        savechanges.click();
        Thread.sleep(2000);

        WebElement back = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"html-dir-tag\"]/body/nav/div[1]/div[1]/a")));
        back.click();
        Thread.sleep(2000);

        WebElement boton = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".cust-btn > .ti-chevron-down")));
        boton.click();
        Thread.sleep(5000);

    }

    private void comprarbanktransfer() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20)); // Aumentamos el tiempo de espera

        String mainTab = driver.getWindowHandle();
        System.out.println("Main Tab:" + mainTab);

        driver.findElement(By.xpath("//*[@id=\"storeList\"]/a[4]/img")).click();
        Thread.sleep(3000);
        // Esperar hasta que la nueva ventana esté disponible y cambiar a ella
        wait.until(ExpectedConditions.numberOfWindowsToBe(2));

        Set<String> handles = driver.getWindowHandles();
        for (String actual : handles) {
            System.out.println("Handle ID:" + actual);
            if (!actual.equalsIgnoreCase(mainTab)) {
                System.out.println("Changing Tab");
                driver.switchTo().window(actual);

                // Realizar todas las acciones en la nueva ventana aquí


                Thread.sleep(2000);

                WebElement iniciarsesion = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/header/div[3]/div/div[2]/div[3]/ul/li[2]/a")));
                iniciarsesion.click();
                Thread.sleep(2000);


                //INICIAR SESION E LA TIENDA CON UNA CUENTA YA REGISTRADA


                //SE DEBE DE TENER UNA CUENTA YA REGISTRADA, CAMBIAR LOS CAMPOS POR LA QUE HAN CREADO
                WebElement emailField = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("email")));
                emailField.clear();
                Thread.sleep(2000);
                emailField.sendKeys("vycprueba@gmail.com");
                Thread.sleep(2000);

                WebElement passwordField = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("password")));
                passwordField.clear();
                Thread.sleep(2000);
                passwordField.sendKeys("12345678");
                Thread.sleep(2000);

                WebElement boton = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("login_button")));
                boton.click();
                Thread.sleep(2000);

                WebElement reselection = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".btn.btn-white")));
                reselection.click();
                Thread.sleep(2000);

                WebElement view = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/section[2]/div/div[2]/div[2]/div/div[1]/div[1]/div")));
                view.click();
                Thread.sleep(2000);


                WebElement btnag = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/section[1]/div/div/div[2]/div/div/button")));
                btnag.click();
                Thread.sleep(7000);


                WebElement btncarrito = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/header/div[3]/div/div[2]/div[3]/ul/li[4]")));
                btncarrito.click();
                Thread.sleep(2000);


                WebElement btncheckout = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"cart-body\"]/div[2]/a[1]/center/button")));
                btncheckout.click();
                Thread.sleep(2000);

                WebElement btncheckout2 = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/section[1]/div/div[2]/div[2]/div/a")));
                btncheckout2.click();
                Thread.sleep(2000);




                WebElement billing = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("billing_info[billing_address]")));
                billing.sendKeys("Sauces");
                Thread.sleep(2000);


                // DIRRECION DE FACTURACION
                WebElement dropdowncountry = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div.nice-select.form-control.country_change")));
                dropdowncountry.click();

                // Espera hasta que las opciones sean visibles y haz clic en la opción deseada usando JavaScript
                WebElement option = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("li.option[data-value='11']")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", option);


                WebElement cp = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("billing_info[billing_postecode]")));
                cp.sendKeys("55766");
                Thread.sleep(2000);



                WebElement dropdownstate = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div.nice-select.form-control.state_name.state_chage")));
                dropdownstate.click();
                Thread.sleep(2000);


                // Espera hasta que las opciones sean visibles y haz clic en la opción deseada usando JavaScript
                WebElement optionstate = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("li.option[data-value='3634']")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", optionstate);
                Thread.sleep(2000);



                WebElement dropdowncity = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div.nice-select.form-control.city_change")));
                dropdowncity.click();

                // Espera hasta que las opciones sean visibles y haz clic en la opción deseada usando JavaScript
                WebElement optioncity = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("li.option[data-value='1']")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", optioncity);



                // Entrega
                WebElement deliveryAddress = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("billing_info[delivery_address]")));
                deliveryAddress.sendKeys("Loma");
                Thread.sleep(2000);

                // Selección del país
                WebElement dropdownCountryDelivery = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//section[@class='Delivery_Address']//div[@class='nice-select form-control country_change']")));
                dropdownCountryDelivery.click();

                WebElement optionCountryDelivery = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//section[@class='Delivery_Address']//li[@data-value='11']")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", optionCountryDelivery);
                Thread.sleep(2000);

                WebElement cpadress = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("billing_info[delivery_postcode]")));
                cpadress.sendKeys("55766");
                Thread.sleep(2000);

                // Selección del estado
                WebElement dropdownStateDelivery = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//section[@class='Delivery_Address']//div[@class='nice-select form-control state_name state_chage delivery_list']")));
                dropdownStateDelivery.click();
                Thread.sleep(2000);

                WebElement optionStateDelivery = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//section[@class='Delivery_Address']//li[@data-value='3634']")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", optionStateDelivery);
                Thread.sleep(2000);

                // Selección de la ciudad
                WebElement dropdownCityDelivery = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//section[@class='Delivery_Address']//div[@class='nice-select form-control city_change delivery_list']")));
                dropdownCityDelivery.click();

                WebElement optionCityDelivery = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//section[@class='Delivery_Address']//li[@data-value='1']")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", optionCityDelivery);
                Thread.sleep(2000);


                // Esperar a que el botón "Continuar" esté presente y hacer clic en él usando JavascriptExecutor
                WebElement continuarCheckout = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".d-flex.acc-back-btn-wrp.align-items-center.justify-content-end .btn.continue-btn.confirm_btn.billing_done")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", continuarCheckout);
                Thread.sleep(2000);




                WebElement paymenttype = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"formdata\"]/div[1]/div[2]/div/div/div[1]/div/label")));
                paymenttype.click();
                Thread.sleep(2000);

                WebElement paver3 = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"formdata\"]/div[1]/div[2]/div/div/div[5]/div/label")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", paver3);
                Thread.sleep(2000);

                WebElement terminosyc = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"formdata\"]/div[1]/div[2]/div/div/div[8]/div/div/label")));
                terminosyc.click();
                Thread.sleep(2000);

                // Esperar a que el botón "Continuar" esté presente y hacer clic en él usando JavascriptExecutor
                WebElement continueEnd = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"formdata\"]/div[1]/div[2]/div/div/div[8]/div/button")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", continueEnd);
                Thread.sleep(2000);

                // Esperar a que el formulario "payfast_form" esté presente y hacer clic en él usando JavascriptExecutor
                WebElement pagar = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"payfast_form\"]")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", pagar);
                Thread.sleep(2000);


                WebElement element = driver.findElement(By.tagName("body"));
                String bodyText = element.getText();
                assertTrue(bodyText.contains("Su pedido"), "¡Texto no encontrado !");

                WebElement volveralpanel = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/section/div/div/div/div/div[2]/a")));
                volveralpanel.click();
                Thread.sleep(2000);


}}}

    private void comprarstripe() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20)); // Aumentamos el tiempo de espera



                WebElement reselection = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".btn.btn-white")));
                reselection.click();
                Thread.sleep(2000);

                WebElement view = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/section[2]/div/div[2]/div[2]/div/div[1]/div[1]/div")));
                view.click();
                Thread.sleep(2000);


                WebElement btnag = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/section[1]/div/div/div[2]/div/div/button")));
                btnag.click();
                Thread.sleep(7000);


                WebElement btncarrito = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/header/div[3]/div/div[2]/div[3]/ul/li[4]")));
                btncarrito.click();
                Thread.sleep(2000);


                WebElement btncheckout = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"cart-body\"]/div[2]/a[1]/center/button")));
                btncheckout.click();
                Thread.sleep(2000);

                WebElement btncheckout2 = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/section[1]/div/div[2]/div[2]/div/a")));
                btncheckout2.click();
                Thread.sleep(2000);


                WebElement billing = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("billing_info[billing_address]")));
                billing.sendKeys("Sauces");
                Thread.sleep(2000);


                // DIRRECION DE FACTURACION
                WebElement dropdowncountry = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div.nice-select.form-control.country_change")));
                dropdowncountry.click();

                // Espera hasta que las opciones sean visibles y haz clic en la opción deseada usando JavaScript
                WebElement option = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("li.option[data-value='11']")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", option);


                WebElement cp = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("billing_info[billing_postecode]")));
                cp.sendKeys("55766");
                Thread.sleep(2000);


                WebElement dropdownstate = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div.nice-select.form-control.state_name.state_chage")));
                dropdownstate.click();
                Thread.sleep(2000);

                // Espera hasta que las opciones sean visibles y haz clic en la opción deseada usando JavaScript
                WebElement optionstate = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("li.option[data-value='3634']")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", optionstate);
                Thread.sleep(2000);


                WebElement dropdowncity = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div.nice-select.form-control.city_change")));
                dropdowncity.click();

                // Espera hasta que las opciones sean visibles y haz clic en la opción deseada usando JavaScript
                WebElement optioncity = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("li.option[data-value='1']")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", optioncity);



                // Entrega
                WebElement deliveryAddress = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("billing_info[delivery_address]")));
                deliveryAddress.sendKeys("Loma");
                Thread.sleep(2000);

                // Selección del país
                WebElement dropdownCountryDelivery = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//section[@class='Delivery_Address']//div[@class='nice-select form-control country_change']")));
                dropdownCountryDelivery.click();

                WebElement optionCountryDelivery = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//section[@class='Delivery_Address']//li[@data-value='11']")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", optionCountryDelivery);
                Thread.sleep(2000);

                WebElement cpadress = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("billing_info[delivery_postcode]")));
                cpadress.sendKeys("55766");
                Thread.sleep(2000);

                // Selección del estado
                WebElement dropdownStateDelivery = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//section[@class='Delivery_Address']//div[@class='nice-select form-control state_name state_chage delivery_list']")));
                dropdownStateDelivery.click();
                Thread.sleep(2000);

                WebElement optionStateDelivery = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//section[@class='Delivery_Address']//li[@data-value='3634']")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", optionStateDelivery);
                Thread.sleep(2000);

                // Selección de la ciudad
                WebElement dropdownCityDelivery = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//section[@class='Delivery_Address']//div[@class='nice-select form-control city_change delivery_list']")));
                dropdownCityDelivery.click();

                WebElement optionCityDelivery = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//section[@class='Delivery_Address']//li[@data-value='1']")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", optionCityDelivery);
                Thread.sleep(2000);


                // Esperar a que el botón "Continuar" esté presente y hacer clic en él usando JavascriptExecutor
                WebElement continuarCheckout = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".d-flex.acc-back-btn-wrp.align-items-center.justify-content-end .btn.continue-btn.confirm_btn.billing_done")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", continuarCheckout);
                Thread.sleep(2000);


                WebElement paymenttype = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"formdata\"]/div[1]/div[2]/div/div/div[2]/div/label")));
                paymenttype.click();
                Thread.sleep(2000);

                WebElement paver3 = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"formdata\"]/div[1]/div[2]/div/div/div[5]/div/label")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", paver3);
                Thread.sleep(2000);

                WebElement terminosyc = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"formdata\"]/div[1]/div[2]/div/div/div[8]/div/div/label")));
                terminosyc.click();
                Thread.sleep(2000);

                // Esperar a que el botón "Continuar" esté presente y hacer clic en él usando JavascriptExecutor
                WebElement continueEnd = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"formdata\"]/div[1]/div[2]/div/div/div[8]/div/button")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", continueEnd);
                Thread.sleep(2000);

                // Esperar a que el formulario "payfast_form" esté presente y hacer clic en él usando JavascriptExecutor
                WebElement pagar = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"payfast_form\"]")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", pagar);
                Thread.sleep(2000);

                //YA EN STRIPE PARA PAGAR

                //Paso 7.2: El usuario ingresa los detalles del pago (número de tarjeta, fecha de vencimiento, CVV).

                WebElement emailstripe = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("email")));
                emailstripe.sendKeys("pruebacyv@gmail.com");
                Thread.sleep(2000);

                WebElement cardNumber = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("cardNumber")));
                cardNumber.sendKeys("4000000320000021");
                Thread.sleep(2000);

                WebElement cardExpiry = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("cardExpiry")));
                cardExpiry.sendKeys("0427");
                Thread.sleep(2000);


                WebElement cardCvc = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("cardCvc")));
                cardCvc.sendKeys("123");
                Thread.sleep(2000);

                WebElement billingName = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("billingName")));
                billingName.sendKeys("Prueba CYV");
                Thread.sleep(2000);


                //Paso 8.2: El usuario hace clic en "Confirmar Pedido" o "Realizar Pedido".

                WebElement submitbutton = wait.until(ExpectedConditions.presenceOfElementLocated(By.className("SubmitButton-IconContainer")));
                submitbutton.click();
                Thread.sleep(5000);

                WebElement element2 = driver.findElement(By.tagName("body"));
                String bodyText2 = element2.getText();
                assertTrue(bodyText2.contains("#"), "¡Texto no encontrado 2 !");

                WebElement volveralpanel2 = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/section/div/div/div/div/div[2]/a")));
                volveralpanel2.click();
                Thread.sleep(2000);



            }

    private void comprarpaypal() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20)); // Aumentamos el tiempo de espera



        WebElement reselection = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".btn.btn-white")));
        reselection.click();
        Thread.sleep(2000);

        WebElement view = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/section[2]/div/div[2]/div[2]/div/div[1]/div[1]/div")));
        view.click();
        Thread.sleep(2000);


        WebElement btnag = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/section[1]/div/div/div[2]/div/div/button")));
        btnag.click();
        Thread.sleep(7000);


        WebElement btncarrito = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/header/div[3]/div/div[2]/div[3]/ul/li[4]")));
        btncarrito.click();
        Thread.sleep(2000);


        WebElement btncheckout = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"cart-body\"]/div[2]/a[1]/center/button")));
        btncheckout.click();
        Thread.sleep(2000);

        WebElement btncheckout2 = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/section[1]/div/div[2]/div[2]/div/a")));
        btncheckout2.click();
        Thread.sleep(2000);


        WebElement billing = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("billing_info[billing_address]")));
        billing.sendKeys("Sauces");
        Thread.sleep(2000);


        // DIRRECION DE FACTURACION
        WebElement dropdowncountry = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div.nice-select.form-control.country_change")));
        dropdowncountry.click();

        // Espera hasta que las opciones sean visibles y haz clic en la opción deseada usando JavaScript
        WebElement option = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("li.option[data-value='11']")));
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", option);


        WebElement cp = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("billing_info[billing_postecode]")));
        cp.sendKeys("55766");
        Thread.sleep(2000);


        WebElement dropdownstate = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div.nice-select.form-control.state_name.state_chage")));
        dropdownstate.click();
        Thread.sleep(2000);

        // Espera hasta que las opciones sean visibles y haz clic en la opción deseada usando JavaScript
        WebElement optionstate = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("li.option[data-value='3634']")));
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", optionstate);
        Thread.sleep(2000);


        WebElement dropdowncity = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div.nice-select.form-control.city_change")));
        dropdowncity.click();

        // Espera hasta que las opciones sean visibles y haz clic en la opción deseada usando JavaScript
        WebElement optioncity = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("li.option[data-value='1']")));
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", optioncity);



        // Entrega
        WebElement deliveryAddress = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("billing_info[delivery_address]")));
        deliveryAddress.sendKeys("Loma");
        Thread.sleep(2000);

        // Selección del país
        WebElement dropdownCountryDelivery = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//section[@class='Delivery_Address']//div[@class='nice-select form-control country_change']")));
        dropdownCountryDelivery.click();

        WebElement optionCountryDelivery = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//section[@class='Delivery_Address']//li[@data-value='11']")));
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", optionCountryDelivery);
        Thread.sleep(2000);

        WebElement cpadress = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("billing_info[delivery_postcode]")));
        cpadress.sendKeys("55766");
        Thread.sleep(2000);

        // Selección del estado
        WebElement dropdownStateDelivery = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//section[@class='Delivery_Address']//div[@class='nice-select form-control state_name state_chage delivery_list']")));
        dropdownStateDelivery.click();
        Thread.sleep(2000);

        WebElement optionStateDelivery = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//section[@class='Delivery_Address']//li[@data-value='3634']")));
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", optionStateDelivery);
        Thread.sleep(2000);

        // Selección de la ciudad
        WebElement dropdownCityDelivery = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//section[@class='Delivery_Address']//div[@class='nice-select form-control city_change delivery_list']")));
        dropdownCityDelivery.click();

        WebElement optionCityDelivery = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//section[@class='Delivery_Address']//li[@data-value='1']")));
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", optionCityDelivery);
        Thread.sleep(2000);


        // Esperar a que el botón "Continuar" esté presente y hacer clic en él usando JavascriptExecutor
        WebElement continuarCheckout = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".d-flex.acc-back-btn-wrp.align-items-center.justify-content-end .btn.continue-btn.confirm_btn.billing_done")));
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", continuarCheckout);
        Thread.sleep(2000);


        WebElement paymenttype = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"formdata\"]/div[1]/div[2]/div/div/div[4]/div/label")));
        paymenttype.click();
        Thread.sleep(2000);

        WebElement paver4 = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"formdata\"]/div[1]/div[2]/div/div/div[5]/div/label")));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", paver4);
        Thread.sleep(2000);

        WebElement terminosyc = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"formdata\"]/div[1]/div[2]/div/div/div[8]/div/div/label")));
        terminosyc.click();
        Thread.sleep(2000);

        // Esperar a que el botón "Continuar" esté presente y hacer clic en él usando JavascriptExecutor
        WebElement continueEnd = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"formdata\"]/div[1]/div[2]/div/div/div[8]/div/button")));
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", continueEnd);
        Thread.sleep(2000);

        // Esperar a que el formulario "payfast_form" esté presente y hacer clic en él usando JavascriptExecutor
        WebElement pagar = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"payfast_form\"]")));
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", pagar);
        Thread.sleep(2000);


        WebElement login_email = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("email")));
        login_email.sendKeys("sb-s1dwq30747151@personal.example.com");

        Thread.sleep(2000);

        WebElement btnNext = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("btnNext")));
        btnNext.click();

        Thread.sleep(2000);

        WebElement passwordpaypal = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("password")));
        passwordpaypal.sendKeys("#)-mA5nu");

        Thread.sleep(2000);


        WebElement btnLogin = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("btnLogin")));
        btnLogin.click();

        Thread.sleep(2000);


        WebElement paymentpays = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("payment-submit-btn")));
        paymentpays.click();
        Thread.sleep(3000);

        WebElement element3 = driver.findElement(By.tagName("body"));
        String bodyText3 = element3.getText();
        assertTrue(bodyText3.contains("Su pedido"), "¡Texto no encontrado 3 !");


        WebElement volveralpanel3 = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/section/div/div/div/div/div[2]/a")));
        volveralpanel3.click();
        Thread.sleep(2000);


    }


    private void comprarauthorize() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20)); // Aumentamos el tiempo de espera



        WebElement reselection = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".btn.btn-white")));
        reselection.click();
        Thread.sleep(2000);

        WebElement view = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/section[2]/div/div[2]/div[2]/div/div[1]/div[1]/div")));
        view.click();
        Thread.sleep(2000);


        WebElement btnag = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/section[1]/div/div/div[2]/div/div/button")));
        btnag.click();
        Thread.sleep(7000);


        WebElement btncarrito = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/header/div[3]/div/div[2]/div[3]/ul/li[4]")));
        btncarrito.click();
        Thread.sleep(2000);


        WebElement btncheckout = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"cart-body\"]/div[2]/a[1]/center/button")));
        btncheckout.click();
        Thread.sleep(2000);

        WebElement btncheckout2 = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/section[1]/div/div[2]/div[2]/div/a")));
        btncheckout2.click();
        Thread.sleep(2000);


        WebElement billing = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("billing_info[billing_address]")));
        billing.sendKeys("Sauces");
        Thread.sleep(2000);


        // DIRRECION DE FACTURACION
        WebElement dropdowncountry = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div.nice-select.form-control.country_change")));
        dropdowncountry.click();

        // Espera hasta que las opciones sean visibles y haz clic en la opción deseada usando JavaScript
        WebElement option = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("li.option[data-value='11']")));
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", option);


        WebElement cp = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("billing_info[billing_postecode]")));
        cp.sendKeys("55766");
        Thread.sleep(2000);


        WebElement dropdownstate = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div.nice-select.form-control.state_name.state_chage")));
        dropdownstate.click();
        Thread.sleep(2000);

        // Espera hasta que las opciones sean visibles y haz clic en la opción deseada usando JavaScript
        WebElement optionstate = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("li.option[data-value='3634']")));
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", optionstate);
        Thread.sleep(2000);


        WebElement dropdowncity = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div.nice-select.form-control.city_change")));
        dropdowncity.click();

        // Espera hasta que las opciones sean visibles y haz clic en la opción deseada usando JavaScript
        WebElement optioncity = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("li.option[data-value='1']")));
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", optioncity);



        // Entrega
        WebElement deliveryAddress = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("billing_info[delivery_address]")));
        deliveryAddress.sendKeys("Loma");
        Thread.sleep(2000);

        // Selección del país
        WebElement dropdownCountryDelivery = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//section[@class='Delivery_Address']//div[@class='nice-select form-control country_change']")));
        dropdownCountryDelivery.click();

        WebElement optionCountryDelivery = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//section[@class='Delivery_Address']//li[@data-value='11']")));
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", optionCountryDelivery);
        Thread.sleep(2000);

        WebElement cpadress = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("billing_info[delivery_postcode]")));
        cpadress.sendKeys("55766");
        Thread.sleep(2000);

        // Selección del estado
        WebElement dropdownStateDelivery = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//section[@class='Delivery_Address']//div[@class='nice-select form-control state_name state_chage delivery_list']")));
        dropdownStateDelivery.click();
        Thread.sleep(2000);

        WebElement optionStateDelivery = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//section[@class='Delivery_Address']//li[@data-value='3634']")));
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", optionStateDelivery);
        Thread.sleep(2000);

        // Selección de la ciudad
        WebElement dropdownCityDelivery = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//section[@class='Delivery_Address']//div[@class='nice-select form-control city_change delivery_list']")));
        dropdownCityDelivery.click();

        WebElement optionCityDelivery = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//section[@class='Delivery_Address']//li[@data-value='1']")));
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", optionCityDelivery);
        Thread.sleep(2000);


        // Esperar a que el botón "Continuar" esté presente y hacer clic en él usando JavascriptExecutor
        WebElement continuarCheckout = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".d-flex.acc-back-btn-wrp.align-items-center.justify-content-end .btn.continue-btn.confirm_btn.billing_done")));
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", continuarCheckout);
        Thread.sleep(2000);


        WebElement paymenttype = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"formdata\"]/div[1]/div[2]/div/div/div[6]/div/label")));
        paymenttype.click();
        Thread.sleep(2000);

        WebElement paver4 = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"formdata\"]/div[1]/div[2]/div/div/div[5]/div/label")));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", paver4);
        Thread.sleep(2000);

        WebElement terminosyc = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"formdata\"]/div[1]/div[2]/div/div/div[8]/div/div/label")));
        terminosyc.click();
        Thread.sleep(2000);

        // Esperar a que el botón "Continuar" esté presente y hacer clic en él usando JavascriptExecutor
        WebElement continueEnd = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"formdata\"]/div[1]/div[2]/div/div/div[8]/div/button")));
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", continueEnd);
        Thread.sleep(2000);

        // Esperar a que el formulario "payfast_form" esté presente y hacer clic en él usando JavascriptExecutor
        WebElement pagar = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"payfast_form\"]")));
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", pagar);
        Thread.sleep(2000);


        WebElement authorizenetForm = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("cardNumber")));
        authorizenetForm.sendKeys("4000000320000021");

        WebElement cvv = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("cvv")));
        cvv.sendKeys("123");

        WebElement monthautho = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"authorizenetForm\"]/div/div[5]/div/input[1]")));
        monthautho.sendKeys("01");

        WebElement age = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"authorizenetForm\"]/div/div[5]/div/input[2]")));
        age.sendKeys("2027");

        WebElement clickautho = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"authorizenetForm\"]/div/div[6]/button")));
        clickautho.click();

        WebElement element4 = driver.findElement(By.tagName("body"));
        String bodyText4 = element4.getText();
        assertTrue(bodyText4.contains("Su pedido"), "¡Texto no encontrado 4 !");

        WebElement volveralpanel3 = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/section/div/div/div/div/div[2]/a")));
        volveralpanel3.click();
        Thread.sleep(2000);


    }


}



