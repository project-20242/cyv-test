package net.tecgurus.clickyvende.home;

import net.tecgurus.clickyvende.BaseTest;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class AdvertencianoMostrada extends BaseTest {

    @Test
    public void testMostrarAdvertencia() throws InterruptedException {
        // Ingresar a la página
        LoginYVerTienda();

        // Comprar productos
        comprar();


    }



    private void LoginYVerTienda() throws InterruptedException {
        driver.manage().window().maximize();
        driver.get(baseUrl + "login");

        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20)); // Aumentamos el tiempo de espera

        // Definimos variables
        WebElement usernameInput = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("email")));
        WebElement passwordInput = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("password")));
        WebElement loginBtn = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".btn.btn-primary.btn-block.mt-2.login_button")));
        WebElement accept = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("c-p-bn")));

        Thread.sleep(2000);

        // Ingresamos las credenciales y hacemos clic en el botón de login
        usernameInput.sendKeys("chamoy@gmail.com");
        passwordInput.sendKeys("12345678");
        accept.click();
        loginBtn.click();

        WebElement boton = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".cust-btn > .ti-chevron-down")));
        boton.click();
        Thread.sleep(5000);
    }

    private void comprar() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20)); // Aumentamos el tiempo de espera

        String mainTab = driver.getWindowHandle();
        System.out.println("Main Tab:" + mainTab);

        driver.findElement(By.xpath("//*[@id=\"storeList\"]/a[4]/img")).click();
        Thread.sleep(3000);
        // Esperar hasta que la nueva ventana esté disponible y cambiar a ella
        wait.until(ExpectedConditions.numberOfWindowsToBe(2));

        Set<String> handles = driver.getWindowHandles();
        for (String actual : handles) {
            System.out.println("Handle ID:" + actual);
            if (!actual.equalsIgnoreCase(mainTab)) {
                System.out.println("Changing Tab");
                driver.switchTo().window(actual);

                // Realizar todas las acciones en la nueva ventana aquí


                Thread.sleep(2000);

                WebElement iniciarsesion = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/header/div[3]/div/div[2]/div[3]/ul/li[2]/a")));
                iniciarsesion.click();
                Thread.sleep(2000);


                //INICIAR SESION E LA TIENDA CON UNA CUENTA YA REGISTRADA


                //SE DEBE DE TENER UNA CUENTA YA REGISTRADA, CAMBIAR LOS CAMPOS POR LA QUE HAN CREADO
                WebElement emailField = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("email")));
                emailField.clear();
                Thread.sleep(2000);
                emailField.sendKeys("totodiaz0711@gmail.com");
                Thread.sleep(2000);

                WebElement passwordField = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("password")));
                passwordField.clear();
                Thread.sleep(2000);
                passwordField.sendKeys("12345678");
                Thread.sleep(2000);

                WebElement boton = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("login_button")));
                boton.click();
                Thread.sleep(2000);


                WebElement reselection = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".btn.btn-white")));
                reselection.click();
                Thread.sleep(2000);



                WebElement view = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/section[2]/div/div[2]/div[2]/div/div[1]/div[1]/div")));
                view.click();
                Thread.sleep(2000);



                WebElement addcantidad = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/section[1]/div/div/div[2]/div/div/div[4]/div[1]/form/div/div/button[2]")));
                addcantidad.click();
                Thread.sleep(2000);

                WebElement btnag = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/section[1]/div/div/div[2]/div/div/button")));
                btnag.click();
                Thread.sleep(7000);


                WebElement btncarrito = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/header/div[3]/div/div[2]/div[3]/ul/li[4]")));
                btncarrito.click();
                Thread.sleep(2000);



                WebElement btncheckout = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"cart-body\"]/div[2]/a[1]/center/button")));
                btncheckout.click();
                Thread.sleep(2000);

                WebElement btncheckout2 = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/section[1]/div/div[2]/div[2]/div/a")));
                btncheckout2.click();
                Thread.sleep(2000);


                //MOSTRAR ERROR DE METODO DE PAGO

                WebElement billing = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("billing_info[billing_address]")));
                billing.sendKeys("Sauces");
                Thread.sleep(2000);


                // DIRRECION DE FACTURACION
                WebElement dropdowncountry = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div.nice-select.form-control.country_change")));
                dropdowncountry.click();

                // Espera hasta que las opciones sean visibles y haz clic en la opción deseada usando JavaScript
                WebElement option = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("li.option[data-value='11']")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", option);


                WebElement cp = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("billing_info[billing_postecode]")));
                cp.sendKeys("55766");
                Thread.sleep(2000);



                WebElement dropdownstate = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div.nice-select.form-control.state_name.state_chage")));
                dropdownstate.click();
                Thread.sleep(2000);


                // Espera hasta que las opciones sean visibles y haz clic en la opción deseada usando JavaScript
                WebElement optionstate = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("li.option[data-value='3634']")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", optionstate);
                Thread.sleep(2000);



                WebElement dropdowncity = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div.nice-select.form-control.city_change")));
                dropdowncity.click();

                // Espera hasta que las opciones sean visibles y haz clic en la opción deseada usando JavaScript
                WebElement optioncity = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("li.option[data-value='1']")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", optioncity);



                // Entrega
                WebElement deliveryAddress = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("billing_info[delivery_address]")));
                deliveryAddress.sendKeys("Loma");
                Thread.sleep(2000);

                // Selección del país
                WebElement dropdownCountryDelivery = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//section[@class='Delivery_Address']//div[@class='nice-select form-control country_change']")));
                dropdownCountryDelivery.click();

                WebElement optionCountryDelivery = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//section[@class='Delivery_Address']//li[@data-value='11']")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", optionCountryDelivery);
                Thread.sleep(2000);

                WebElement cpadress = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("billing_info[delivery_postcode]")));
                cpadress.sendKeys("55766");
                Thread.sleep(2000);

                // Espera hasta que el menú desplegable del estado sea clickable y luego haz clic en él
                WebElement dropdownStateDelivery = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//section[@class='Delivery_Address']//div[@class='nice-select form-control state_name state_chage delivery_list']")));
                dropdownStateDelivery.click();
                Thread.sleep(2000);

                // Espera hasta que la opción de Catamarca sea clickable y luego haz clic en ella
                WebElement optionStateDelivery = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//section[@class='Delivery_Address']//li[@data-value='3635']")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", optionStateDelivery);
                Thread.sleep(2000);

                // Espera hasta que el menú desplegable de la ciudad sea clickable y luego haz clic en él
                WebElement dropdownCityDelivery = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//section[@class='Delivery_Address']//div[@class='nice-select form-control city_change delivery_list']")));
                dropdownCityDelivery.click();
                Thread.sleep(2000);

                // Espera hasta que la opción de Ambato sea clickable y luego haz clic en ella
                WebElement optionCityDelivery = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//section[@class='Delivery_Address']//li[@data-value='140']")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", optionCityDelivery);
                Thread.sleep(2000);


                WebElement continuarCheckout = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".d-flex.acc-back-btn-wrp.align-items-center.justify-content-end .btn.continue-btn.confirm_btn.billing_done")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", continuarCheckout);
                Thread.sleep(2000);




                WebElement paymenttype = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"formdata\"]/div[1]/div[2]/div/div/div[2]/div/label")));
                paymenttype.click();
                Thread.sleep(2000);

                WebElement produdesta = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"formdata\"]/div[1]/div[2]/div/div/div[3]/div/label")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", produdesta);
                Thread.sleep(2000);

                WebElement terminosyc = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"formdata\"]/div[1]/div[2]/div/div/div[8]/div/div/label")));
                terminosyc.click();
                Thread.sleep(2000);

                // Esperar a que el botón "Continuar" esté presente y hacer clic en él usando JavascriptExecutor
                WebElement continueEnd = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"formdata\"]/div[1]/div[2]/div/div/div[8]/div/button")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", continueEnd);
                Thread.sleep(2000);

                // Esperar a que el formulario "payfast_form" esté presente y hacer clic en él usando JavascriptExecutor
                WebElement pagar = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"payfast_form\"]")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", pagar);
                Thread.sleep(4000);


                WebElement element = driver.findElement(By.tagName("body"));

                String bodyText = element.getText();

                assertTrue(bodyText.contains("This shipping method does not work, choose another one or contact the administrator."), "¡Texto no encontrado!");


                //MOSTRAR MENSAJE DE ERROR DE METODO DE PAGO

                //FACTURACION

                WebElement billings = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("billing_info[billing_address]")));
                billings.sendKeys("Sauces");
                Thread.sleep(2000);


                WebElement cpbill = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("billing_info[billing_postecode]")));
                cpbill.sendKeys("55766");
                Thread.sleep(2000);

                // Espera hasta que el menú desplegable del país sea clickable y luego haz clic en él
                WebElement dropdownCountryBilling = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//section//div[@class='nice-select form-control country_change']")));
                dropdownCountryBilling.click();
                Thread.sleep(2000);

                // Espera hasta que la opción de Argentina sea clickable y luego haz clic en ella
                WebElement optionCountryBilling = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//section//li[@data-value='11']")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", optionCountryBilling);
                Thread.sleep(2000);

                // Selecciona el estado de Buenos Aires en la dirección de facturación
                WebElement dropdownStateBilling = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='form-container']//div[@class='nice-select form-control state_name state_chage']")));
                dropdownStateBilling.click();
                Thread.sleep(2000);

                WebElement optionStateBilling = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='form-container']//li[@data-value='3634']")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", optionStateBilling);
                Thread.sleep(2000);

                WebElement dropdowncity2 = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div.nice-select.form-control.city_change")));
                dropdowncity2.click();
                Thread.sleep(2000);

                // Espera hasta que las opciones sean visibles y haz clic en la opción deseada usando JavaScript
                WebElement optioncity2 = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("li.option[data-value='1']")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", optioncity2);


                // Entrega
                WebElement deliveryAddress2 = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("billing_info[delivery_address]")));
                deliveryAddress2.sendKeys("Loma");
                Thread.sleep(2000);

                // Selección del país
                WebElement dropdownCountryDelivery2 = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//section[@class='Delivery_Address']//div[@class='nice-select form-control country_change']")));
                dropdownCountryDelivery2.click();

                WebElement optionCountryDelivery2 = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//section[@class='Delivery_Address']//li[@data-value='11']")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", optionCountryDelivery2);
                Thread.sleep(2000);

                WebElement cpadress2 = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("billing_info[delivery_postcode]")));
                cpadress2.sendKeys("55766");
                Thread.sleep(2000);

                // Selección del estado
                WebElement dropdownStateDelivery2 = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//section[@class='Delivery_Address']//div[@class='nice-select form-control state_name state_chage delivery_list']")));
                dropdownStateDelivery2.click();
                Thread.sleep(2000);

                WebElement optionStateDelivery2 = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//section[@class='Delivery_Address']//li[@data-value='3634']")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", optionStateDelivery2);
                Thread.sleep(2000);

                // Selección de la ciudad
                WebElement dropdownCityDelivery2 = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//section[@class='Delivery_Address']//div[@class='nice-select form-control city_change delivery_list']")));
                dropdownCityDelivery2.click();

                WebElement optionCityDelivery2 = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//section[@class='Delivery_Address']//li[@data-value='1']")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", optionCityDelivery2);
                Thread.sleep(2000);


                // Esperar a que el botón "Continuar" esté presente y hacer clic en él usando JavascriptExecutor
                WebElement continuarCheckout2 = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".d-flex.acc-back-btn-wrp.align-items-center.justify-content-end .btn.continue-btn.confirm_btn.billing_done")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", continuarCheckout2);
                Thread.sleep(2000);

                WebElement paymenttype2 = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"formdata\"]/div[1]/div[2]/div/div/div[4]/div/label")));
                paymenttype2.click();
                Thread.sleep(2000);

                WebElement produdesta2 = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"formdata\"]/div[1]/div[2]/div/div/div[4]/div/label")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", produdesta2);
                Thread.sleep(2000);


                WebElement terminosyc2 = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"formdata\"]/div[1]/div[2]/div/div/div[8]/div/div/label")));
                terminosyc2.click();
                Thread.sleep(2000);

                // Esperar a que el botón "Continuar" esté presente y hacer clic en él usando JavascriptExecutor
                WebElement continueEnd2 = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"formdata\"]/div[1]/div[2]/div/div/div[8]/div/button")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", continueEnd2);
                Thread.sleep(2000);

                // Esperar a que el formulario "payfast_form" esté presente y hacer clic en él usando JavascriptExecutor
                WebElement pagar2 = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"payfast_form\"]")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", pagar2);
                Thread.sleep(4000);

                WebElement element2 = driver.findElement(By.tagName("body"));

                String bodyText2 = element2.getText();

                assertTrue(bodyText2.contains("This payment method does not work, choose another one or contact the administrator."), "¡Texto no encontrado!");



                WebElement volverinicio = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/header/div[3]/div/div[1]/h1/a")));
                volverinicio.click();

            }

        }

    }


}
