package net.tecgurus.clickyvende.home;

import net.tecgurus.clickyvende.BaseTest;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestRoles extends BaseTest {

    @Test
    public void TestProbarRoles() {

        loginAsAdmin();


        navigateToRolesModule();


        performActions();


        performAdditionalActions();

        loginAsAdmi();

        accionreversa();
    }

    private void loginAsAdmin() {
        System.out.println("Navigating to the login page...");
        driver.manage().window().maximize();
        driver.get("https://test.clickyvende.net/");
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20));
        try {
            WebElement acceptCookiesButton = driver.findElement(By.xpath("//button[contains(text(), 'Aceptar')]"));
            acceptCookiesButton.click();
            Thread.sleep(1000);
        } catch (Exception e) {
            System.out.println("El botón de aceptar cookies no se encontró o ya se aceptaron.");
        }
        try {
            System.out.println("Waiting for the login button...");
            WebElement loginButton = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".btn:nth-child(1) > .hide-mob")));
            loginButton.click();

            System.out.println("Waiting for the username input...");
            WebElement usernameInput = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("email")));
            System.out.println("Entering the username...");
            usernameInput.sendKeys("phone@gmail.com");

            System.out.println("Waiting for the password input...");
            WebElement passwordInput = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("password")));
            System.out.println("Entering the password...");
            passwordInput.sendKeys("12345678");

            System.out.println("Waiting for the submit button...");
            WebElement submitButton = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".btn")));
            System.out.println("Clicking the submit button...");
            submitButton.click();
        } catch (Exception e) {
            System.out.println("Error during login: " + e.getMessage());
            throw e;
        }
    }

    private void navigateToRolesModule() {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20));
        try {
            System.out.println("Waiting for the admin menu...");
            WebElement adminMenu = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".dash-item:nth-child(4) .dash-mtext")));
            adminMenu.click();

            System.out.println("Waiting for the roles option...");
            WebElement rolesOption = wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Roles")));
            rolesOption.click();
        } catch (Exception e) {
            System.out.println("Error navigating to roles module: " + e.getMessage());
            throw e;
        }
    }

    private void performActions() {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20));

        try {
            System.out.println("Creating 'Administrador Productos' role...");
            WebElement createRoleButton = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".ti-plus")));
            createRoleButton.click();
            WebElement roleNameInput = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("name")));
            roleNameInput.sendKeys("Administrador Productos");

            WebElement checkAllGeneral = wait.until(ExpectedConditions.elementToBeClickable(By.id("checkall-general")));
            checkAllGeneral.click();

            WebElement saveRoleButton = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".btn-primary:nth-child(2)")));
            saveRoleButton.click();

            WebElement element = driver.findElement(By.tagName("body"));
            String bodyText = element.getText();
            assertTrue(bodyText.contains("Administrador Productos"), "¡Texto no encontrado!");


            System.out.println("Creating 'Administrador Limitadoo' role...");
            createRoleButton = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".ti-plus")));
            createRoleButton.click();
            roleNameInput = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("name")));
            roleNameInput.sendKeys("Administrador Limitadoo");

            WebElement productsModule = wait.until(ExpectedConditions.elementToBeClickable(By.id("module_checkbox_60_products")));
            productsModule.click();

            WebElement permission60_47 = wait.until(ExpectedConditions.elementToBeClickable(By.id("permission_60_47")));
            permission60_47.click();

            WebElement ordersModule = wait.until(ExpectedConditions.elementToBeClickable(By.id("module_checkbox_53_orders")));
            ordersModule.click();

            WebElement permission53_27 = wait.until(ExpectedConditions.elementToBeClickable(By.id("permission_53_27")));
            permission53_27.click();

            WebElement settingsModule = wait.until(ExpectedConditions.elementToBeClickable(By.id("module_checkbox_146_setting")));
            settingsModule.click();

            WebElement permission16_45 = wait.until(ExpectedConditions.elementToBeClickable(By.id("permission_16_45")));
            permission16_45.click();

            WebElement cartModule = wait.until(ExpectedConditions.elementToBeClickable(By.id("module_checkbox_0_cart")));
            cartModule.click();

            saveRoleButton = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".btn-primary:nth-child(2)")));
            saveRoleButton.click();

            WebElement element2 = driver.findElement(By.tagName("body"));
            String bodyText2 = element2.getText();
            assertTrue(bodyText2.contains("Administrador Limitadoo"), "¡Texto no encontrado!");


            System.out.println("Creating user 'Edna Maria'...");
            WebElement usersModule = wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Usuarios")));
            usersModule.click();

            WebElement createUserButton = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".f-30")));
            createUserButton.click();
            WebElement userNameInput = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("name")));
            userNameInput.sendKeys("Edna Maria");

            WebElement userEmailInput = wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("email")));
            userEmailInput.sendKeys("edna09@gmail.com");

            WebElement userPasswordInput = wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("password")));
            userPasswordInput.sendKeys("12345678");

            WebElement roleDropdown = wait.until(ExpectedConditions.elementToBeClickable(By.name("role")));
            roleDropdown.click();

            WebElement roleOption = roleDropdown.findElement(By.xpath("//option[. = 'Administrador Productos']"));
            roleOption.click();

            WebElement saveUserButton = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".btn-primary:nth-child(2)")));
            saveUserButton.click();

            WebElement element3 = driver.findElement(By.tagName("body"));
            String bodyText3 = element3.getText();
            assertTrue(bodyText3.contains("Edna Maria"), "¡Texto no encontrado!");


            System.out.println("Creating user 'Maria'...");
            createUserButton = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".f-30")));
            createUserButton.click();
            userNameInput = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("name")));
            userNameInput.sendKeys("Maria");

            userEmailInput = wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("email")));
            userEmailInput.sendKeys("maria27@gmail.com");

            userPasswordInput = wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("password")));
            userPasswordInput.sendKeys("12345678");

            roleDropdown = wait.until(ExpectedConditions.elementToBeClickable(By.name("role")));
            roleDropdown.click();

            roleOption = roleDropdown.findElement(By.xpath("//option[. = 'Administrador Limitadoo']"));
            roleOption.click();

            saveUserButton = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".btn-primary:nth-child(2)")));
            saveUserButton.click();

            WebElement element4 = driver.findElement(By.tagName("body"));
            String bodyText4 = element4.getText();
            assertTrue(bodyText4.contains("Maria"), "¡Texto no encontrado!");
        } catch (Exception e) {
            System.out.println("Error performing actions: " + e.getMessage());
            throw e;
        }
    }

    private void performAdditionalActions() {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20));

        try {
            // Additional actions
            System.out.println("Logging out as admin and logging in as Edna Maria...");
            driver.get("https://test.clickyvende.net/users");
            driver.manage().window().maximize();
            driver.findElement(By.cssSelector(".dash-head-link > .ms-2")).click();
            driver.findElement(By.linkText("Cerrar sesión")).click();
            driver.findElement(By.cssSelector(".btn:nth-child(1) > .hide-mob")).click();
            driver.findElement(By.id("email")).click();
            driver.findElement(By.id("email")).sendKeys("edna09@gmail.com");
            driver.findElement(By.id("password")).click();
            driver.findElement(By.id("password")).sendKeys("12345678");
            driver.findElement(By.id("password")).sendKeys(Keys.ENTER);

            {
                WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".dash-content")));
                Actions builder = new Actions(driver);
                builder.moveToElement(element).clickAndHold().perform();
            }
            {
                WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".dash-content")));
                Actions builder = new Actions(driver);
                builder.moveToElement(element).perform();
            }
            {
                WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".dash-content")));
                Actions builder = new Actions(driver);
                builder.moveToElement(element).release().perform();
            }

            System.out.println("Navigating to Products page...");
            WebElement adminMenuEdna = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".theme-3")));
            adminMenuEdna.click();
            WebElement productsMenuEdna = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".dash-item:nth-child(6) .dash-mtext")));
            productsMenuEdna.click();
            WebElement productsOptionEdna = wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Product")));
            productsOptionEdna.click();

            System.out.println("Modifying product name...");
            WebElement modifyButtonEdna = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("tr:nth-child(2) > .text-end > .btn > .ti")));
            modifyButtonEdna.click();
            WebElement productNameInputEdna = wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("name")));
            productNameInputEdna.click();
            productNameInputEdna.sendKeys("   Nuevo 2024");
            WebElement saveButtonEdna = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".me-2:nth-child(2)")));
            saveButtonEdna.click();


            WebElement updatedProductName = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//tr[2]/td[contains(text(), 'Nuevo 2024')]")));
            String actualProductName = updatedProductName.getText();
            String expectedProductName = "IPHONE 15 Nuevo 2024";
            assertEquals(expectedProductName, actualProductName, "El nombre del producto no se actualizo");

            System.out.println("Deleting product...");
            WebElement deleteButtonEdna = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("tr:nth-child(2) .d-inline .ti")));
            deleteButtonEdna.click();
            WebElement confirmDeleteButtonEdna = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".swal2-confirm")));
            confirmDeleteButtonEdna.click();
            List<WebElement> deletedProduct = driver.findElements(By.xpath("//tr[2]/td[contains(text(), 'IPHONE 15 Nuevo 2024')]"));
            assertTrue(deletedProduct.isEmpty(), "Product was not deleted successfully");

            System.out.println("Logging out as Edna Maria...");
            WebElement userMenuEdna = wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Hi, Edna Maria!")));
            userMenuEdna.click();
            WebElement logoutButtonEdna = wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Log Out")));
            logoutButtonEdna.click();
            WebElement loginPageButtonEdna = wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Inicio Sesión")));
            loginPageButtonEdna.click();

            System.out.println("Logging in as Maria...");
            WebElement emailInputMaria = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("email")));
            emailInputMaria.sendKeys("maria27@gmail.com");
            WebElement passwordInputMaria = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("password")));
            passwordInputMaria.sendKeys("12345678");
            WebElement submitButtonMaria = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".btn")));
            submitButtonMaria.click();

            System.out.println("Navigating to Products page...");
            WebElement adminMenuMaria = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".dash-item:nth-child(1) .dash-mtext")));
            adminMenuMaria.click();
            WebElement productsOptionMaria = wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Product")));
            productsOptionMaria.click();

            {
                WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".ti-pencil")));
                Actions builder = new Actions(driver);
                builder.moveToElement(element).perform();
            }

            WebElement modifyButtonMaria = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".ti-pencil")));
            modifyButtonMaria.click();

            WebElement productNameInputMaria = wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("name")));
            productNameInputMaria.sendKeys(" Oferta ");

            WebElement saveButtonMaria = wait.until(ExpectedConditions.elementToBeClickable(By.id("submit-all")));
            saveButtonMaria.click();

            WebElement deleteButtonMaria = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".ti-trash")));
            deleteButtonMaria.click();

            WebElement confirmDeleteButtonMaria = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".swal2-confirm")));
            assertTrue(confirmDeleteButtonMaria.isDisplayed(), "El botón de confirmación de eliminación no está visible.");
            confirmDeleteButtonMaria.click();
            driver.findElement(By.linkText("Hi, Maria!")).click();
            driver.findElement(By.linkText("Log Out")).click();
        } catch (Exception e) {
            System.out.println("Error performing additional actions: " + e.getMessage());
            throw e;
        }
    }
    private void loginAsAdmi() {
        System.out.println("Navigating to the login page...");
        driver.manage().window().maximize();
        driver.get("https://test.clickyvende.net/");

        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20));
        try {
            WebElement acceptCookiesButton = driver.findElement(By.xpath("//button[contains(text(), 'Aceptar')]"));
            acceptCookiesButton.click();
            Thread.sleep(1000);
        } catch (Exception e) {
            System.out.println("El botón de aceptar cookies no se encontró o ya se aceptaron.");
        }
        try {
            System.out.println("Waiting for the login button...");
            WebElement loginButton = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".btn:nth-child(1) > .hide-mob")));
            assertTrue(loginButton.isDisplayed(), "El botón de inicio de sesión debe estar visible.");
            loginButton.click();

            System.out.println("Waiting for the username input...");
            WebElement usernameInput = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("email")));
            assertTrue(usernameInput.isDisplayed(), "El campo de correo electrónico debe estar visible.");
            System.out.println("Entering the username...");
            usernameInput.sendKeys("phone@gmail.com");

            System.out.println("Waiting for the password input...");
            WebElement passwordInput = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("password")));
            assertTrue(passwordInput.isDisplayed(), "El campo de contraseña debe estar visible.");
            System.out.println("Entering the password...");
            passwordInput.sendKeys("12345678");

            System.out.println("Waiting for the submit button...");
            WebElement submitButton = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".btn")));
            System.out.println("Clicking the submit button...");
            submitButton.click();
        } catch (Exception e) {
            System.out.println("Error during login: " + e.getMessage());
            throw e;
        }
    }

    private void accionreversa() {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20));

        try {
            System.out.println("Navigating to Products...");
            WebElement productsMenu = waitUntilVisible(By.cssSelector(".dash-item:nth-child(6) .dash-mtext"));
            clickElementUsingActions(productsMenu);
            WebElement productsOption = waitUntilVisible(By.linkText("Producto"));
            clickElementUsingActions(productsOption);

            System.out.println("Performing delete operation on the product...");
            WebElement deleteButton = waitUntilVisible(By.cssSelector("tr:nth-child(1) .d-inline > .btn"));
            clickElementUsingActions(deleteButton);
            WebElement confirmDeleteButton = waitUntilVisible(By.cssSelector(".swal2-confirm"));
            clickElementUsingActions(confirmDeleteButton);

            System.out.println("Navigating to Roles...");
            WebElement rolesMenu = waitUntilVisible(By.cssSelector(".dash-item:nth-child(4) .dash-mtext"));
            clickElementUsingActions(rolesMenu);
            WebElement rolesOption = waitUntilVisible(By.linkText("Roles"));
            assertTrue(rolesOption.isDisplayed(), "El elemento 'Roles' debe estar visible.");

            clickElementUsingActions(rolesOption);

            System.out.println("Performing delete operation on the role...");
            WebElement deleteRoleButton = waitUntilVisible(By.cssSelector(".border-0:nth-child(1) .d-inline .ti"));
            clickElementUsingActions(deleteRoleButton);
            WebElement confirmDeleteRoleButton = waitUntilVisible(By.cssSelector(".swal2-confirm"));
            clickElementUsingActions(confirmDeleteRoleButton);
            WebElement trashButton = waitUntilVisible(By.cssSelector(".ti-trash"));
            clickElementUsingActions(trashButton);
            WebElement confirmTrashButton = waitUntilVisible(By.cssSelector(".swal2-confirm"));
            clickElementUsingActions(confirmTrashButton);

            System.out.println("Navigating to Users...");
            WebElement usersMenu = waitUntilVisible(By.linkText("Usuarios"));
            assertTrue(usersMenu.isDisplayed(), "El menú de 'Usuarios' debe estar visible.");
            clickElementUsingActions(usersMenu);

            System.out.println("Performing delete operations on users...");
            WebElement showUserButton = waitUntilVisible(By.cssSelector(".feather.icon-more-vertical"));
            clickElementUsingActions(showUserButton);
            WebElement deleteUserButton = waitUntilVisible(By.cssSelector(".show > .d-inline .ms-2"));
            clickElementUsingActions(deleteUserButton);
            WebElement confirmDeleteUserButton = waitUntilVisible(By.cssSelector(".swal2-confirm"));
            clickElementUsingActions(confirmDeleteUserButton);
            WebElement showUserButtonAgain = waitUntilVisible(By.cssSelector(".feather.icon-more-vertical"));
            clickElementUsingActions(showUserButtonAgain);
            WebElement finalDeleteButton = waitUntilVisible(By.linkText("Eliminar"));
            assertTrue(finalDeleteButton.isDisplayed(), "El botón 'Eliminar' debe estar visible.");
            clickElementUsingActions(finalDeleteButton);
            WebElement confirmFinalDeleteButton = waitUntilVisible(By.cssSelector(".swal2-confirm"));
            clickElementUsingActions(confirmFinalDeleteButton);
        } catch (Exception e) {
            System.out.println("Error performing actions: " + e.getMessage());
            throw e;
        }
    }

    private WebElement waitUntilVisible(By locator) {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20));
        WebElement element = null;
        try {
            element = wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        } catch (Exception e) {
            // Intentar desplazar el elemento a la vista y esperar de nuevo
            JavascriptExecutor js = (JavascriptExecutor) driver;
            WebElement toScrollElement = driver.findElement(locator);
            js.executeScript("arguments[0].scrollIntoView(true);", toScrollElement);
            element = wait.until(ExpectedConditions.visibilityOf(toScrollElement));
        }
        return element;
    }

    private void clickElementUsingActions(WebElement element) {
        Actions actions = new Actions(driver);
        actions.moveToElement(element).click().perform();
    }
}

